# Job Config
in_area   = '/data/atlaspmb/custom_nightly_tests/database_staging_area' # Unmerged files
db_area   = '/data/atlaspmb/database_staging_area' # Merged files
web_area  = '/data/atlaspmb/www_staging_area/'
n_days    = 21
n_workers = 16 # Number of workers in the pool for creating the webpages
jobs      = (
  # RAWtoALL
  ['rawtoall_tier0_reco_data16'                , ['21.0','master'], ['x86_64-centos7-gcc62-opt','x86_64-centos7-gcc11-opt']],
  ['rawtoall_tier0_reco_data17'                , ['21.0','master'], ['x86_64-centos7-gcc62-opt','x86_64-centos7-gcc11-opt']],
  ['rawtoall_tier0_reco_data18'                , ['21.0','master'], ['x86_64-centos7-gcc62-opt','x86_64-centos7-gcc11-opt']],
  ['rawtoall_data18_mt1'                       , ['master','master--archflagtest'], ['x86_64-centos7-gcc11-opt']],
  # MC full-chain
  ['fullchain_mc16_ttbar_valid_13tev_25ns_mu40', ['21.0','master'], ['x86_64-centos7-gcc62-opt','x86_64-centos7-gcc11-opt']],
  # Simulation
  ['simulation_ttbar'                          , ['21.0','master','master--HepMC3'], ['x86_64-centos7-gcc62-opt','x86_64-centos7-gcc11-opt']],
  ['fast_simulation_ttbar'                     , ['21.0','master','master--HepMC3'], ['x86_64-centos7-gcc62-opt','x86_64-centos7-gcc11-opt']],
  ['simulation_run3optimizations_ttbar'        , ['master'], ['x86_64-centos7-gcc11-opt']],
  ['simulation_run3optandgeom_ttbar'           , ['master'], ['x86_64-centos7-gcc11-opt']],
  # Digitization
  ['digitization_validation_mc16'              , ['master'], ['x86_64-centos7-gcc11-opt']],
  # Derivation
  ['derivation_physval_mc16'                   , ['21.2'], ['x86_64-centos7-gcc62-opt']],
  ['derivation_physval_data18'                 , ['21.2'], ['x86_64-centos7-gcc62-opt']],
  # ITK / Phase-2 Upgrade
  ['simulation_itk'                            , ['21.9'], ['x86_64-centos7-gcc62-opt']],
  ['rawtoall_mc_itk'                           , ['21.9'], ['x86_64-centos7-gcc62-opt']],
  ['digireco_mc_phase2_upgrade_mu60'           , ['21.9'], ['x86_64-centos7-gcc62-opt']],
  ['digireco_mc_phase2_upgrade_mu200'          , ['21.9'], ['x86_64-centos7-gcc62-opt']],
  ['rawtoall_mc_phase2_upgrade_itk_mu200'      , ['21.9'], ['x86_64-centos7-gcc62-opt']],
  # Run-3 Trigger
  ['rdotordotrigger_q221_mt1'                  , ['master'], ['x86_64-centos7-gcc11-opt']],
  # Run-3 Muons
  #['digireco_mc_muons_run3layout'              , ['master'], ['x86_64-centos7-gcc11-opt']]
)
