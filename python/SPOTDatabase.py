#!/usr/bin/env python3

__author__ = "Alaettin Serhan Mete <amete@cern.ch>"
__version__ = "0.0.1"

from os import path
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, DATE, ForeignKey, Float

import datetime
import json
import pandas as pd
import re
import sqlite3
import subprocess
import tarfile
import uuid

DEBUGMODE = False

class SPOTDatabase:
    """ Common SPOT Daily Job Database """

    def __init__(self, filename):

        # Setup output files properties
        self.filename = filename

        if path.isfile(filename):
            print("Database file {} already exists".format(filename))
            return

        # Create the connection
        self.db = create_engine('sqlite:///' + self.filename)
        self.db.echo = False
        self.metadata = MetaData(self.db)

        # Setup tables
        self.job = Table('job', self.metadata,
                         Column('jid', String(36), primary_key=True),
                         Column('name',               String(60)),
                         Column('step',               String(20)),
                         Column('platform',           String(30)),
                         Column('release',            String(30)),
                         Column('date',               DATE),
                         Column('nightly',            String(10)),
                         Column('exitcode',           Integer),
                         Column('nthreads',           Integer),
                         Column('nslots',             Integer),
                         Column('nevents',            Integer),
                         Column('cpu_per_event',      Integer),
                         Column('wall_per_event',     Integer),
                         Column('cpu_efficiency',     Integer),
                         Column('max_vmem',           Integer),
                         Column('max_pss',            Integer),
                         Column('max_rss',            Integer),
                         Column('leak_estimate_vmem', Integer),
                         Column('leak_estimate_pss',  Integer),
                         Column('cpu_model',          String(60)),
                         Column('cpu_ncores',         Integer),
                         Column('total_memory',       Integer),
                         Column('malloc_library',     String(60)),
                         Column('math_library',       String(60)))

        self.job.create()

        self.snapshot = Table('snapshot', self.metadata,
                              Column('stage',        String(10)),
                              Column('delta_cpu',    Float),
                              Column('delta_wall',   Float),
                              Column('avg_ncpu',     Float),
                              Column('delta_vmem',   Integer),
                              Column('delta_rss',    Integer),
                              Column('delta_pss',    Integer),
                              Column('delta_swap',   Integer),
                              Column('jid', Integer, ForeignKey('job.jid')))

        self.snapshot.create()

        self.component = Table('component', self.metadata,
                               Column('name',         String(80)),
                               Column('domain',       String(40)),
                               Column('step',         String(10)),
                               Column('stage',        String(10)),
                               Column('count',        Integer),
                               Column('total_cpu',    Integer),
                               #Column('total_wall',   Integer),
                               Column('total_vmem',   Integer),
                               Column('total_malloc', Integer),
                               Column('jid', Integer, ForeignKey('job.jid')))

        self.component.create()

        self.container = Table('container', self.metadata,
                               Column('name',        String(200)),
                               Column('domain',      String(40)),
                               Column('datatype',    String(40)),
                               Column('items',       Integer),
                               Column('compression', Float),
                               Column('sizeperevt',  Float),
                               Column('memsize',     Float),
                               Column('disksize',    Float),
                               Column('jid', Integer, ForeignKey('job.jid'))
                               )

        self.container.create()

    def mergeDBFiles(self, filesToMerge):

        conn = sqlite3.connect(self.filename)
        c = conn.cursor()

        if isinstance(filesToMerge, str):
            filesToMerge = [filesToMerge]

        for fileToMerge in filesToMerge:

            if DEBUGMODE:
                print(f"Attempt merging the DB file {fileToMerge} into {self.filename}")

            cmd = f"attach \"{fileToMerge}\" as toMerge"
            c.execute(cmd)

            cmd = "begin"
            c.execute(cmd)

            cmd = "select job.jid,job.release,job.nightly,job.step from toMerge.job"
            c.execute(cmd)

            all_rows = c.fetchall()

            cmd = "insert into job select * from toMerge.job"
            try:
                c.execute(cmd)
            except BaseException:
                print(f"Cannot merge job table from file {fileToMerge}!")

            cmd = "insert into snapshot select * from toMerge.snapshot"
            c.execute(cmd)

            cmd = "insert into component select * from toMerge.component"
            c.execute(cmd)

            cmd = "insert into container select * from toMerge.container"
            c.execute(cmd)

            cmd = "commit"
            c.execute(cmd)

            cmd = "detach database toMerge"
            c.execute(cmd)

            if DEBUGMODE:
                print(f"Succesfully merged the DB file {fileToMerge} into {self.filename}")

    def getCompDomain(self, compname):
        """ Build algorithm domains on-the-fly """

        # We didn't get a string...
        if not isinstance(compname, str):
            return ''

        # LRT
        if compname.endswith('R3LargeD0') or compname.startswith('LRT') or compname.endswith('LRT'):
            return 'LRT'
        # Monitoring
        elif 'MonAlg' in compname or 'monitoring' in compname.lower():
            return 'DataQuality'
        # PFlow
        elif compname.startswith('PF'):
            return 'PFlow'
        # ID
        elif compname.startswith('InDet'):
            return 'InDet'
        # Egamma
        elif (compname.startswith('egamma') or 'egamma' in compname.lower() or
              compname.startswith('electron') or compname.startswith('photon') or
              compname.startswith('EM')):
            return 'Egamma'
        # Jet/MET
        elif (compname.startswith('jetalg') or compname.startswith('MET') or
              compname.startswith('jetrecalg')):
            return 'JetMET'
        # Tau
        elif compname.startswith('Tau') or compname.startswith('DiTau'):
            return 'Tau'
        # Muon
        elif (compname.startswith('Muon') or compname.startswith('MuGirl') or compname.startswith('MuPat') or
              compname.startswith('Tgc') or compname.startswith('Csc') or compname.startswith('Rpc') or compname.startswith('Mdt') or
              compname.startswith('MS')):
            return 'Muon'
        # Calo
        elif compname.startswith('Calo') or compname.startswith('Tile') or compname.startswith('LAr'):
            if compname == 'CaloExtensionBuilderAlg': # Special case on request
                return 'CommonCaloExtension'
            return 'Calo'
        # Incident
        elif compname.startswith('Incident'):
            return 'Incident'
        # Output
        elif compname.startswith('Output') or compname.startswith('Stream'):
            return 'Output'
        # Trigger
        elif (compname.startswith('Trig') or compname.startswith('HLT') or
              compname.startswith('LVL1') or 'trig' in compname.lower()):
            return 'Trigger'
        # Everthing else
        else:
            return 'Other'

    def addEntry(self, release, platform, nightly, jobname, jobpath):

        # Check if we should be creating anyhting
        if not hasattr(self, 'db') or not hasattr(self, 'job'):
            print(f"\nNothing to be done for {release} {platform} {nightly} {jobname}")
            return
        print(f"\nProcessing {release} {platform} {nightly} {jobname}...")

        # Find all substeps
        import glob
        StepLogFiles = glob.glob(f"{jobpath}/log.*")

        # Loop over all steps
        for logFile in StepLogFiles:

            # Current step
            step = logFile.split('log.')[1]

            # Open and extract the PerfMonMT input - not all steps have one...
            try:
                tarFile = tarfile.open(f"{jobpath}/perfmonmt_{step}.json.tar.gz")
                jsonFile = tarFile.extractfile(f"perfmonmt_{step}.json")
            except BaseException:
                print(f"Cannot extract input tar/json files for step {step}, will continue...")
                continue

            # Get the exit code
            ecode = 99
            cmd = f"grep \"leaving with code\" {logFile}"
            line = subprocess.Popen(['/bin/bash', '-c',cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
            line = line.decode('utf-8').rstrip().split('\n')[0]
            matched = re.search('leaving with code (.+?):', line)
            if matched:
                ecode = int(matched.group(1))
            else:
                print("Cannot extract exitcode information...")

            # Proceed only if the job was successful
            if ecode != 0:
                print(f"Job has non-zero exitcode of {ecode}, continuing...")
                continue

            # Extract some metadata from the log file
            threads, slots = 0, 0
            cmd = f"grep \"Service is configured for .* threads analyzing .* events concurrently\" {logFile}"
            line = subprocess.Popen(['/bin/bash', '-c',cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]
            line = line.decode('utf-8').rstrip().split('\n')[0]
            matched = re.search('Service is configured for \[(.+?)\] threads analyzing \[(.+?)\] events concurrently', line)
            if matched:
                threads = int(matched.group(1))
                slots = int(matched.group(2))
            else:
                print("Cannot extract thread/slot information...")

            # Build nightly date time
            year = int(nightly.split('T')[0].split('-')[0])
            month = int(nightly.split('T')[0].split('-')[1])
            day = int(nightly.split('T')[0].split('-')[2])
            ndatetime = datetime.date(year, month, day)

            # Load the data
            data = json.load(jsonFile)

            # Get the summary data and convert into a DF
            df = pd.DataFrame(data, columns=['summary', 'componentLevel'])

            # Generate a unique id to be used for cross referencing tables
            jid = str(uuid.uuid4())

            # Fill in the data
            loop_wall_time = float(df['summary']['snapshotLevel']['FirstEvent']['dWall'])
            loop_wall_time += float(df['summary']['snapshotLevel']['Execute']['dWall'])
            loop_cpu_time = float(df['summary']['snapshotLevel']['FirstEvent']['dCPU'])
            loop_cpu_time += float(df['summary']['snapshotLevel']['Execute']['dCPU'])
            total_events = int(df['summary']['nEvents'])

            # Here insert into the table
            ins = self.job.insert()
            result = self.db.execute(ins,
                                     jid                = jid,
                                     name               = jobname,
                                     step               = step,
                                     platform           = platform,
                                     release            = release,
                                     date               = ndatetime,
                                     nightly            = nightly,
                                     exitcode           = ecode,
                                     nthreads           = threads,
                                     nslots             = slots,
                                     nevents            = total_events ,
                                     cpu_per_event      = loop_cpu_time/total_events if total_events > 0 else 0,
                                     wall_per_event     = loop_wall_time/total_events if total_events > 0 else 0,
                                     cpu_efficiency     = int(df['summary']['misc']['cpuUtilEff']),
                                     max_vmem           = int(df['summary']['peaks']['vmemPeak']),
                                     max_pss            = int(df['summary']['peaks']['pssPeak']),
                                     max_rss            = int(df['summary']['peaks']['rssPeak']),
                                     leak_estimate_vmem = int(df['summary']['leakEstimates']['vmemLeak']),
                                     leak_estimate_pss  = int(df['summary']['leakEstimates']['pssLeak']),
                                     cpu_model          = df['summary']['sysInfo']['cpuModel'],
                                     cpu_ncores         = int(df['summary']['sysInfo']['coreNum']),
                                     total_memory       = int(df['summary']['sysInfo']['totMem']),
                                     malloc_library     = df['summary']['envInfo']['mallocLib'],
                                     math_library       = df['summary']['envInfo']['mathLib'])

            # Print some information about what has happened
            if not result:
                print("Insertion into job table failed for step {step}!")
            elif DEBUGMODE:
                selection = self.job.select()
                result = self.db.execute(selection)
                for row in result:
                    print(f"Inserted {row}")

            # Now build the snapshot table
            s_df = pd.DataFrame(df['summary']['snapshotLevel']).transpose().rename_axis('stage').reset_index()

            # Rename the colums to match the table
            s_df = s_df.rename(columns={'dCPU':'delta_cpu', 'dWall':'delta_wall', 'cpuUtil':'avg_ncpu',
                                        'dVmem':'delta_vmem', 'dRss':'delta_rss', 'dPss':'delta_pss', 'dSwap':'delta_swap'})

            # Print the dataframe
            if DEBUGMODE:
                print(f"Prepared snapshot level dataframe")
                print(s_df)

            # Now add the jid
            s_df = s_df.assign(jid = jid)

            # Now add to the table
            s_df.to_sql('snapshot', index=None, con=self.db, if_exists='append')

            # Now check the insertion
            if DEBUGMODE:
                selection = self.snapshot.select()
                result = self.db.execute(selection)
                for row in result:
                    print(f"Inserted {row}")

            # Now build the component table
            stages = ['Initialize', 'FirstEvent', 'Execute', 'Finalize', 'preLoadProxy'] # Which execution steps to keep
            keep_top_n = 1000 # Keep Top N

            # Loop over all stages
            for stage in stages:

                if stage not in df['componentLevel']:
                    print(f"WARNING :: For step {step} {stage} not in the dataframe, check the job details!")
                    continue

                # Now create a dataframe for convenience
                c_df = pd.DataFrame(df['componentLevel'][stage]).transpose().rename_axis('name').reset_index()
                # Add the domain information at this point
                c_df['domain'] = c_df.apply(lambda x: self.getCompDomain(x['name']), axis=1)
                # Remove Sequences of type Ath(.*)Seq
                c_df = c_df[~(c_df.name.str.startswith('Ath')&(c_df.name.str.endswith('Seq')))]
                # Remove inclusive calls such as A.B.C.D in which case only keep A
                c_df = c_df[~(c_df.name.str.contains('\.'))]
                # Now order by cpu-time and pick the top N
                c_df_cpu = c_df.sort_values(by=['cpuTime'], ascending = False).head(keep_top_n)
                if threads == 1 or 'FirstEvent' not in stage or 'Execute' not in stage:
                    # Now order by malloc and pick the top N
                    c_df_malloc = c_df.sort_values(by=['malloc'], ascending = (stage != 'Finalize')).head(keep_top_n)
                    # Now order by vmem and pick the top N
                    c_df_vmem = c_df.sort_values(by=['vmem'], ascending = False).head(keep_top_n)
                    # Now concatenate all three and remove the duplicates
                    c_df_all = pd.concat([c_df_cpu,c_df_malloc,c_df_vmem]).drop_duplicates()
                else:
                    c_df_all = c_df_cpu
                # Now add some extra information
                c_df_all = c_df_all.assign(jid = jid)
                c_df_all = c_df_all.assign(step = step)
                c_df_all = c_df_all.assign(stage = stage)

                # Rename some of the columns
                c_df_all = c_df_all.rename(columns={'cpuTime':'total_cpu','wallTime':'total_wall','malloc':'total_malloc','vmem':'total_vmem'})
                try:
                    c_df_all.drop(columns=['total_wall'], inplace=True) # For now...
                except BaseException:
                    pass

                # Print the dataframe
                if DEBUGMODE:
                    print(f"Prepared component level dataframe for stage {stage}")
                    print(c_df_all)

                # Now add to the table
                c_df_all.to_sql('component', index=None, con=self.db, if_exists='append')

                # Now check the insertion
                if DEBUGMODE:
                    selection = self.component.select()
                    result = self.db.execute(selection)
                    for row in result:
                        print(f"Inserted {row}")

            # Now build the container table that holds container size information
            if step == 'Overlay':
                output_file_type = 'Overlay' # This produces an RDO but we don't track it for now
            else:
                output_file_type = logFile.split('to')[-1]

            # Need to implement other cases such as log.generate/log.AODtoDAOD
            output_file_type = 'AOD' if output_file_type == 'ALL' else output_file_type

            # Now read the checkxAOD.py output file (might not exist for multi-chain jobs)
            if not path.isfile(f"{jobpath}/my{output_file_type}.pool.root.checkfile.txt"):
                print(f"Did not find an xAODCheck file for {output_file_type}, continuing...")
                continue

            with open(f"{jobpath}/my{output_file_type}.pool.root.checkfile.txt") as checkxaod_output:

                current_block = '' # Event data, Categorized data, and Meta data

                # Loop over the lines
                for line in checkxaod_output:

                    # Filter out lines we don't need
                    if 'Container Name' in line or 'Total' in line:
                        continue

                    # Strip the new line at the end
                    line = line.rstrip()
                    values = line.split()

                    # Set the current block
                    if 'Event data' in line:
                        current_block = 'Event'
                    elif 'Categorized data' in line:
                        current_block = 'Categorized'
                    elif 'Meta data' in line:
                        current_block = 'Meta'

                    # Initialize the data
                    cdata = { 'name'        :  '',
                              'domain'      :  '',
                              'datatype'    :  '',
                              'items'       : -1 ,
                              'compression' : 0.0,
                              'sizeperevt'  : 0.0,
                              'memsize'     : 0.0,
                              'disksize'    : 0.0 }

                    # Look for event data
                    # Format : Mem Size      k Size        Size/Evt      Compression  Items  Container Name (Type)
                    if current_block == 'Event' and len(values) == 11:
                        cdata['datatype']     = 'EventData'
                        cdata['memsize']      = float(values[0])
                        cdata['disksize']     = float(values[2])
                        cdata['sizeperevt']   = float(values[4])
                        cdata['compression']  = float(values[6])
                        cdata['items']        = int(values[7])
                        cdata['name']         = values[8]
                        cdata['domain']       = values[10][1:-1]
                    # Look for meta data
                    # Format :  Mem Size       Disk Size         Container Name
                    elif current_block == 'Meta' and len(values) == 5:
                        cdata['datatype']     = 'MetaData'
                        cdata['memsize']      = float(values[0])
                        cdata['disksize']     = float(values[2])
                        cdata['name']         = values[4]

                    # If there is no data, move to the next line
                    if not cdata['name']:
                        continue

                    # Here insert into the table
                    ins = self.container.insert()
                    result = self.db.execute(ins,
                                             jid         = jid,
                                             name        = cdata['name'],
                                             domain      = cdata['domain'],
                                             datatype    = cdata['datatype'],
                                             items       = cdata['items'],
                                             compression = cdata['compression'],
                                             sizeperevt  = cdata['sizeperevt'],
                                             memsize     = cdata['memsize'],
                                             disksize    = cdata['disksize'])

                    # Print some information
                    if not result:
                        print("Insertion into container table failed for step {step}!")
                    elif DEBUGMODE:
                        selection = self.container.select()
                        result = self.db.execute(selection)
                        for row in result:
                            print(f"Inserted {row}")
