#!/usr/bin/env python3

# INCLUDED MODULE
import warnings
import re
import shutil
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
from sqlalchemy import create_engine
import numpy as np
import pandas as pd
plt.ioff()

# Suppress UserWarnings from matplotlib
warnings.filterwarnings("ignore", category=UserWarning, module="matplotlib")

# GLOBAL VARIABLES FOR PLOTTING

colors = ['gold', 'navy', 'orange', 'green', 'darkviolet', 'orangered', 'olive',
          'blue', 'firebrick', 'teal', 'purple', 'coral', 'steelblue', 'seagreen', 'red',
          'gold', 'navy', 'orange', 'green', 'darkviolet', 'orangered', 'olive', 'blue',
          'firebrick', 'teal', 'purple', 'coral', 'steelblue', 'seagreen', 'red']
markers = ['o', '<', 'x', '>', 'D', 'd', 's', 'p', '*', 'h', 'H',
           '+', 'o', '<', 'x', '>', '*', 'h', 'H', '+', 'o', '<', 'x', '>']

# CLASSES


class PMBWebPage:
    'Common class for a set of PMB web pages'

    def __init__(self, dbname, name, releases,
                 platforms, days=14, pathname='./'):
        self.name = name
        self.releases = releases
        self.platforms = platforms
        self.pathname = pathname
        self.days = days          # number of days to plot

        # Get connection to sqlite database

        db = create_engine('sqlite:///' + dbname)
        self.con = db.connect()
        self.list_of_commands = []

        # Get list of steps

        qry = 'SELECT DISTINCT j.step FROM job j WHERE j.name=\'' + \
            self.name + '\' AND j.exitcode=0 '
        self.steps = self.GetListOfNames('step', qry)
        if 'rawtoall_mc_phase2_upgrade_itk_mu200' in self.name:
            self.steps = ['RAWtoESD'] # overwrite

        if self.steps == []:
            self.list_of_commands = []
            self.common_labels = []
            self.prefix = ''
        else:

            # Build list of commands for list of platforms and releases
            for rel in self.releases:
                for plat in self.platforms:

                    stepsCommand = 'AND ('
                    counter = 0
                    for ste in self.steps:
                        if counter > 0:
                            stepsCommand += ' OR '
                        stepsCommand += 'j.step=\'' + ste + '\' '
                        counter += 1
                    stepsCommand += ') '

                    newCommand = 'j.release=\'' + rel + '\' AND  j.platform=\'' + plat + '\''
                    cmd = ' SELECT j.release,j.platform,j.name,j.date,j.jid FROM job j WHERE ' + \
                        newCommand + ' ' + stepsCommand + \
                        ' AND j.exitcode=0 GROUP BY j.date ORDER BY j.date'
                    entry = pd.read_sql_query(cmd, self.con)
                    if not entry.empty:
                        if len(entry['date']) < self.days:
                            self.days = len(entry['date'])
                        self.list_of_commands.append(newCommand)

            if len(self.list_of_commands) > 1:  # Building a comparison plot
                self.days = 3

            self.common_labels = []
            if len(self.releases) == 1:
                self.common_labels.append(self.releases[0])
            if len(self.platforms) == 1:
                self.common_labels.append(self.platforms[0])

            if len(self.list_of_commands) > 1:  # Building a comparison plot
                self.prefix = self.name + '__comparison'
            else:
                self.prefix = self.name + '__' + \
                    self.platforms[-1] + '__' + self.releases[-1]

        return

    def IsValid(self):
        if len(self.list_of_commands) == 0:
            return False
        else:
            return True

    def displaySteps(self):
        print("Steps")
        for s in self.steps:
            print(s)

    def MyXPlot(self, MyBigData, xlabel, ylabel, plotfileName, title):
        #       Best to remove sub-series with no data

        idxs = []
        index = 0
        for MyB in MyBigData:
            if len(MyB[2]) < 1:
                idxs.append(index)
            index += 1

        MyBigData = [i for j, i in enumerate(MyBigData) if j not in idxs]

        if MyBigData == []:
            return
        MyBigData.sort(key=lambda tup: tup[2][-1])
        MyBigData.reverse()

        #x_labels = []
        fig, ax1 = plt.subplots()

        # Scale the y-axis values
        yLabelLower = ylabel.lower()
        yMultiplier = 1.0
        if 'memory' in yLabelLower:
            if '(mb)' in yLabelLower or '(mb/event)' in yLabelLower:
                yMultiplier = 1.0/(1024.)
            elif '(gb)' in yLabelLower or '(gb/event)' in yLabelLower:
                yMultiplier = 1.0/(1024.*1024.)
        if 'time' in yLabelLower:
            if '(sec)' in yLabelLower:
                yMultiplier = 1.0/1000.

        # Get maximum x
        # Get maximum y
        # Plot empty data

        max_x = np.array(range(self.days * len(self.list_of_commands)))

        #max_index = 0
        max_val = 0
        index = 0
        xa = MyBigData[0][1]
        for MySeries in MyBigData:
            #print(MySeries)
            if len(MySeries[1]) == len(max_x):
                xa = MySeries[1]

            if MySeries[2] != []:
                if max(MySeries[2]) > max_val:
                    max_val = max(MySeries[2])
                    #max_index = index
                index += 1

                max_y = np.empty(len(max_x))
                max_y.fill(max_val*yMultiplier)
                #print(f"max_x length {len(max_x)}, xa length {len(xa)}")
                #print(f"max_x {max_x}, xa {xa}")
                #print(f"xlabel = {xlabel}")
                #print(f"ylabel = {ylabel}")
                #print(f"plotfileName = {plotfileName}")
                #print(f"title = {title}")
                if 'opt-' in xa[0]:
                    plt.xticks(max_x, [ '{}-{}'.format(val.split('opt-')[0].split('-')[2],
                                                       val.split('opt-')[1]) for val in xa ])
                else:
                    plt.xticks(max_x, xa)

        ax1.xaxis.set_tick_params(labelsize=20)
        ax1.plot(max_x,
                 max_y, '-', alpha=0.0)

        index = 0
        myMax = 0

        length = len(max_x)

        labelsize = 0

        for MySeries in MyBigData:
            x = []
            y = []
            for i in range(len(xa)):
                # Get date
                idx = 0
                for k in MySeries[1]:
                    if k == xa[i]:
                        x.append(i)
                        y.append(MySeries[2][idx]*yMultiplier)
                    idx += 1
            x = np.array(x)
            y = np.array(y)
            if len(y) < 1:
                continue
            if max(y) > myMax:
                myMax = max(y)

            ax1.plot(x,
                     y,
                     marker=markers[index],
                     markerfacecolor=colors[index],
                     markeredgecolor=colors[index],
                     color=colors[index], label=MySeries[0])
            for val in MySeries:
                labelsize = max(labelsize, len(val))

            index += 1

        ax1.grid(True)
        ax1.margins(0.05)
        ax1.set_xlim(-0.5, length - 0.5)
        if myMax > 0:
            ax1.set_ylim(0, myMax * 1.2)

        if length > self.days:
            for i in range(int(length / self.days) - 1):
                ax1.add_line(mlines.Line2D([(i + 1) * self.days - 0.5,
                                            (i + 1) * self.days - 0.5,
                                            (i + 1) * self.days - 0.5],
                                           [0, 1, myMax * 1.2],
                                           linewidth=5, color='teal', alpha=0.3))

        lgfont = 10.
        cutoff = 28.

        if labelsize > cutoff:
            lgfont = max(lgfont * cutoff / float(labelsize), 5.)

        leg = plt.legend(
            bbox_to_anchor=(1.01, 1.01),
            loc='upper left',
            ncol=1,
            numpoints=1,
            prop={'size': lgfont},
            title="")
        leg.get_frame().set_linewidth(0.0)

        plt.xticks(rotation='vertical', fontsize=8)
        plt.xticks(rotation=90)
        if len(self.releases) > 1:
            xlabel = 'Compiler-Release-' + xlabel
        plt.xlabel(xlabel, fontsize=10)
        plt.ylabel(ylabel, fontsize=10)

        fig.suptitle(title, fontsize=10)
        fig.subplots_adjust(top=0.88)
        fig.subplots_adjust(bottom=0.40)

        fig.subplots_adjust(right=0.55)

        fig.savefig(self.prefix + '__' + plotfileName + '.png')
        fig.savefig(self.prefix + '__' + plotfileName + '.pdf')

        plt.close("all")

        return

    def BuildComparisonPlot(self, list_of_commands, list_of_names, preamble_cmd,
                            fieldName, fileName, plotTitle, plotXlabel, plotYlabel):

        MyDataX = []
        dfs = {}
        for thealg in list_of_names:

            if thealg is None:
                continue
            dfs[thealg] = []
            dfs['xlabel'] = []

            for myCommand in list_of_commands:
                myC = re.split('\'', myCommand)
                newMyC = [x for x in myC if 'j.' not in x and x != '']

                cmd = ' ' + preamble_cmd + thealg + '\' AND ' + myCommand + ' AND j.name=\'' + \
                    self.name + '\' GROUP BY j.date ORDER BY j.date desc LIMIT ' + \
                    str(self.days) + ' ; '

                df0 = pd.read_sql_query(cmd, self.con)
                myList = df0[fieldName].values.tolist()
                myList.reverse()

                dfs[thealg] += myList

                myDateList = df0['date'].values.tolist()
                myDateList.reverse()

                for date in myDateList:
                    xlabel = str(date)
                    if len(list_of_commands) > 1:
                        for c in newMyC:
                            if c not in self.common_labels:
                                xlabel = str(c) + '-' + xlabel
                    dfs['xlabel'].append(xlabel)

            myXlist = dfs['xlabel']
            MyDataX.append([thealg, myXlist, dfs[thealg]])
        for myLabel in self.common_labels:
            if myLabel in self.releases:
                plotXlabel += '\n Release: ' + myLabel
            if myLabel in self.platforms:
                plotXlabel += '\n Platform: ' + myLabel

        if MyDataX != []:
            self.MyXPlot(MyDataX, plotXlabel, plotYlabel, fileName, plotTitle)

    def BuildData(self, list_of_commands, list_of_names,
                  preamble_cmd, fieldName):

        MyDataX = []
        dfs = {}
        for thealg in list_of_names:

            if thealg is None:
                continue
            dfs[thealg] = []
            dfs['xlabel'] = []

            for myCommand in list_of_commands:
                myC = re.split('\'', myCommand)
                newMyC = [x for x in myC if 'j.' not in x and x != '']

                cmd = ''' ''' + preamble_cmd + thealg + '''\' AND ''' + myCommand + \
                    ''' GROUP BY j.date ORDER BY j.date desc LIMIT ''' + \
                    str(self.days) + ''' ; '''
                df0 = pd.read_sql_query(cmd, self.con)
                myList = df0[fieldName].values.tolist()
                myList.reverse()

                dfs[thealg] += myList

                myDateList = df0['date'].values.tolist()
                myDateList.reverse()

                for date in myDateList:
                    xlabel = str(date)
                    if len(list_of_commands) > 1:
                        for c in newMyC:
                            if c not in self.common_labels:
                                xlabel = str(c) + '-' + xlabel
                    dfs['xlabel'].append(xlabel)

            myXlist = dfs['xlabel']
            MyDataX.append([thealg, myXlist, dfs[thealg]])

        return MyDataX

    def GetOrderedListOfDomains(self,
                                fieldName,
                                domainName,
                                tableName,
                                condition):

        cmd = '''
        SELECT MAX(j.date) as maxdate,c.''' + domainName + ''',SUM(c.''' + fieldName + ''') as t
        FROM ''' + tableName + ''' c
        INNER JOIN job j
        ON j.jid = c.jid
        WHERE  ''' + condition + '''
        GROUP BY c.''' + domainName + '''
        ORDER BY t DESC;
        '''

        _list = (pd.read_sql_query(cmd, self.con)).values.tolist()

        ordered_domains_list = []
        for item in _list:
            if item[1] != '':
                ordered_domains_list.append(item[1])

        return ordered_domains_list

    def GetMaxJids(self, step):

        name = self.name
        maxjids = []

        for cmd in self.list_of_commands:
            cmd = 'SELECT MAX(j.jid) as maxjid FROM job j WHERE j.name=\'' + name + \
                '\' AND j.step=\'' + step + '\' AND ' + cmd + ' ORDER BY maxjid'
            dfc = pd.read_sql_query(cmd, self.con)

            maxjid = dfc.values.tolist()[0][0]
            maxjids.append(maxjid)

        return maxjids

    def GetMaxDates(self, step):

        name = self.name
        maxdates = []

        for cmd in self.list_of_commands:
            cmd = 'SELECT MAX(j.date) as maxdate FROM job j WHERE j.name=\'' + name + \
                '\' AND j.step=\'' + step + '\' AND ' + cmd + ' ORDER BY maxdate'
            dfc = pd.read_sql_query(cmd, self.con)

            maxdate = dfc.values.tolist()[0][0]
            maxdates.append(maxdate)

        return maxdates

    def GetListOfNames(self, fieldName, query):

        df = pd.read_sql_query(query, self.con)
        y = df[fieldName].values.tolist()
        return y

    def makeDomainPlot(self, table='component', field='cpu', domain='domain',
                       plotTitle='Breakdown per domain', plotXlabel='Date', plotYlabel='Time per event (ms)'):
        for step in self.steps:
            myCmd = 'j.name=\'' + self.name + '\' AND j.step=\'' + step + '\''
            list_of_names = self.GetOrderedListOfDomains(
                field, domain, table, myCmd)
            preamble_cmd = ' SELECT c.' + domain + \
                ',j.date,SUM(c.' + field + ') as t FROM ' + table + \
                ' c INNER JOIN job j ON j.jid = c.jid WHERE ' + myCmd + 'AND c.name NOT LIKE \'%total_for%\' AND c.' + domain + '=\''
                ##' c INNER JOIN job j ON j.jid = c.jid WHERE ' + myCmd + ' AND c.' + domain + '=\''
            filename = step + '__' + domain + 'breakdown__' + field
            domain_exclusion_list = ['*Unknown*', 'caloringer', 'aod', 'forward'] # 2020/12/23 Hack to get the domain plot to behaved, fix properly
            list_of_names = [x for x in list_of_names if x is not None and x != '[---]' and x not in domain_exclusion_list]
            ##list_of_names = [x for x in list_of_names if x is not None]
            if list_of_names != []:
                self.BuildComparisonPlot(
                    self.list_of_commands,
                    list_of_names,
                    preamble_cmd,
                    't',
                    filename,
                    plotTitle +
                    '\nStep: ' +
                    step +
                    '\nJob: ' +
                    self.name,
                    plotXlabel,
                    plotYlabel)

    def makeTopTen(self, table='component', field='cpu', uniqCmd='c.stage=\'[evt]\' AND c.domain=\'id\'',
                   plotTitle='Top Ten in event loop', plotXlabel='Date', plotYlabel='Time per event (ms)'):

        for step in self.steps:
            jobName = self.name
            jobStep = step

            maxjids = self.GetMaxJids(step)
            maxjids = [x for x in maxjids if x is not None]
            if maxjids == []:
                return
            # Build jid command
            jidcmd = ''
            index = 0
            for jid in maxjids:
                index += 1
                if index == len(maxjids):
                    jidcmd += ' j.jid=' + str(jid) + ' )'
                else:
                    jidcmd += ' j.jid=' + str(jid) + ' OR '

            fieldName = field

            myCmd = ' SELECT c.name,c.' + fieldName + ' FROM ' + table + ' c INNER JOIN job j ON j.jid = c.jid WHERE ' + uniqCmd + \
                ' AND (' + jidcmd + '  AND j.name=\'' + jobName + '\' AND j.step=\'' + jobStep + \
                '\' AND c.name NOT LIKE \'%total_for%\' AND c.name NOT LIKE \'%Total%\' AND c.name NOT LIKE \'%collapsed%\' ORDER BY c.' + \
                    fieldName + ' DESC LIMIT 10 '

            list_of_names = self.GetListOfNames('name', myCmd)

            output = []
            for x in list_of_names:
                if x not in output:
                    output.append(x)
            list_of_names = output

            preamble_cmd = ' SELECT c.' + fieldName + ',j.date,c.name FROM ' + table + ' c INNER JOIN job j ON j.jid = c.jid WHERE j.name=\'' + \
                jobName + '\' AND j.step=\'' + jobStep + \
                '\' AND ' + uniqCmd + '  AND c.name=\''

            filename = step + '__' + \
                uniqCmd.replace('\'', '').replace(' ', '_') + '__' + field
            self.BuildComparisonPlot(
                self.list_of_commands,
                list_of_names,
                preamble_cmd,
                fieldName,
                filename,
                plotTitle +
                '\nStep: ' +
                step +
                '\nJob: ' +
                self.name,
                plotXlabel,
                plotYlabel)

        return

    def BuildComparisonPlotJob(self, list_of_commands, list_of_names, postamble_cmd,
                               fileName, step, plotTitle, plotXLabel, plotyLabel):

        MyDataX = []
        dfs = {}
        for thealg in list_of_names:

            dfs[thealg] = []
            dfs['xlabel'] = []

            for myCommand in list_of_commands:

                myC = re.split('\'', myCommand)
                newMyC = [x for x in myC if 'j.' not in x and x != '']
                cmd = '''SELECT j.''' + thealg + ''',j.date FROM job j WHERE ''' + postamble_cmd + ''' AND ''' + \
                    myCommand + ''' GROUP BY j.date ORDER BY j.date desc LIMIT ''' + \
                    str(self.days) + ''' ; '''

                df0 = pd.read_sql_query(cmd, self.con)

                myYList = df0[thealg].values.tolist()
                myYList.reverse()
                myYList[:] = [val for val in myYList]
                dfs[thealg] += myYList

                myXList = df0['date'].values.tolist()
                myXList.reverse()

                for date in myXList:
                    xlabel = str(date)
                    if len(list_of_commands) > 1:
                        for c in newMyC:
                            if c not in self.common_labels:
                                xlabel = str(c) + '-' + xlabel
                    dfs['xlabel'].append(xlabel)

            MyDataX.append([thealg, dfs['xlabel'], dfs[thealg]])

        for myLabel in self.common_labels:
            if myLabel in self.releases:
                plotXLabel += '\n Release: ' + myLabel
            if myLabel in self.platforms:
                plotXLabel += '\n Platform: ' + myLabel

        self.MyXPlot(MyDataX, plotXLabel, plotyLabel, fileName, plotTitle)

        return

# limo3
    def BuildComparisonPlotComponent(self, list_of_commands, list_of_names,
                                     postamble_cmd, fileName, step, plotTitle, plotXLabel, plotyLabel):

        MyDataX = []
        dfs = {}
        for thealg in list_of_names:

            dfs[thealg] = []
            dfs['xlabel'] = []

            for myCommand in list_of_commands:

                myC = re.split('\'', myCommand)
                newMyC = [x for x in myC if 'j.' not in x and x != '']

                cmd = '''SELECT c.''' + thealg + ''',j.date FROM component c INNER JOIN job j ON j.jid = c.jid WHERE  ''' + \
                    postamble_cmd + ''' AND ''' + myCommand + \
                    ''' GROUP BY j.date ORDER BY j.date desc LIMIT ''' + \
                    str(self.days) + ''' ; '''

                df0 = pd.read_sql_query(cmd, self.con)
                myYList = df0[thealg].values.tolist()
                myYList.reverse()
                dfs[thealg] += myYList

                myXList = df0['date'].values.tolist()
                myXList.reverse()

                for date in myXList:
                    xlabel = str(date)
                    if len(list_of_commands) > 1:
                        for c in newMyC:
                            if c not in self.common_labels:
                                xlabel = str(c) + '-' + xlabel

                    dfs['xlabel'].append(xlabel)
            MyDataX.append([thealg, dfs['xlabel'], dfs[thealg]])

        for myLabel in self.common_labels:
            if myLabel in self.releases:
                plotXLabel += '\n Release: ' + myLabel
            if myLabel in self.platforms:
                plotXLabel += '\n Platform: ' + myLabel

        self.MyXPlot(MyDataX, plotXLabel, plotyLabel, fileName, plotTitle)
        return

    def PlotComponentDifference(self,
                                component1='snapshot_post_ini',
                                component2='snapshot_post_1stevt',
                                field1='wall',
                                field2='cpu',
                                stage='[---]',
                                ylabel='Time (ms)',
                                title='Time for first event',
                                desc='time1stevt',
                                postamble=' j.name=\'fullchain_mc15_ttbar_valid_13tev_25ns_mu40\' AND j.step=\'RAWtoESD\' ',
                                myCommand=' AND j.platform=\'x86_64-slc6-gcc49-opt\' AND j.release=\'21.0.X-VAL\' ',
                                pathName='.',
                                nevts=False):

        n_e = 1
        if nevts:
            cmd = '''
            SELECT c.n,j.date
            FROM component c
            INNER JOIN job j
            ON j.jid = c.jid
            WHERE c.name=\'evtloop_time\'
            AND ''' + postamble + ''' AND
            ''' + myCommand + '''
            GROUP BY j.date ORDER BY j.date desc LIMIT ''' + str(self.days) + ''' ;
            '''
            df = pd.read_sql_query(cmd, self.con)
            if df.empty:
                return []
            if len(df.values.tolist()[0]) > 1:
                n_e = (df.values.tolist()[0][0]) - 1
            else:
                return []

        d_f1 = {}
        d_f2 = {}

        fig, ax1 = plt.subplots()

        cmd = 'SELECT c.' + field1 + ',j.date FROM component c INNER JOIN job j ON j.jid = c.jid WHERE c.name=\'' + component1 + \
            '\' AND ' + postamble + ' AND ' + myCommand + \
            ' GROUP BY j.date ORDER BY j.date desc LIMIT ' + \
            str(self.days) + ' ;'

        df = pd.read_sql_query(cmd, self.con)
        myYList = df[field1].values.tolist()
        myYList.reverse()
        d_f1[component1] = myYList
        d_f1[component1] = np.array(d_f1[component1])
        d_f1[component1] = d_f1[component1] / float(n_e)

        cmd = 'SELECT c.\'' + field1 + '\',j.date FROM component c INNER JOIN job j ON j.jid = c.jid WHERE c.name=\'' + component2 + \
            '\' AND ' + postamble + ' AND ' + myCommand + \
            ' GROUP BY j.date ORDER BY j.date desc LIMIT ' + \
            str(self.days) + ' ;'
        df = pd.read_sql_query(cmd, self.con)
        myYList = df[field1].values.tolist()
        myYList.reverse()
        d_f1[component2] = myYList
        d_f1[component2] = np.array(d_f1[component2])
        d_f1[component2] = d_f1[component2]

        myXList = df['date'].values.tolist()
        myXList.reverse()
        d_f1['date'] = myXList

        d_f1['diff'] = []

        n_elements = range(len(d_f1[component1]))
        for k in n_elements:
            d_f1['diff'].append(
                (d_f1[component2][k] - d_f1[component1][k]) / float(n_e))

        cmd = 'SELECT c.\'' + field2 + '\',j.date FROM component c INNER JOIN job j ON j.jid = c.jid WHERE c.name=\'' + component1 + \
            '\' AND ' + postamble + ' AND ' + myCommand + \
            ' GROUP BY j.date ORDER BY j.date desc LIMIT ' + \
            str(self.days) + ' ;'
        df = pd.read_sql_query(cmd, self.con)
        myYList = df[field2].values.tolist()
        myYList.reverse()
        d_f2[component1] = myYList
        d_f2[component1] = np.array(d_f2[component1])
        d_f2[component1] = d_f2[component1] / float(n_e)

        cmd = 'SELECT c.\'' + field2 + '\',j.date FROM component c INNER JOIN job j ON j.jid = c.jid WHERE c.name=\'' + component2 + \
            '\' AND ' + postamble + ' AND ' + myCommand + \
            ' GROUP BY j.date ORDER BY j.date desc LIMIT ' + \
            str(self.days) + ' ;'

        df = pd.read_sql_query(cmd, self.con)
        myYList = df[field2].values.tolist()
        myYList.reverse()
        d_f2[component2] = myYList
        d_f2[component1] = np.array(d_f2[component1])
        d_f2[component1] = d_f2[component1]

        d_f2['date'] = df['date'].values.tolist()

        d_f2['diff'] = []
        n_elements = range(len(d_f1[component1]))
        for k in n_elements:
            d_f2['diff'].append((float(d_f2[component2][k]) - float(d_f2[component1][k])) / float(n_e))

        #dates = pd.to_datetime(d_f1['date'], format='%Y-%m-%d')

        d_f3 = {}
        d_f3['diff'] = []
        n_elements = range(len(d_f1[component1]))
        for k in n_elements:
            d_f3['diff'].append(float(d_f1['diff'][k]) - float(d_f2['diff'][k]))
        myData = []
        myData.append(['wall', d_f1['date'], d_f1['diff']])
        myData.append(['cpu', d_f1['date'], d_f2['diff']])
        #myData.append(['wall-cpu', d_f1['date'], d_f3['diff']])

        return myData

    def JobPlot(self, list_of_names, postamble_cmd, filename,
                plotTitle, plotXlabel, plotYlabel):
        for step in self.steps:
            self.BuildComparisonPlotJob(
                self.list_of_commands,
                list_of_names, ' j.name=\'' + self.name + '\' AND j.step=\'' + step + '\' ' + postamble_cmd,
                step + filename,
                step,
                plotTitle + ' \nStep: ' + step + '\nJob: ' + self.name,
                plotXlabel,
                plotYlabel)

    def ComponentPlot(self, list_of_names, postamble_cmd,
                      filename, plotTitle, plotXlabel, plotYlabel):
        for step in self.steps:
            self.BuildComparisonPlotComponent(
                self.list_of_commands,
                list_of_names,
                ' j.name=\'' + self.name + '\' AND j.step=\'' + step + '\' ' + postamble_cmd,
                step + filename, step,
                plotTitle + ' \nStep: ' + step + '\nJob: ' + self.name,
                plotXlabel,
                plotYlabel)

    def GetComparisonTimeDifferences(self,
                                     starttime='snapshot_post_ini',
                                     endtime='snapshot_post_1stevt',
                                     title='Time for first event',
                                     filename='time1stevt', ne=False):
        for step in self.steps:
            postAmble = ' j.name=\'' + self.name + '\' AND j.step=\'' + step + '\' '
            list_of_commands = self.list_of_commands[-1]

            bigX = {}
            bigX['wall'] = []
            bigX['cpu'] = []
            #bigX['wall-cpu'] = []

            bigY = {}
            bigY['wall'] = []
            bigY['cpu'] = []
            #bigY['wall-cpu'] = []

            bigZ = []

            for myCommand in self.list_of_commands:

                cmdData = self.PlotComponentDifference(
                    starttime,
                    endtime,
                    'wall',
                    'cpu',
                    '[---]',
                    'Time (sec)',
                    title,
                    filename,
                    postAmble,
                    myCommand,
                    '.',
                    ne)

                if cmdData == []:
                    return

                for cdata in cmdData:
                    bigX[cdata[0]] += cdata[1]
                    bigY[cdata[0]] += cdata[2]

            #myIndex = 0
            newbigX = []
            for i in range(len(self.list_of_commands)):

                myC = re.split('\'', self.list_of_commands[i])
                newMyC = [x for x in myC if 'j.' not in x and x != '']

                for j in range(self.days * i, self.days * (i + 1)):
                    if len(bigX['wall']) <= j:
                        continue
                    xlabel = str(bigX['wall'][j])
                    if len(list_of_commands) > 1:
                        for c in newMyC:
                            if c not in self.common_labels:
                                xlabel = str(c) + '-' + xlabel
                    newbigX.append(xlabel)

            plotXlabel = 'Date'
            for myLabel in self.common_labels:
                if myLabel in self.releases:
                    plotXlabel += '\n Release: ' + myLabel
                if myLabel in self.platforms:
                    plotXlabel += '\n Platform: ' + myLabel

            for key in bigX:
                ydata = [key, newbigX, bigY[key]]
                bigZ.append(ydata)

            self.MyXPlot(
                bigZ,
                plotXlabel,
                'Time (sec)',
                step +
                '__' +
                filename,
                title +
                '\nStep: ' +
                step +
                '\nJob: ' +
                self.name)

        return

    def HomePlot(self, tableName, fieldNames, cname, filename,
                 plotXlabel, plotYlabel, plotTitle):
        MyData = []
        jobName = self.name
        list_of_names = self.steps
        for field in fieldNames:
            Y = []
            preamble_cmd = ' SELECT c.' + field + ',j.date FROM ' + tableName + \
                ' c INNER JOIN job j ON j.jid = c.jid WHERE j.name=\'' + \
                jobName + '\' AND c.name=\'' + cname + '\' AND j.step=\''
            Y = self.BuildData(
                self.list_of_commands,
                list_of_names,
                preamble_cmd,
                field)
            for sd in Y:
                sd[0] = sd[0] + '/' + field
                MyData.append(sd)

        for myLabel in self.common_labels:
            if myLabel in self.releases:
                plotXlabel += '\n Release: ' + myLabel
            if myLabel in self.platforms:
                plotXlabel += '\n Platform: ' + myLabel

        # Sort a list

        if MyData != []:
            MyData.sort(key=lambda tup: tup[2])
            MyData.reverse()
            self.MyXPlot(
                MyData,
                plotXlabel,
                plotYlabel,
                filename,
                plotTitle +
                '\nJob: ' +
                self.name)

        return

    def Build_timing_WebPage(self):

        jobName = self.name

        self.GetComparisonTimeDifferences(
            'snapshot_pre_ini',
            'snapshot_post_ini',
            'Time for initialization',
            'ini__timing',
            False)
        self.GetComparisonTimeDifferences(
            'snapshot_post_ini',
            'snapshot_post_1stevt',
            'Time for first event',
            '1stevt__timing',
            False)
        self.GetComparisonTimeDifferences(
            'snapshot_post_1stevt',
            'snapshot_post_lastevt',
            'Time for event loop',
            'evtloop__timing',
            True)
        self.GetComparisonTimeDifferences(
            'snapshot_post_lastevt',
            'snapshot_post_fin',
            'Time for finalization',
            'fin__timing',
            False)

        self.JobPlot(
            ['jobcfg_walltime'],
            ' ',
            '__jobcfg__timing',
            'Job configuration time',
            'Date',
            'Time (sec)')
        self.ComponentPlot(['wall',
                            'cpu'],
                           ' AND c.name=\'overhead_time\' ',
                           '__overhead__timing',
                           'Overhead time',
                           'Date',
                           'Time (sec)')

        fieldName = 'timing'
        desc = 'This page has plots detailing the timing of jobs in the stages of an Athena job.<p/>\n'

        directory = self.pathname + "/arch-mon-" + jobName + \
            "/pages_custom/" + fieldName + "/build_and_stream_comparisons"

        if len(self.list_of_commands) == 1:
            myCommand = self.list_of_commands[0]
            myC = re.split('\'', myCommand)
            newMyC = [x for x in myC if 'j.' not in x and x != '']
            jobRelease = newMyC[0]
            jobPlatform = newMyC[1]
            directory = self.pathname + "/arch-mon-" + jobName + \
                "/pages_custom/" + fieldName + "/" + jobRelease + "__" + jobPlatform

        if not os.path.exists(directory):
            os.makedirs(directory)

        myfile = directory + '/html.snippet'

        release_web_page = open(myfile, 'w')
        release_web_page.write('<div style="width:900px;font-size:80%;color:gray;padding:10px">\n')
        release_web_page.write('    <div style="width:900px;font-size:80%;color:gray;padding:10px">\n')
        release_web_page.write(desc)
        release_web_page.write('    </div>\n')
        release_web_page.write('    <p/><strong>Plots on this page:</strong>\n')
        release_web_page.write('    <div style="width:1000px;column-count:3;-moz-column-count:3;-webkit-column-count:3">\n')
        release_web_page.write('      <ul style="font-size:80%">\n')
        import glob

        for step in self.steps:
            fileloc = self.name + '__comparison__' + step + '__' + '*' + '__' + fieldName
            if len(self.list_of_commands) == 1:
                fileloc = self.name + '__' + jobPlatform + '__' + \
                    jobRelease + '__' + step + '__' + '*' + '__' + fieldName

            # GET LIST OF DOMAINS FOR THE GIVEN STEPS BY SEARCHING FOR THE
            # FILES

            plots = glob.glob(fileloc + '.png')
            plots_pdf = glob.glob(fileloc + '.pdf')

            for plot in plots_pdf:
                shutil.move(plot, directory + '/' + plot.split('/')[-1])

            for plot in plots:
                shutil.move(plot, directory + '/' + plot.split('/')[-1])

                plotfile = plot.split('/')[-1]
                plot_ref = step + '/' + (plot.split('__')[-2]).split('=')[-1]

                release_web_page.write('        <li><a href="!#' + plot_ref + '">' + plot_ref + '</a></li>\n')

        release_web_page.write('      </ul>\n')
        release_web_page.write('    </div>\n')

        for step in self.steps:
            fileloc = self.name + '__comparison__' + step + '__' + '*' + '__' + fieldName
            if len(self.list_of_commands) == 1:
                fileloc = self.name + '__' + jobPlatform + '__' + \
                    jobRelease + '__' + step + '__' + '*' + '__' + fieldName
            plots = glob.glob(directory + '/' + fileloc + '.png')
            for plot in plots:
                plotfile = plot.split('/')[-1]
                plotfile_pdf = plotfile.replace('png', 'pdf')

                plotfile = plot.split('/')[-1]
                plot_ref = step + '/' + (plot.split('__')[-2]).split('=')[-1]

                release_web_page.write('  \n')
                release_web_page.write('  <div style="padding:10px;width:1000px;height:450px">\n')
                release_web_page.write('    <a name="' + plot_ref + '"><strong>Plot: ' + plot_ref + '</strong></a><br/>\n')
                release_web_page.write('    <div style="float:left;width:500px;padding:10px">\n')
                release_web_page.write('      <a href="' + plotfile + '"><img alt="' + plot.split( '/')[-1] + '" width="500" height="400" src="' + plotfile + '"/></a>\n')
                release_web_page.write('    </div>\n')
                release_web_page.write('    <div style="float:right;width:450px;font-size:80%;color:gray;padding:10px">\n')
                release_web_page.write('      <strong>Direct links:</strong> <a href="!">this page</a> / <a href="!#' + plot_ref + '">this plot</a><p/>\n')
                release_web_page.write('      <strong>Other formats:</strong> <a href="' + plotfile_pdf + '">PDF</a> \n')
                release_web_page.write('    </div>\n')
                release_web_page.write('  </div><br/>\n')

        self.Build_logsF(release_web_page)

        return

    def Build_memory_WebPage(self):

        jobName = self.name

        self.JobPlot(['vmem_peak',
                      'vmem_mean',
                      'rss_mean'],
                     ' ',
                     '__total__memory',
                     'Total memory',
                     'Date',
                     'Memory (GB)')
        self.ComponentPlot(['vmem',
                            'malloc'],
                           ' AND c.name=\'leakperevt_evt11to100\' ',
                           '__leak__memory',
                           'Memory leak',
                           'Date',
                           'Memory Growth (KB/event)')

        fieldName = 'memory'
        desc = 'This page has plots detailing how memory is consumed in the stages of an Athena job.<p/>\n'

        directory = self.pathname + "/arch-mon-" + jobName + \
            "/pages_custom/" + fieldName + "/build_and_stream_comparisons"

        if len(self.list_of_commands) == 1:
            myCommand = self.list_of_commands[0]
            myC = re.split('\'', myCommand)
            newMyC = [x for x in myC if 'j.' not in x and x != '']
            jobRelease = newMyC[0]
            jobPlatform = newMyC[1]
            directory = self.pathname + "/arch-mon-" + jobName + \
                "/pages_custom/" + fieldName + "/" + jobRelease + "__" + jobPlatform

        if not os.path.exists(directory):
            os.makedirs(directory)

        myfile = directory + '/html.snippet'

        release_web_page = open(myfile, 'w')
        release_web_page.write('<div style="width:900px;font-size:80%;color:gray;padding:10px">\n')
        release_web_page.write('    <div style="width:900px;font-size:80%;color:gray;padding:10px">\n')
        release_web_page.write(desc)
        release_web_page.write('    </div>\n')
        release_web_page.write('    <p/><strong>Plots on this page:</strong>\n')
        release_web_page.write('    <div style="width:1000px;column-count:3;-moz-column-count:3;-webkit-column-count:3">\n')
        release_web_page.write('      <ul style="font-size:80%">\n')
        import glob
        for step in self.steps:
            fileloc = self.name + '__comparison__' + step + '__' + '*' + '__' + fieldName
            if len(self.list_of_commands) == 1:
                fileloc = self.name + '__' + jobPlatform + '__' + \
                    jobRelease + '__' + step + '__' + '*' + '__' + fieldName

            # GET LIST OF DOMAINS FOR THE GIVEN STEPS BY SEARCHING FOR THE
            # FILES

            plots = glob.glob(fileloc + '.png')
            plots_pdf = glob.glob(fileloc + '.pdf')

            for plot in plots_pdf:
                shutil.move(plot, directory + '/' + plot.split('/')[-1])

            for plot in plots:

                shutil.move(plot, directory + '/' + plot.split('/')[-1])

                plotfile = plot.split('/')[-1]
                plot_ref = step + '/' + (plot.split('__')[-2]).split('=')[-1]

                release_web_page.write('        <li><a href="!#' + plot_ref + '">' + plot_ref + '</a></li>\n')

        release_web_page.write('      </ul>\n')
        release_web_page.write('    </div>\n')

        for step in self.steps:
            fileloc = self.name + '__comparison__' + step + '__' + '*' + '__' + fieldName
            if len(self.list_of_commands) == 1:
                fileloc = self.name + '__' + jobPlatform + '__' + \
                    jobRelease + '__' + step + '__' + '*' + '__' + fieldName
            plots = glob.glob(directory + '/' + fileloc + '.png')
            for plot in plots:
                plotfile = plot.split('/')[-1]
                plotfile_pdf = plotfile.replace('png', 'pdf')

                plotfile = plot.split('/')[-1]
                plot_ref = step + '/' + (plot.split('__')[-2]).split('=')[-1]

                release_web_page.write('  \n')
                release_web_page.write('  <div style="padding:10px;width:1000px;height:450px">\n')
                release_web_page.write('    <a name="' + plot_ref + '"><strong>Plot: ' + plot_ref + '</strong></a><br/>\n')
                release_web_page.write('    <div style="float:left;width:500px;padding:10px">\n')
                release_web_page.write('      <a href="' + plotfile + '"><img alt="' + plot.split( '/')[-1] + '" width="500" height="400" src="' + plotfile + '"/></a>\n')
                release_web_page.write('    </div>\n')
                release_web_page.write('    <div style="float:right;width:450px;font-size:80%;color:gray;padding:10px">\n')
                release_web_page.write('      <strong>Direct links:</strong> <a href="!">this page</a> / <a href="!#' + plot_ref + '">this plot</a><p/>\n')
                release_web_page.write('      <strong>Other formats:</strong> <a href="' + plotfile_pdf + '">PDF</a> \n')
                release_web_page.write('    </div>\n')
                release_web_page.write('  </div><br/>\n')

        self.Build_logsF(release_web_page)

        return

# USED
    def Build_home_WebPage(self):

        jobName = self.name

        myorderdict = {
            'cpu_looptime': 0,
            'wall_looptime': 1,
            'physical_memory': 2,
            'virtual_memory': 3,
            'memory_leak': 4,
            'cpu_overhead': 5,
            'wall_overhead': 6}

        self.HomePlot(
            'component',
            ['cpu'],
            'evtloop_time',
            'cpu_looptime__all',
            'Date',
            'Time (sec)',
            'Time in Event Loop (CPU)')
        self.HomePlot(
            'component',
            ['wall'],
            'evtloop_time',
            'wall_looptime__all',
            'Date',
            'Time (sec)',
            'Time in Event Loop (Wall)')
        self.HomePlot(
            'job',
            ['vmem_peak'],
            jobName,
            'virtual_memory__all',
            'Date',
            'Memory (GB)',
            'Virtual Memory')
        self.HomePlot(
            'job',
            ['rss_mean'],
            jobName,
            'physical_memory__all',
            'Date',
            'Memory (GB)',
            'Physical Memory')
        self.HomePlot('component',
                      ['malloc',
                       'vmem'],
                      'leakperevt_evt11to100',
                      'memory_leak__all',
                      'Date',
                      'Memory Growth (MB/event)',
                      'Memory leak')
        self.HomePlot(
            'component',
            ['cpu'],
            'overhead_time',
            'cpu_overhead__all',
            'Date',
            'Time (sec)',
            'Overhead Time (CPU)')
        self.HomePlot(
            'component',
            ['wall'],
            'overhead_time',
            'wall_overhead__all',
            'Date',
            'Time (sec)',
            'Overhead Time (Wall)')

        fieldName = 'all'
        desc = '      Welcome to the SPOT performance monitoring pages where various reconstruction computing performance metrics are plotted.<p/>The present page contains a few plots providing a very high-level overview of the current CPU and memory performance in both the Tier0 and the main development branch of ATLAS offline software. Please use the menu at the top of the page to navigate to more detailed information.<p/>For maximum stability and reproducibility the relevant jobs are not run in ART, but rather via custom scripts in cron on the same machines each day. Most data is collected via <a href="https://twiki.cern.ch/twiki/bin/viewauth/Atlas/PerfMonSD">PerfMonSD</a>, the documentation of which it is recommended to read. Note that only data from successful jobs are used for the plots, and that not all builds are displayed on this frontpage. <p/>\n'

        # directory=self.pathname+"arch-mon-"+jobName+"/pages_custom/all/overview"
        directory = self.pathname + "arch-mon-" + jobName + \
            "/pages_custom/" + fieldName + "/build_and_stream_comparisons"

        if len(self.list_of_commands) == 1:
            myCommand = self.list_of_commands[0]
            myC = re.split('\'', myCommand)
            newMyC = [x for x in myC if 'j.' not in x and x != '']
            jobRelease = newMyC[0]
            jobPlatform = newMyC[1]
            directory = self.pathname + "/arch-mon-" + jobName + \
                "/pages_custom/" + fieldName + "/" + jobRelease + "__" + jobPlatform

        if not os.path.exists(directory):
            os.makedirs(directory)

        myfile = directory + '/html.snippet'

        release_web_page = open(myfile, 'w')
        release_web_page.write('<div style="width:900px;font-size:80%;color:gray;padding:10px">\n')
        release_web_page.write('    <div style="width:900px;font-size:80%;color:gray;padding:10px">\n')
        release_web_page.write(desc)
        release_web_page.write('    </div>\n')
        release_web_page.write('    <p/><strong>Plots on this page:</strong>\n')
        release_web_page.write('    <div style="width:1000px;column-count:3;-moz-column-count:3;-webkit-column-count:3">\n')
        release_web_page.write('      <ul style="font-size:80%">\n')
        import glob

        fileloc = self.name + '__comparison__' + '*' + '__' + fieldName

        # GET LIST OF DOMAINS FOR THE GIVEN STEPS BY SEARCHING FOR THE FILES

        plots = glob.glob(fileloc + '.png')
        plots_pdf = glob.glob(fileloc + '.pdf')

        if plots == []:
            fileloc = self.name + '__' + \
                self.platforms[-1] + '__' + \
                self.releases[-1] + '*__' + fieldName
            plots = glob.glob(fileloc + '.png')
            plots_pdf = glob.glob(fileloc + '.pdf')

        for plot in plots_pdf:
            shutil.move(plot, directory + '/' + plot.split('/')[-1])

        for plot in sorted(plots, key=lambda x: myorderdict[(
                x.split('__')[-2]).split('=')[-1]]):
            shutil.move(plot, directory + '/' + plot.split('/')[-1])

            plotfile = plot.split('/')[-1]
            plot_ref = (plot.split('__')[-2]).split('=')[-1]

            release_web_page.write('        <li><a href="!#' + plot_ref + '">' + plot_ref + '</a></li>\n')

        release_web_page.write('      </ul>\n')
        release_web_page.write('    </div>\n')

        plots = glob.glob(directory + '/' + fileloc + '.png')
        for plot in sorted(plots, key=lambda x: myorderdict[(
                x.split('__')[-2]).split('=')[-1]]):
            plotfile = plot.split('/')[-1]
            plotfile_pdf = plotfile.replace('png', 'pdf')

            plotfile = plot.split('/')[-1]
            plot_ref = (plot.split('__')[-2]).split('=')[-1]

            release_web_page.write('  \n')
            release_web_page.write('  <div style="padding:10px;width:1000px;height:450px">\n')
            release_web_page.write('    <a name="' + plot_ref + '"><strong>Plot: ' + plot_ref + '</strong></a><br/>\n')
            release_web_page.write('    <div style="float:left;width:500px;padding:10px">\n')
            release_web_page.write('      <a href="' + plotfile + '"><img alt="' + plot.split( '/')[-1] + '" width="500" height="400" src="' + plotfile + '"/></a>\n')
            release_web_page.write('    </div>')

            release_web_page.write('    <div style="float:right;width:450px;font-size:80%;color:gray;padding:10px">\n')

            release_web_page.write('      <strong>Direct links:</strong> <a href="!">this page</a> / <a href="!#' + plot_ref + '">this plot</a><p/>\n')
            release_web_page.write('      <strong>Other formats:</strong> <a href="' + plotfile_pdf + '">PDF</a> \n')
            release_web_page.write('    </div>\n')
            release_web_page.write('  </div><br/>\n')

        self.Build_logsF(release_web_page)
        return

    def Build_table_WebPage(self, tableName='component', fieldName='cpu', domainName='domain', plotXlabel='Date', plotYlabel='Time per event (ms)',
                            cmd='c.stage=\'[evt]\'', plotTitle='Times in event loop', desc='This page has plots detailing where the time is spent in the Athena event loops. By assigning the algorithms into a few domains as listed in the job output file, results are shown both overall by domain as well as  a more detailed view within a given domain where applicable.<p/>For a look at wall-time and the time spent outside the event loop, have a look at the <em>timings</em> pages.<p/>\n'):

        jobName = self.name

        self.makeDomainPlot(
            tableName,
            fieldName,
            domainName,
            'Breakdown per ' +
            domainName,
            plotXlabel,
            plotYlabel)

        domlist = self.GetOrderedListOfDomains(
            fieldName,
            domainName,
            tableName,
            'j.name=\'' + self.name + '\' AND j.platform=\'' + self.platforms[-1] + '\' AND j.release=\'' + self.releases[-1] + '\'')

        for dom in domlist:
            self.makeTopTen(
                tableName,
                fieldName,
                cmd + ' AND c.' + domainName + '=\'' + str(dom) + '\'',
                'Top Ten ' + plotTitle + ' : ' + str(dom),
                plotXlabel,
                plotYlabel)

        self.makeTopTen(
            tableName,
            fieldName,
            cmd,
            plotTitle,
            plotXlabel,
            plotYlabel)
#        self.makeTopTen('component','maxcpu','c.stage=\'[evt]\'','Outlier Time','Date','Time (ms)')

        directory = self.pathname + "/arch-mon-" + jobName + \
            "/pages_custom/" + fieldName + "/build_and_stream_comparisons"

        if len(self.list_of_commands) == 1:
            myCommand = self.list_of_commands[0]
            myC = re.split('\'', myCommand)
            newMyC = [x for x in myC if 'j.' not in x and x != '']
            jobRelease = newMyC[0]
            jobPlatform = newMyC[1]
            directory = self.pathname + "/arch-mon-" + jobName + \
                "/pages_custom/" + fieldName + "/" + jobRelease + "__" + jobPlatform

        if not os.path.exists(directory):
            os.makedirs(directory)

        myfile = directory + '/html.snippet'

        release_web_page = open(myfile, 'w')
        release_web_page.write('<div style="width:900px;font-size:80%;color:gray;padding:10px">\n')
        release_web_page.write('    <div style="width:900px;font-size:80%;color:gray;padding:10px">\n')
        release_web_page.write(desc)
        release_web_page.write('    </div>\n')
        release_web_page.write('    <p/><strong>Plots on this page:</strong>\n')
        release_web_page.write('    <div style="width:1000px;column-count:3;-moz-column-count:3;-webkit-column-count:3">\n')
        release_web_page.write('      <ul style="font-size:80%">\n')
        import glob

        for step in self.steps:
            fileloc = self.name + '__comparison__' + step + '__' + '*' + '__' + fieldName
            if len(self.list_of_commands) == 1:
                fileloc = self.name + '__' + jobPlatform + '__' + \
                    jobRelease + '__' + step + '__' + '*' + '__' + fieldName

            plots = glob.glob(fileloc + '.png')
            plots_pdf = glob.glob(fileloc + '.pdf')

            for plot in plots_pdf:
                shutil.move(plot, directory + '/' + plot.split('/')[-1])
            for plot in plots:
                shutil.move(plot, directory + '/' + plot.split('/')[-1])

                plotfile = plot.split('/')[-1]
                plot_ref = step + '/' + (plot.split('__')[-2]).split('=')[-1]

                release_web_page.write('        <li><a href="!#' + plot_ref + '">' + plot_ref + '</a></li>\n')

        release_web_page.write('      </ul>\n')
        release_web_page.write('    </div>\n')

        for step in self.steps:

            fileloc = self.name + '__comparison__' + step + '__' + '*' + '__' + fieldName
            if len(self.list_of_commands) == 1:
                fileloc = self.name + '__' + jobPlatform + '__' + \
                    jobRelease + '__' + step + '__' + '*' + '__' + fieldName

            plots = glob.glob(directory + '/' + fileloc + '.png')
            for plot in plots:

                plotfile = plot.split('/')[-1]
                plotfile_pdf = plotfile.replace('png', 'pdf')
                plotfile = plot.split('/')[-1]
                plot_ref = step + '/' + (plot.split('__')[-2]).split('=')[-1]

                release_web_page.write('  \n')
                release_web_page.write('  <div style="padding:10px;width:1000px;height:450px">\n')
                release_web_page.write('    <a name="' + plot_ref + '"><strong>Plot: ' + plot_ref + '</strong></a><br/>\n')
                release_web_page.write('    <div style="float:left;width:500px;padding:10px">\n')
                release_web_page.write('      <a href="' + plotfile + '"><img alt="' + plot.split( '/')[-1] + '" width="500" height="400" src="' + plotfile + '"/></a>\n')
                release_web_page.write('    </div>\n')

                release_web_page.write('    <div style="float:right;width:450px;font-size:80%;color:gray;padding:10px">\n')

                release_web_page.write('      <strong>Direct links:</strong> <a href="!">this page</a> / <a href="!#' + plot_ref + '">this plot</a><p/>\n')
                release_web_page.write('      <strong>Other formats:</strong> <a href="' + plotfile_pdf + '">PDF</a> \n')
                release_web_page.write('    </div>\n')
                release_web_page.write('  </div><br/>\n')

        self.Build_logsF(release_web_page)

        return

    # FUNCTION TO GATHER JOB LOGS AND JOB COMMANDS FROM THE ARCHIVE AND COPY
    # THEM TO THE WEB AREA
    def Copy_logs_commands(self):

        from shutil import copyfile
        import subprocess

        jobName = self.name
        logs_directory = self.pathname + "/arch-mon-" + jobName + "/pages_custom/logs"
        commands_directory = self.pathname + "/arch-mon-" + \
            jobName + "/pages_custom/commands"

        if not os.path.exists(logs_directory):
            os.makedirs(logs_directory)

        if not os.path.exists(commands_directory):
            os.makedirs(commands_directory)

        # Write LOG BOX
        MyDataX = []
        MyDataRel = []
        MyDataPlat = []
        MyDataStep = []

        for step in self.steps:

            jobName = self.name
            jobStep = step

            for myCommand in self.list_of_commands:
                myCmd = ' SELECT DISTINCT j.nightly,j.release,j.platform,j.step FROM job j WHERE j.name=\'' + jobName + '\' AND j.step=\'' + \
                    jobStep + '\' AND ' + myCommand + \
                        ' GROUP BY j.date ORDER BY j.date desc LIMIT ' + \
                    str(self.days) + ' ;'
                newNightlies = self.GetListOfNames('nightly', myCmd)
                newReleases = self.GetListOfNames('release', myCmd)
                newPlatforms = self.GetListOfNames('platform', myCmd)
                newSteps = self.GetListOfNames('step', myCmd)
                MyDataX.extend(newNightlies)
                MyDataRel.extend(newReleases)
                MyDataPlat.extend(newPlatforms)
                MyDataStep.extend(newSteps)

            for i in range(len(MyDataX)):
                splits = MyDataX[i].split('-')
                if len(splits) < 3:
                    continue
                year = splits[0]
                month = splits[1]
                day = splits[2]
                day = day.split('T')[0]

                jobName = self.name
                jobStep = MyDataStep[i]
                jobRelease = MyDataRel[i]
                jobPlatform = MyDataPlat[i]
                jobStep = MyDataStep[i]

                xdir = '/eos/atlas/user/a/atlaspmb/archive/custom/%s/%s/%s/%s/%s/%s' % (
                    day, month, year, jobRelease, jobPlatform, jobName)
                x = '/eos/atlas/user/a/atlaspmb/archive/custom/%s/%s/%s/%s/%s/%s/log.%s' % (
                    day, month, year, jobRelease, jobPlatform, jobName, step)

                # COPY FILE TO EOS WEB DIRECTORY
                destination = logs_directory + \
                    '/%s-%s-%s-%s-%s-%s-%s' % (day,
                                               month,
                                               year,
                                               jobRelease,
                                               jobPlatform,
                                               jobName,
                                               step)

                try:
                    copyfile(x, destination)
                except BaseException:
                    print("Failed to copy {} to {}".format(x, destination))

                # Get COMMAND
                cmd = 'zgrep "Reco_tf" ' + xdir + \
                    r'/__command.sh | sed "s/\/build1/root:\/\/eosatlas\/\/eos\/atlas\/user\/a/g;"'
                if 'simulation' in jobName:
                    cmd = 'zgrep "Sim_tf" ' + xdir + \
                        r'/__command.sh | sed "s/\/build1/root:\/\/eosatlas\/\/eos\/atlas\/user\/a/g;"'
                elif 'generation' in jobName:
                    cmd = 'zgrep "Generate_tf" ' + xdir + \
                        r'/__command.sh | sed "s/\/build1/root:\/\/eosatlas\/\/eos\/atlas\/user\/a/g;"'
                output, error = subprocess.Popen(
                    ['/bin/bash', '-c', cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
                output, error = output.decode('utf-8'), error.decode('utf-8')
                myfilecmd = commands_directory + \
                    '/%s-%s-%s-%s-%s-%s-%s' % (day,
                                               month,
                                               year,
                                               jobRelease,
                                               jobPlatform,
                                               jobName,
                                               step)

                cmdf = open(myfilecmd, 'w')
                cmdf.write(output)

        return


# FOOTER OF ALL WEB PAGES SHOWING LINKS TO LOGS, COMMANDS AND RELEASE
# DIFFERENCES

    def Build_logsF(self, htmlfile):  # undone
        if len(self.list_of_commands) == 1:
            myCommand = self.list_of_commands[0]
            myC = re.split('\'', myCommand)
            newMyC = [x for x in myC if 'j.' not in x and x != '']
            jobRelease = newMyC[0]
            jobPlatform = newMyC[1]

            htmlfile.write(
                '<p style="padding-top:100px"/><a name="jobinfo"><strong>Details of concerned jobs:</strong></a>\n')
            htmlfile.write(
                '<table style="font-size:100%;border:thin solid gray;background-color:rgb(240,240,240);text-align:left;white-space:nowrap;">\n')
            htmlfile.write(
                '    <tr><th style="text-align:center;color:white;background-color:rgb(10,128,192);" colspan="5">Job information</th></tr>\n')
            htmlfile.write(
                '    <tr style="color:white;background-color:rgb(54,189,189);"><th>Date</th><th>Release</th><th>Platform</th><th>Step</th><th>Command</th></tr>\n')

            jobField = {}
            for step in self.steps:

                jobName = self.name
                jobStep = step
                jobField[step] = []

                myCmd = ' SELECT j.step,j.date FROM job j WHERE j.name=\'' + jobName + '\' AND j.step=\'' + jobStep + \
                    '\' AND ' + \
                        self.list_of_commands[-1] + ' GROUP BY j.date ORDER BY j.date desc LIMIT ' + str(
                            self.days) + ' ;'
                jobField[step] = self.GetListOfNames('date', myCmd)

                for mydata in jobField[step]:
                    splits = mydata.split('-')
                    if len(splits) < 3:
                        continue
                    year = splits[0]
                    month = splits[1]
                    day = splits[2]
                    x = '/eos/atlas/user/a/atlaspmb/archive/custom/%s/%s/%s/%s/%s/%s/log.%s' % (
                        day, month, year, jobRelease, jobPlatform, jobName, step)

                    #Date = mydata

                    htmlfile.write('    <tr style="font-size:80%">\
                    <th>' + str(mydata) + '</th>\
                    <th style="text-align:center"><span style="color:green">' + jobRelease + '</span></th>\
                    <th style="text-align:center"><span style="color:green">' + jobPlatform + '</span></th>\
                    <th style="text-align:center"><a style="color:black;" href="../../logs/' + day + '-' + month + '-' + year + '-' + jobRelease + '-' + jobPlatform + '-' + jobName + '-' + jobStep + '" target="_blank">' + 'log.' + jobStep + '</a></th> \
<th style="text-align:center"><a style="color:black;" href="../../commands/' + day + '-' + month + '-' + year + '-' + jobRelease + '-' + jobPlatform + '-' + jobName + '-' + jobStep + '" target="_blank">' + 'cmd.' + jobStep + '</a></th></tr>\n')
                htmlfile.write(
                    '<tr class="blank_row"><td colspan="3"></td></tr>\n')
            htmlfile.write('</table>\n')
            htmlfile.write('</p>\n')
            htmlfile.write('      </ul>\n')
            htmlfile.write('    </div>\n')

            htmlfile.write('')
            htmlfile.write(
                '<p style="padding-top:100px"/><a name="jobinfo"><strong>Release differences :</strong></a>\n')
            htmlfile.write('  <div style="width:900px;padding:10px;color:gray;font-size:80%">The following table contains nightly release differences for builds where jobs used on this page were running.\n')
            htmlfile.write(
                '<table style="font-size:100%;border:thin solid gray;background-color:rgb(240,240,240);text-align:left;white-space:nowrap;">\n')
            htmlfile.write(
                '<table style="font-size:100%;border:thin solid gray;background-color:rgb(240,240,240);text-align:left;white-space:nowrap;">\n')
            htmlfile.write(
                '      <tr><th style="text-align:center;color:white;background-color:rgb(10,128,192);" colspan="4">Sample release differences</th></tr>\n')
            htmlfile.write('      <tr style="color:white;background-color:rgb(54,189,189);"><th>Build 1</th><th>Build 2</th><th style="text-align:center">Release Differences</th><th style="text-align:center">Commit Differences</th></tr>\n')

            for step in self.steps:

                jobName = self.name
                jobStep = step

                myCmd = ' SELECT j.step,j.date,j.nightly FROM job j WHERE j.name=\'' + jobName + '\' AND j.step=\'' + jobStep + \
                    '\' AND ' + \
                        self.list_of_commands[-1] + ' GROUP BY j.date ORDER BY j.date desc LIMIT ' + str(
                            self.days) + ' ;'
                myListRelease = self.GetListOfNames('nightly', myCmd)
                jobField[step] = self.GetListOfNames('date', myCmd)

                for i in range(len(myListRelease) - 1):
                    htmlfile.write('\n')
                    d1 = str(myListRelease[i])
                    d2 = str(myListRelease[i + 1])
                    d1s = myListRelease[i].split('-')
                    d2s = myListRelease[i + 1].split('-')
                    if len(d1s) < 3:
                        continue
                    if len(d2s) < 3:
                        continue
                    d1n = d1s[1] + '-' + d1s[2].split('T')[0]
                    d2n = d2s[1] + '-' + d2s[2].split('T')[0]

                    reldiff = 'https://test-atrvshft.web.cern.ch/test-atrvshft/tools/cgi-bin/release_notes_nightly.pl?nightly_new=nightly%2F' + \
                        jobRelease + '%2F' + \
                        str(d1) + '&nightly_old=nightly%2F' + jobRelease + \
                        '%2F' + str(d2) + '&submit=Submit+Request'
                    gitdiff = 'https://gitlab.cern.ch/atlas/athena/compare/nightly%2F' + \
                        jobRelease + '%2F' + \
                        str(d2) + '...nightly%2F' + \
                        jobRelease + '%2F' + str(d1)
                    htmlfile.write((' <tr style="font-size:100%">'
                                    '<th style="text-align:center">' + jobRelease + '/' + d2n +
                                    '</th><th style="text-align:center">' + jobRelease + '/' + d1n + '</th>'
                                    '<th style="text-align:center"><a href="' + reldiff + '">link</a>&nbsp;</th>'
                                    '<th style="text-align:center"><a href="' + gitdiff + '">link</a>&nbsp;</th>'
                                    ' </tr>'))

            htmlfile.write('  </table>\n')
            htmlfile.write('    <div style="height:100px"></div>\n')

        else:

            # Write LOG BOX
            MyDataX = []
            MyDataRel = []
            MyDataPlat = []
            MyDataStep = []

            htmlfile.write(
                '<p style="padding-top:100px"/><a name="jobinfo"><strong>Details of concerned jobs:</strong></a>\n')
            htmlfile.write(
                '<table style="font-size:100%;border:thin solid gray;background-color:rgb(240,240,240);text-align:left;white-space:nowrap;">\n')
            htmlfile.write(
                '    <tr><th style="text-align:center;color:white;background-color:rgb(10,128,192);" colspan="5">Job information</th></tr>\n')
            htmlfile.write(
                '    <tr style="color:white;background-color:rgb(54,189,189);"><th>Date</th><th>Release</th><th>Platform</th><th>Step</th><th>Command</th></tr>')

            for step in self.steps:

                jobName = self.name
                jobStep = step

                for myCommand in self.list_of_commands:
                    myCmd = ' SELECT DISTINCT j.nightly,j.release,j.platform,j.step FROM job j WHERE j.name=\'' + jobName + '\' AND j.step=\'' + \
                        jobStep + '\' AND ' + myCommand + \
                            ' GROUP BY j.date ORDER BY j.date desc LIMIT ' + \
                        str(self.days) + ' ;'
                    newNightlies = self.GetListOfNames('nightly', myCmd)
                    newReleases = self.GetListOfNames('release', myCmd)
                    newPlatforms = self.GetListOfNames('platform', myCmd)
                    newSteps = self.GetListOfNames('step', myCmd)
                    MyDataX.extend(newNightlies)
                    MyDataRel.extend(newReleases)
                    MyDataPlat.extend(newPlatforms)
                    MyDataStep.extend(newSteps)

            for i in range(len(MyDataX)):
                splits = MyDataX[i].split('-')
                if len(splits) < 3:
                    continue
                year = splits[0]
                month = splits[1]
                day = splits[2].split('T')[0]

                jobName = self.name
                jobStep = MyDataStep[i]
                jobRelease = MyDataRel[i]
                jobPlatform = MyDataPlat[i]
                x = '/eos/atlas/user/a/atlaspmb/archive/custom/%s/%s/%s/%s/%s/%s/log.%s' % (
                    day, month, year, jobRelease, jobPlatform, jobName, step)

                htmlfile.write('    <tr style="font-size:80%">\
                    <th>' + str(MyDataX[i]) + '</th>\
                    <th style="text-align:center"><span style="color:green">' + jobRelease + '</span></th>\
                    <th style="text-align:center"><span style="color:green">' + jobPlatform + '</span></th>\
                    <th style="text-align:center"><a style="color:black;" href="../../logs/' + day + '-' + month + '-' + year + '-' + jobRelease + '-' + jobPlatform + '-' + jobName + '-' + jobStep + '" target="_blank">' + 'log.' + jobStep + '</a></th> \
<th style="text-align:center"><a style="color:black;" href="../../commands/' + day + '-' + month + '-' + year + '-' + jobRelease + '-' + jobPlatform + '-' + jobName + '-' + jobStep + '" target="_blank">' + 'cmd.' + jobStep + '</a></th></tr>\n')
                htmlfile.write(
                    '<tr class="blank_row"><td colspan="3"></td></tr>\n')

            htmlfile.write('</table>\n')
            htmlfile.write('</p>\n')
            htmlfile.write('      </ul>\n')
            htmlfile.write('    </div>\n')

            htmlfile.write('')
            htmlfile.write(
                '<p style="padding-top:100px"/><a name="jobinfo"><strong>Release differences :</strong></a>\n')
            htmlfile.write('  <div style="width:900px;padding:10px;color:gray;font-size:80%">The following table contains nightly release differences for builds where jobs used on this page were running.\n')
            htmlfile.write(
                '<table style="font-size:100%;border:thin solid gray;background-color:rgb(240,240,240);text-align:left;white-space:nowrap;">\n')
            htmlfile.write(
                '<table style="font-size:100%;border:thin solid gray;background-color:rgb(240,240,240);text-align:left;white-space:nowrap;">\n')
            htmlfile.write(
                '      <tr><th style="text-align:center;color:white;background-color:rgb(10,128,192);" colspan="4">Sample release differences</th></tr>\n')
            htmlfile.write('      <tr style="color:white;background-color:rgb(54,189,189);"><th>Build 1</th><th>Build 2</th><th style="text-align:center">Release Differences</th><th style="text-align:center">Commit Differences</th></tr>\n')

            # Write TAG DIFFERENCE BOX

            MyDataX = []
            MyDataRel = []
            MyDataPlat = []
            for myCommand in self.list_of_commands:
                jobName = self.name
                myCmd = ' SELECT DISTINCT j.nightly,j.release,j.platform FROM job j WHERE j.name=\'' + jobName + \
                    '\' AND ' + myCommand + ' GROUP BY j.date ORDER BY j.date desc LIMIT ' + \
                        str(self.days) + ' ;'
                newNightlies = self.GetListOfNames('nightly', myCmd)
                newReleases = self.GetListOfNames('release', myCmd)
                newPlatforms = self.GetListOfNames('platform', myCmd)
                MyDataX.extend(newNightlies)
                MyDataRel.extend(newReleases)
                MyDataPlat.extend(newPlatforms)

            for i in range(len(MyDataX)):
                for j in range(i + 1, len(MyDataX)):

                    # Get the releases
                    rel1 = str(MyDataRel[i])
                    rel2 = str(MyDataRel[j])

                    # Only compare the same releases with each other
                    if rel1 != rel2:
                        continue

                    # Get the platforms
                    platform1 = str(MyDataPlat[i])
                    platform2 = str(MyDataPlat[j])

                    # Only compare the same platforms with each other
                    if platform1 != platform2:
                        continue

                    # Get the compilers
                    comp1 = platform1.split('-')[2]
                    comp2 = platform2.split('-')[2]

                    # Get the nightlies
                    nightly1 = str(MyDataX[i])
                    nightly2 = str(MyDataX[j])

                    d1s = MyDataX[i].split('-')
                    d2s = MyDataX[j].split('-')
                    if len(d1s) < 3:
                        continue
                    if len(d2s) < 3:
                        continue
                    d1n = d1s[1] + '-' + d1s[2].split('T')[0]
                    d2n = d2s[1] + '-' + d2s[2].split('T')[0]

                    reldiff = 'https://test-atrvshft.web.cern.ch/test-atrvshft/tools/cgi-bin/release_notes_nightly.pl?nightly_new=nightly%2F' + \
                        rel1 + '%2F' + str(nightly1) + '&nightly_old=nightly%2F' + \
                        rel2 + '%2F' + str(nightly2) + '&submit=Submit+Request'
                    gitdiff = 'https://gitlab.cern.ch/atlas/athena/compare/nightly%2F' + rel2 + \
                        '%2F' + str(nightly2) + '...nightly%2F' + \
                        rel1 + '%2F' + str(nightly1)
                    htmlfile.write((' <tr style="font-size:100%">'
                                    '<th style="text-align:center">' + rel2 + '/' + d2n + ' (' + comp2 + ')' +
                                    '</th><th style="text-align:center">' + rel1 + '/' + d1n + ' (' + comp1 + ')</th>'
                                    '<th style="text-align:center"><a href="' + reldiff + '">link</a>&nbsp;</th>'
                                    '<th style="text-align:center"><a href="' + gitdiff + '">link</a>&nbsp;</th>'
                                    ' </tr>\n'))

            htmlfile.write('  </table>\n')
            htmlfile.write('    <div style="height:100px"></div>\n')

        return

    def Build_All_WebPages(self):
        if not self.IsValid():
            print("No entries to process within the last days for this job type")
            return
        self.Copy_logs_commands()
        self.Build_home_WebPage()
        self.Build_timing_WebPage()
        self.Build_memory_WebPage()
        self.Build_table_WebPage(
            'component',
            'cpu',
            'domain',
            'Date',
            'Time per event (ms)',
            'c.stage=\'[evt]\'',
            'Times in event loop',
            'This page has plots detailing where the time is spent in the Athena event loops. By assigning the algorithms into a few domains as listed in the job output file, results are shown both overall by domain as well as  a more detailed view within a given domain where applicable.<p/>For a look at wall-time and the time spent outside the event loop, have a look at the <em>timings</em> pages.<p/>\n')
        self.Build_table_WebPage(
            'component',
            'malloc',
            'stage',
            'Date',
            'Memory (MB)',
            'c.stage!=\'[---]\'',
            'Memory allocated',
            'This page has plots detailing how much physical memory is allocated. This is shown for each stage in an Athena step.<p/>\n')
        self.Build_table_WebPage(
            'component',
            'vmem',
            'stage',
            'Date',
            'Memory (MB)',
            'c.stage!=\'[---]\'',
            'Memory allocated',
            'This page has plots detailing how much virtual memory is allocated. This is shown for each stage in an Athena step.<p/>\n')
        self.Build_table_WebPage(
            'container',
            'sizeperevt',
            'domain',
            'Date',
            'Size per event (KB)',
            'c.domain!=\'all\'',
            'Container Sizes',
            'This page has plots detailing what is stored in Athena outputs. By assigning the containers into domains as listed in the job output file, results are shown both overall by domain as well as a more detailed view within a given domain where applicable.<p/>\n')
