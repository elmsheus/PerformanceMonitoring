#!/bin/bash

# Archive dir
ARCHIVE_DIR="${HOME}/eos_atlaspmb/archive/custom"

# Web dir
# The actual EOS path doesn't work w/ mkdir -p
# Therefore, I'm using a soft-link from ${HOME} instead.
# See JIRA EOS-4364
WEB_DIR="${HOME}/www_atlaspmb"

# Execute the specific job

# mc21a full-chain MC
run_mc21a() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}
    DATADIR="/data/atlaspmb/spot-job-inputs"

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --inputHITSFile="${DATADIR}/mc21_13p6TeV.601237.PhPy8EG_A14_ttbar_hdamp258p75_allhad.merge.HITS.e8453_e8455_s3873_s3874/HITS.29625936._002926.pool.root.1" \
      --inputRDO_BKGFile="${DATADIR}/mc21_13p6TeV.900149.PG_single_nu_Pt50.merge.RDO.e8453_e8455_s3864_d1761_d1758/RDO.29616553._005014.pool.root.1" \
      --outputAODFile="myAOD.pool.root" \
      --perfmon "fullmonmt" \
      --maxEvents=${NEVENTS} \
      --multithreaded="True" \
      --preExec "RAWtoALL:rec.doDetailedPerfMonMT=True;" \
      --preInclude "all:Campaigns/MC21a.py" \
      --postInclude "default:PyJobTransforms/UseFrontier.py" \
      --skipEvents="0" \
      --autoConfiguration="everything" \
      --conditionsTag "default:OFLCOND-MC21-SDR-RUN3-07" \
      --geometryVersion="default:ATLAS-R3S-2021-03-00-00" \
      --runNumber="601237" \
      --digiSeedOffset1="232" \
      --digiSeedOffset2="232" \
      --AMITag="r13768" \
      --steering "doOverlay" "doRDO_TRIG" "doTRIGtoALL" > __log.txt 2>&1

    # Get the exit code
    echo $? > __exitcode;

}

# q445 based full-chain MC
run_q445() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --AMI 'q445' \
      --perfmon 'fullmonmt' \
      --maxEvents ${NEVENTS} \
      --outputAODFile 'myAOD.pool.root' \
      --multithreaded 'True' \
      --deleteIntermediateOutputfiles 'True' \
      --preExec 'HITtoRDO:from PerfMonComps.PerfMonFlags import jobproperties as jp; jp.PerfMonFlags.doFullMonMT=True; jp.PerfMonFlags.OutputJSON="perfmonmt_HITtoRDO.json";' \
                'RAWtoALL:rec.doDetailedPerfMonMT=True;' \
      --preInclude 'HITtoRDO:PerfMonComps/PerfMonMTSvc_jobOptions.py' \
      --postExec 'all:svcMgr.PerfMonMTSvc.memFitLowerLimit=300; svcMgr.AlgResourcePool.CountAlgorithmInstanceMisses=True;' \
      --steering doRDO_TRIG doTRIGtoALL > __log.txt 2>&1

    # Get the exit code
    echo $? > __exitcode;

}

# RDOtoRDOTrigger from q221
run_r2rt() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} \
    Reco_tf.py \
      --inputRDOFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TriggerTest/valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.RDO.e4993_s3214_r11315/RDO.17533168._000001.pool.root.1' \
      --AMI 'q221' \
      --perfmon 'fullmonmt' \
      --conditionsTag 'OFLCOND-MC16-SDR-RUN2-09' \
      --maxEvents ${NEVENTS} \
      --outputRDO_TRIGFile 'myRDOTrigger.pool.root' \
      --multithreaded 'True' \
      --preExec 'RDOtoRDOTrigger:' \
      --postExec 'RDOtoRDOTrigger:svcMgr.AlgResourcePool.CountAlgorithmInstanceMisses=True;' \
      --steering doRDO_TRIG > __log.txt 2>&1

    # Get the exit code
    echo $? > __exitcode;

}

# RAWtoALL + DQ
run_r2a_data22() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    if [[ "${NTHREADS}" == "1" ]];
    then
        ATHENA_CORE_NUMBER=1 \
        Reco_tf.py  \
          --inputBSFile '/data/atlaspmb/spot-job-inputs/data22_13p6TeV/data22_13p6TeV.00429782.physics_Main.daq.RAW._lb0562._SFO-11._0001.data' \
          --maxEvents ${NEVENTS} \
          --outputAODFile 'myAOD.pool.root' \
          --outputHISTFile 'myHIST.root' \
          --multithreaded 'True' \
          --preExec 'all:from RecExConfig.RecFlags import rec; rec.doZdc.set_Value_and_Lock(False); from AthenaConfiguration.AllConfigFlags import ConfigFlags; ConfigFlags.Trigger.triggerConfig="DB"; ConfigFlags.DQ.Steering.HLT.doBjet=True; ConfigFlags.DQ.Steering.HLT.doInDet=True; ConfigFlags.DQ.Steering.HLT.doBphys=True; ConfigFlags.DQ.Steering.HLT.doCalo=True; ConfigFlags.DQ.Steering.HLT.doEgamma=True; ConfigFlags.DQ.Steering.HLT.doMET=True; ConfigFlags.DQ.Steering.HLT.doJet=True; ConfigFlags.DQ.Steering.HLT.doMinBias=True; ConfigFlags.DQ.Steering.HLT.doMuon=True; ConfigFlags.DQ.Steering.HLT.doTau=True;' 'RAWtoALL:from PerfMonComps.PerfMonFlags import jobproperties as jp; jp.PerfMonFlags.doFullMonMT=True; jp.PerfMonFlags.OutputJSON="perfmonmt_RAWtoALL.json"' \
          --postExec 'all:from AthenaCommon.AlgScheduler import AlgScheduler; AlgScheduler.CheckDependencies(True); AlgScheduler.ShowControlFlow(True); AlgScheduler.ShowDataDependencies(True); svcMgr.AlgResourcePool.CountAlgorithmInstanceMisses=True;' \
          --postInclude 'all:PerfMonComps/PerfMonMTSvc_jobOptions.py,RecExCommon/ValgrindTweaks.py' \
          --autoConfiguration 'everything' \
          --conditionsTag 'CONDBR2-BLKPA-2022-06' \
          --geometryVersion 'ATLAS-R3S-2021-03-00-00' \
          --runNumber '429782' \
          --steering 'doRAWtoALL'  > __log.txt 2>&1
    else
        ATHENA_CORE_NUMBER=${NTHREADS} \
        Reco_tf.py  \
          --inputBSFile '/data/atlaspmb/spot-job-inputs/data22_13p6TeV/data22_13p6TeV.00429782.physics_Main.daq.RAW._lb0562._SFO-11._0001.data' \
          --maxEvents ${NEVENTS} \
          --outputAODFile 'myAOD.pool.root' \
          --outputHISTFile 'myHIST.root' \
          --multithreaded 'True' \
          --preExec 'all:from RecExConfig.RecFlags import rec; rec.doZdc.set_Value_and_Lock(False); from AthenaConfiguration.AllConfigFlags import ConfigFlags; ConfigFlags.Trigger.triggerConfig="DB"; ConfigFlags.DQ.Steering.HLT.doBjet=True; ConfigFlags.DQ.Steering.HLT.doInDet=True; ConfigFlags.DQ.Steering.HLT.doBphys=True; ConfigFlags.DQ.Steering.HLT.doCalo=True; ConfigFlags.DQ.Steering.HLT.doEgamma=True; ConfigFlags.DQ.Steering.HLT.doMET=True; ConfigFlags.DQ.Steering.HLT.doJet=True; ConfigFlags.DQ.Steering.HLT.doMinBias=True; ConfigFlags.DQ.Steering.HLT.doMuon=True; ConfigFlags.DQ.Steering.HLT.doTau=True; rec.doDetailedPerfMonMT = True;' \
          --postExec 'all:from AthenaCommon.AlgScheduler import AlgScheduler; AlgScheduler.CheckDependencies(True); AlgScheduler.ShowControlFlow(True); AlgScheduler.ShowDataDependencies(True); svcMgr.PerfMonMTSvc.memFitLowerLimit = 300; svcMgr.AlgResourcePool.CountAlgorithmInstanceMisses=True;' \
          --autoConfiguration 'everything' \
          --conditionsTag 'CONDBR2-BLKPA-2022-06' \
          --geometryVersion 'ATLAS-R3S-2021-03-00-00' \
          --runNumber '429782' \
          --steering 'doRAWtoALL'  > __log.txt 2>&1
    fi

    # Get the exit code
    echo $? > __exitcode;

}


run_r2a_data18() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    if [[ "${NTHREADS}" == "1" ]];
    then
        ATHENA_CORE_NUMBER=1 \
        Reco_tf.py \
          --inputBSFile '/data/atlaspmb/spot-job-inputs/data18_13TeV/data18_13TeV.00357750.physics_Main.daq.RAW._lb0105._SFO-4._0002.data' \
          --maxEvents ${NEVENTS} \
          --outputAODFile 'myAOD.pool.root' \
          --outputHISTFile 'myHIST.pool.root' \
          --multithreaded 'True' \
          --preExec 'all:from AthenaMonitoring.DQMonFlags import DQMonFlags; DQMonFlags.doMonitoring=True; DQMonFlags.doNewMonitoring=True; DQMonFlags.doHLTMon=False;' 'RAWtoALL:from PerfMonComps.PerfMonFlags import jobproperties as jp; jp.PerfMonFlags.doFullMonMT=True; jp.PerfMonFlags.OutputJSON="perfmonmt_RAWtoALL.json"' \
          --postExec 'all:from AthenaCommon.AlgScheduler import AlgScheduler; AlgScheduler.CheckDependencies(True); AlgScheduler.ShowControlFlow(True); AlgScheduler.ShowDataDependencies(True); svcMgr.AlgResourcePool.CountAlgorithmInstanceMisses=True;' \
          --postInclude 'all:PerfMonComps/PerfMonMTSvc_jobOptions.py,RecExCommon/ValgrindTweaks.py' \
          --autoConfiguration 'everything' \
          --conditionsTag 'all:CONDBR2-BLKPA-RUN2-09' \
          --geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
          --runNumber '357750' \
          --steering 'doRAWtoALL' > __log.txt 2>&1
    else
        ATHENA_CORE_NUMBER=${NTHREADS} \
        Reco_tf.py \
          --inputBSFile '/data/atlaspmb/spot-job-inputs/data18_13TeV/data18_13TeV.00357750.physics_Main.daq.RAW._lb0105._SFO-4._0002.data' \
          --maxEvents ${NEVENTS} \
          --outputAODFile 'myAOD.pool.root' \
          --outputHISTFile 'myHIST.pool.root' \
          --multithreaded 'True' \
          --preExec 'all:from AthenaMonitoring.DQMonFlags import DQMonFlags; DQMonFlags.doMonitoring=True; DQMonFlags.doNewMonitoring=True; DQMonFlags.doHLTMon=False; rec.doDetailedPerfMonMT = True;' \
          --postExec 'all:from AthenaCommon.AlgScheduler import AlgScheduler; AlgScheduler.CheckDependencies(True); AlgScheduler.ShowControlFlow(True); AlgScheduler.ShowDataDependencies(True); svcMgr.PerfMonMTSvc.memFitLowerLimit = 300; svcMgr.AlgResourcePool.CountAlgorithmInstanceMisses=True;' \
          --autoConfiguration 'everything' \
          --conditionsTag 'all:CONDBR2-BLKPA-RUN2-09' \
          --geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
          --runNumber '357750' \
          --steering 'doRAWtoALL' > __log.txt 2>&1
    fi

    # Get the exit code
    echo $? > __exitcode;

}

# ITk simulation
run_e2h_itk() {

    # Parse the inputs
    NTHREADS=${1}
    NEVENTS=${2}

    # Run the job
    export TRF_ECHO=1;
    ATHENA_CORE_NUMBER=${NTHREADS} Sim_tf.py \
      --CA \
      --inputEVNTFile '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/inputs/EVNT.01485091._001049.pool.root.1' \
      --maxEvents ${NEVENTS} \
      --outputHITSFile 'myHITS.pool.root' \
      --multithreaded 'True' \
      --perfmon 'fullmonmt' \
      --conditionsTag 'default:OFLCOND-MC15c-SDR-14-05' \
      --physicsList 'FTFP_BERT_ATL' \
      --truthStrategy 'MC15aPlus' \
      --simulator 'FullG4MT' \
      --postInclude 'default:PyJobTransforms.UseFrontier' \
      --preInclude 'EVNTtoHITS:SimuJobTransforms.BeamPipeKill,SimuJobTransforms.FrozenShowersFCalOnly,SimuJobTransforms.TightMuonStepping' \
      --DataRunNumber '242000' \
      --geometryVersion 'default:ATLAS-P2-ITK-24-00-00' > __log.txt 2>&1

    # Get the exit code
    echo $? > __exitcode;

}

# Define and execute the test
execute() {

    # Setup environment
    source ~/.bashrc;
    source ~/.bash_profile;

    # Define test parameters
    JOBNAME="${1}";
    JOBRELEASE="${2}";
    JOBPLATFORM="${3}";

    echo "${JOBNAME} - ${JOBRELEASE} - ${JOBPLATFORM}"

    # Define the top-level workdir
    WORKDIR="/data/atlaspmb/athenamt-perfmonmt-jobs";

    # Create the rundir
    RUNDIR="${WORKDIR}/${JOBNAME}/${JOBRELEASE}/${JOBPLATFORM}";
    if [[ ! -d ${RUNDIR} ]]; then
        mkdir -p ${RUNDIR};
    fi

    # Go to the main rundir
    echo "Using ${RUNDIR} as the rundir...";
    cd "${RUNDIR}";

    # Setup the latest Athena - job runs once per day at a fixed time
    lsetup "asetup Athena,${JOBRELEASE},${JOBPLATFORM//-/,},latest";

    # Check the currently nightly tag
    nightly=`echo "${Athena_DIR##*/${JOBRELEASE}_Athena_${JOBPLATFORM}/}"`;
    nightly=`echo "${nightly%%/Athena/*}"`;

    # Check if it exists already
    if [[ -d "${nightly}" ]]; then
        echo "Directory for ${nightly} already exists, nothing to do."
        return 0;
    fi

    # Now setup the run directory
    mkdir -p "${nightly}"; cd "${nightly}";

    # Let's start
    touch __start;

    # Now run the job :
    # 1) Reco (data18) + DQ w/ N threads over M events
    # 2) Reco (data22) + DQ w/ N threads over M events
    # 3) ITk simulation w/ 16 threads over 200 events
    # 4) q221 based trigger w/ 8 threads over 500 events
    # 5) q445 based full-chain w/ 8 threads over 800 events
    if [[ "${JOBNAME}" == "rawtoall_data18_mt16" ]]; then
        run_r2a_data18 16 2000; # 2000 events in 16 threads
    elif [[ "${JOBNAME}" == "rawtoall_data18_mt8" ]]; then
        run_r2a_data18 8 1000; # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "rawtoall_data18_mt1" ]]; then
        run_r2a_data18 1 200;  # 200 events in 1 thread
    elif [[ "${JOBNAME}" == "rawtoall_data22_mt16" ]]; then
        run_r2a_data22 16 2000; # 2000 events in 16 threads
    elif [[ "${JOBNAME}" == "rawtoall_data22_mt8" ]]; then
        run_r2a_data22 8 1000; # 1000 events in 8 threads
    elif [[ "${JOBNAME}" == "rawtoall_data22_mt1" ]]; then
        run_r2a_data22 1 200;  # 200 events in 1 thread
    elif [[ "${JOBNAME}" == "evnttohits_itk_ca_mt16" ]]; then
        run_e2h_itk 16 250;  # 250 events in 16 threads
    elif [[ "${JOBNAME}" == "rdotordotrigger_q221_mt8" ]]; then
        run_r2rt 8 500;  # 500 events in 8 threads
    elif [[ "${JOBNAME}" == "fullchain_q445_mt8" ]]; then
        run_q445 8 800;  # 800 events in 8 threads
    elif [[ "${JOBNAME}" == "fullchain_mc21a_mt8" ]]; then
        run_mc21a 8 1000;  # 1000 events in 8 threads
    else
        echo "Unknown job ${JOBNAME}, quitting..."
        return 0
    fi

    # Now check the POOL content
    if [[ -f "myHITS.pool.root" ]]; then
        checkxAOD.py myHITS.pool.root > myHITS.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "myRDO.pool.root" ]]; then
        checkxAOD.py myRDO.pool.root > myRDO.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "myRDOTrigger.pool.root" ]]; then
        checkxAOD.py myRDOTrigger.pool.root > myRDOTrigger.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "myESD.pool.root" ]]; then
        checkxAOD.py myESD.pool.root > myESD.pool.root.checkfile.txt 2>/dev/null;
    fi
    if [[ -f "myAOD.pool.root" ]]; then
        checkxAOD.py myAOD.pool.root > myAOD.pool.root.checkfile.txt 2>/dev/null;
    fi

    # Cleanup the POOL files to save disk-space
    rm -f *.pool.root;
    if [[ "${JOBNAME}" == "fullchain_mc21a_mt8" ]]; then
        rm -f tmp.RDO;
        rm -f tmp.RDO_TRIG;
    fi

    # Let's extract the transform command to be used on the webpage
    echo "#!/bin/bash" > __command.txt;
    echo "export $( grep "ATHENA_CORE_NUMBER" env.txt )" >> __command.txt;
python3 << END
import json
with open("__command.txt","a") as outfile, open("jobReport.json") as infile:
    data = json.load(infile) # Load the job report
    cmd = data['cmdLine'].split("' '") # Extract the transform command
    tf  = cmd[0].split('/')[-1] # This is the main transform, strip away full path
    cmd = [ f"{val} " if "--" in val else f"'{val}' " for val in cmd[1:] ]
    cmd = [ val.replace("/data/atlaspmb/","/eos/atlas/atlascerngroupdisk/proj-spot/") for val in cmd ]
    cmd = [ val.replace("--","\\\\\n  --") for val in cmd ]
    cmd = [ val.replace("''","'") for val in cmd ]
    outfile.write(f"{tf} {''.join(cmd)}")
END

    # Let's archive the results on EOS
    nightlydate=$( echo "${nightly}" | cut -c1-10 );
    tokens=( $( echo "${nightlydate}" | tr "-" " " ) );
    YEAR="${tokens[0]}";
    MONTH="${tokens[1]}";
    DAY="${tokens[2]}";
    TARGET_DIR="${ARCHIVE_DIR}/${DAY}/${MONTH}/${YEAR}/${JOBRELEASE}/${JOBPLATFORM}/spot-mon-${JOBNAME}";
    echo "Copying results into ${TARGET_DIR}";
    mkdir -p ${TARGET_DIR};
    rsync -avuz __* prmon* log* perfmonmt* *checkfile* runargs* runwrapper* env.txt ${TARGET_DIR}/.;

    # Copy the command to the webpage area
    TARGET_DIR="${WEB_DIR}/spot-mon-${JOBNAME}/pages/commands";
    echo "Copying commands into ${TARGET_DIR}";
    mkdir -p ${TARGET_DIR};
    rsync -avuz __command.txt ${TARGET_DIR}/${DAY}-${MONTH}-${YEAR}-${JOBRELEASE}-${JOBPLATFORM}-${JOBNAME}

    # Copy the logs to the webpage area
    TARGET_DIR="${WEB_DIR}/spot-mon-${JOBNAME}/pages/logs";
    echo "Copying logs into ${TARGET_DIR}";
    mkdir -p ${TARGET_DIR};
    for file in $( ls log.* )
    do
        tokens=( $( echo "${file}" | tr "." " "  ) );
        JOBSTEP="${tokens[1]}"
        if [[ "${JOBSTEP}" == "DQHistogramMerge" ]]; then
            continue
        fi
        rsync -avuz ${file} ${TARGET_DIR}/${DAY}-${MONTH}-${YEAR}-${JOBRELEASE}-${JOBPLATFORM}-${JOBNAME}-${JOBSTEP}
    done

    # All done
    touch __done;

    # Go back to rundir
    cd "${RUNDIR}";

}

# Define the main function
main() {

    # For the time being make sure only a single instance of this script is running
    # This prevents jobs running on top of each other by serializing the executions
    # In the future we can changes this logic...
    pidof -o %PPID -x $0 >/dev/null && echo "WARNING: Script ${0} already running, nothing to do..." && exit 0

    # Define the jobs this setup will attempt to run
    JOBS=( "22.0 x86_64-centos7-gcc11-opt rawtoall_data22_mt8" \
           "22.0 x86_64-centos7-gcc11-opt rawtoall_data18_mt8" \
           "22.0 x86_64-centos7-gcc11-opt fullchain_mc21a_mt8" \
           "22.0 x86_64-centos7-gcc11-opt fullchain_q445_mt8" \
           "22.0 x86_64-centos7-gcc11-opt rawtoall_data22_mt1" \
           "22.0 x86_64-centos7-gcc11-opt rawtoall_data18_mt1" \
           "master x86_64-centos7-gcc11-opt rawtoall_data22_mt8" \
           "master x86_64-centos7-gcc11-opt rawtoall_data18_mt8" \
           "master x86_64-centos7-gcc11-opt fullchain_mc21a_mt8" \
           "master x86_64-centos7-gcc11-opt fullchain_q445_mt8" \
           "master x86_64-centos7-gcc11-opt rawtoall_data22_mt1" \
           "master x86_64-centos7-gcc11-opt rawtoall_data18_mt1" \
           "master x86_64-centos7-clang14-opt rawtoall_data22_mt8" \
           "master x86_64-centos7-clang14-opt rawtoall_data18_mt8" \
           "master x86_64-centos7-clang14-opt fullchain_mc21a_mt8" \
           "master x86_64-centos7-clang14-opt fullchain_q445_mt8" \
           "master x86_64-centos7-clang14-opt rawtoall_data22_mt1" \
           "master x86_64-centos7-clang14-opt rawtoall_data18_mt1" \
           "master--archflagtest x86_64-centos7-gcc11-opt rawtoall_data22_mt8" \
           "master--archflagtest x86_64-centos7-gcc11-opt fullchain_mc21a_mt8" \
           "master--ltoflagtest x86_64-centos7-gcc11-opt rawtoall_data22_mt8" \
           "master--ltoflagtest x86_64-centos7-gcc11-opt fullchain_mc21a_mt8" )

    # Loop over all defined jobs
    for job in "${JOBS[@]}"
    do
        # Parse the inputs: release platform name
        vals=(${job})
        jobrelease="${vals[0]}"
        jobplatform="${vals[1]}"
        jobname="${vals[2]}"
        # Match the release w/ the user input
        if [[ "${1}" == "${jobrelease}" ]]; then
            execute "${jobname}" "${jobrelease}" "${jobplatform}"
        fi
    done

}

# Execute the main function
main "${1}"
