#!/bin/bash

# Collect data
function collect_data {
    # CPU/IO Load and CPU scaling
    echo `date +%s; cat /proc/loadavg; cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor;` >> $1/sys-logs/average-load-history.log;

    # Memory
    mytime=`date +%s`
    memresults=`cat /proc/meminfo | grep "MemTotal\|MemFree\|Buffers\|Cached\|SReclaimable\|Shmem\|SwapTotal\|SwapFree"`
    values=`(IFS=' '; for memresult in $memresults; do echo "$memresult" | grep "[0-9]"; done)`
    echo ${mytime} ${values} >> $1/sys-logs/memory-history.log
}

# The BRICK
if [[ `hostname` == "uclhc-1.ps.uci.edu" ]]; then
    # Set workdir
    workdir="/home/amete/system_monitoring/"

    # Setup enviroment
    cd ${workdir};
    source ${workdir}/.profile
    echo "${KINIT_PASSWD}" | kinit -f "${KINIT_USER}"@CERN.CH
    source ~/.bashrc
    source ~/.bash_profile
    lsetup "asetup Athena,21.0,latest,slc6";  #find better solution

    # Collect data
    collect_data ${workdir}

    # Disk
    echo `date +%s; df | awk '{if ($6 == "/data") print substr($5,0,length($5)-1);}'` >> ${workdir}/sys-logs/disk-history.log

    # Make the plot and publish
    if python make_system_usage_plots.py `hostname`; then
        echo "Making plot succeeded"
        rsync --remove-source-files -avz -e ssh uclhc-average-load.png amete@lxplus.cern.ch:/eos/user/a/amete/www/atlas-uci-brick-status/plots/.
        rsync --remove-source-files -avz -e ssh uclhc-mem-info.png     amete@lxplus.cern.ch:/eos/user/a/amete/www/atlas-uci-brick-status/plots/.
        rsync --remove-source-files -avz -e ssh uclhc-disk-info.png    amete@lxplus.cern.ch:/eos/user/a/amete/www/atlas-uci-brick-status/plots/.
    else
        echo "Making plot failed"
    fi
# ATPC002
elif [[ `hostname` == "atpc002" ]]; then
    # Set workdir
    workdir="/atlas/scratch0/amete/system_monitoring/"

    # Setup enviroment
    cd ${workdir};
    source ${workdir}/.profile
    echo "${KINIT_PASSWD}" | kinit -f "${KINIT_USER}"@CERN.CH
    source ~/.bashrc
    source ~/.bash_profile
    lsetup "asetup Athena,master,latest";  #find better solution

    # Collect data
    collect_data ${workdir}

    # Disk
    echo `date +%s; df | awk '{if ($6 == "/atlas/scratch0") print substr($5,0,length($5)-1);}'` >> ${workdir}/sys-logs/disk-history.log

    # Make the plot and publish
    if python make_system_usage_plots.py `hostname`; then
        echo "Making plot succeeded"
        rsync --remove-source-files -avz atpc002-average-load.png /eos/user/a/amete/www/atlas-spot-desktop-status/plots/.
        rsync --remove-source-files -avz atpc002-mem-info.png     /eos/user/a/amete/www/atlas-spot-desktop-status/plots/.
        rsync --remove-source-files -avz atpc002-disk-info.png    /eos/user/a/amete/www/atlas-spot-desktop-status/plots/.
    else
        echo "Making plot failed"
    fi
# AIBUILD001
elif [[ `hostname` == "aibuild001.cern.ch" ]]; then
    # Set workdir
    workdir="/afs/cern.ch/user/a/atlaspmb/system_monitoring"

    # Setup enviroment
    cd ${workdir};
    source ~/.bashrc
    source ~/.bash_profile
    lsetup "asetup Athena,master,latest";  #find better solution

    # Collect data
    collect_data ${workdir}

    # Disk
    #echo `date +%s; df | awk '{if ($6 == "/build1") print substr($5,0,length($5)-1);}'` >> ${workdir}/sys-logs/disk-history.log
    echo `date +%s; df | grep "/build" | sort -k 6 | awk '{ printf "%s ", substr($5,0,length($5)-1) };';` >> ${workdir}/sys-logs/disk-history.log

    # Make the plot and publish
    if python make_system_usage_plots.py `hostname`; then
        echo "Making plot succeeded"
        rsync --remove-source-files -auvz aibuild001-average-load.png /afs/cern.ch/user/a/atlaspmb/www_atlaspmb/sys-mon/.
        rsync --remove-source-files -auvz aibuild001-mem-info.png     /afs/cern.ch/user/a/atlaspmb/www_atlaspmb/sys-mon/.
        rsync --remove-source-files -auvz aibuild001-disk-info.png    /afs/cern.ch/user/a/atlaspmb/www_atlaspmb/sys-mon/.
    else
        echo "Making plot failed"
    fi
# AIATLASBM001
elif [[ `hostname` == "aiatlasbm001.cern.ch" ]]; then
    # Set workdir
    workdir="/afs/cern.ch/user/a/atlaspmb/system_monitoring"

    # Setup enviroment
    cd ${workdir};
    source ~/.bashrc
    source ~/.bash_profile
    lsetup "asetup Athena,master,latest";  #find better solution

    # Collect data
    collect_data ${workdir}

    # Disk
    echo `date +%s; df | grep "/data" | sort -k 6 | awk '{ printf "%s ", substr($5,0,length($5)-1) };';` >> ${workdir}/sys-logs/disk-history.log

    # Make the plot and publish
    if python make_system_usage_plots.py `hostname`; then
        echo "Making plot succeeded"
        rsync --remove-source-files -auvz aiatlasbm001-average-load.png /afs/cern.ch/user/a/atlaspmb/www_atlaspmb/sys-mon/.
        rsync --remove-source-files -auvz aiatlasbm001-mem-info.png     /afs/cern.ch/user/a/atlaspmb/www_atlaspmb/sys-mon/.
        rsync --remove-source-files -auvz aiatlasbm001-disk-info.png    /afs/cern.ch/user/a/atlaspmb/www_atlaspmb/sys-mon/.
    else
        echo "Making plot failed"
    fi
fi
