#!/bin/bash

# Setup
source ~/.bashrc
source ~/.bash_profile

#lsetup "lcgenv -p LCG_98python3_ATLAS_3 x86_64-centos7-gcc8-opt sqlalchemy"  # This seems to pull in matplotlib, pandas, and numpy as well
#lsetup "asetup Athena,master,latest"
lsetup "asetup Athena,22.0.33"
eosfusebind -g # ATLINFR-3256

export PYTHONPATH=/afs/cern.ch/atlas/project/pmb/new_pmb/PerformanceMonitoring/python:$PYTHONPATH

# Go to rundir
cd /data/atlaspmb/www_staging_area/rundir;

# First prepare the database
/afs/cern.ch/atlas/project/pmb/new_pmb/PerformanceMonitoring/scripts/merge_database.py > __database.log 2>&1;

# Then create the alerts
/afs/cern.ch/atlas/project/pmb/new_pmb/PerformanceMonitoring/scripts/send_alerts.py > __alerts.txt 2>&1;

# Then build the webpage
/afs/cern.ch/atlas/project/pmb/new_pmb/PerformanceMonitoring/scripts/create_webpage.py > __webpage.txt 2>&1;

echo "Finished running master.sh, please check the outputs and the webpage." | mail -s "[SPOT] master.sh cron job finished" atlaspmb@cern.ch
