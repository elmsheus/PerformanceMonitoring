#!/bin/bash

# The BRICK
if [[ `hostname` == "uclhc-1.ps.uci.edu" ]]; then
    # Set workdir
    workdir="/home/amete/system_monitoring/"

    # Setup enviroment
    cd ${workdir};
    source ${workdir}/.profile
    echo "${KINIT_PASSWD}" | kinit -f "${KINIT_USER}"@CERN.CH
    source ~/.bashrc
    source ~/.bash_profile
    lsetup "asetup Athena,21.0,latest,slc6";  #find a better solution

    # User disk usage
    echo "#" `date -u +"%a %b %d, %Y at %H:%M %Z"` >  ${workdir}/du-logs/uci-latest.log
    du -s /data/uclhc/uci/user/* | sort -n -r      >> ${workdir}/du-logs/uci-latest.log
    echo "#" `date -u +"%a %b %d, %Y at %H:%M %Z"` >  ${workdir}/du-logs/home-latest.log
    du -s /data/home/* | sort -n -r                >> ${workdir}/du-logs/home-latest.log

    # Make the plot and publish
    if python make_diskusage_plots.py `hostname`; then
        echo "Making plot succeeded"
        rsync --remove-source-files -avz -e ssh uclhc-disk-uci-info.png   amete@lxplus.cern.ch:/eos/user/a/amete/www/atlas-uci-brick-status/plots/.
        rsync --remove-source-files -avz -e ssh uclhc-disk-home-info.png  amete@lxplus.cern.ch:/eos/user/a/amete/www/atlas-uci-brick-status/plots/.
    else
        echo "Making plot failed"
    fi
# ATPC002 
elif [[ `hostname` == "atpc002" ]]; then
    # Set workdir
    workdir="/atlas/scratch0/amete/system_monitoring/"

    # Setup enviroment
    cd ${workdir};
    source ${workdir}/.profile
    echo "${KINIT_PASSWD}" | kinit -f "${KINIT_USER}"@CERN.CH
    source ~/.bashrc
    source ~/.bash_profile
    lsetup "asetup Athena,master,latest";  #find a better solution

    # User disk usage
    echo "#" `date -u +"%a %b %d, %Y at %H:%M %Z"` >  ${workdir}/du-logs/scratch-latest.log
    du -s /atlas/scratch0/* | sort -n -r           >> ${workdir}/du-logs/scratch-latest.log

    # Make the plot and publish
    if python make_diskusage_plots.py `hostname`; then
        echo "Making plot succeeded"
        rsync --remove-source-files -avz atpc002-disk-scratch-info.png   /eos/user/a/amete/www/atlas-spot-desktop-status/plots/.
    else
        echo "Making plot failed"
    fi
fi
