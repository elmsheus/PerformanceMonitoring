#!/usr/bin/env python3

# Script for running a few select jobs on a few select branches on
# specific build machine via cron. This mainly to be able to monitor
# developments in cpu usage.
#
# Several efforts are made to ensure that results are as reliable as
# possible: Job is only started if machine is not too busy, input files
# are copied locally before start, and each job is preceded by a small
# nevts=1 test job, to ensure all afs caches are set up.

import os
import glob
import time
import datetime
import sys
import subprocess
import shutil

##############################################################################
############################# DEFINE DIRS ####################################
##############################################################################
cvmfs = True

username = os.getenv('USER')

if not os.getenv('BASEDIR'):
    print('ERROR: $BASEDIR not defines')
    sys.exit(1)

archive_dir = '/eos/atlas/user/a/atlaspmb/archive/custom'
if not os.path.exists(archive_dir):
    print('ERROR: ' + archive_dir + ' is not available')
    sys.exit(1)

nightlies_dir_cvmfs = '/cvmfs/atlas-nightlies.cern.ch/repo/sw'

tmpdir_base = ''
free_space_checks_gb = ''

print('HOSTNAME: ', os.getenv('HOSTNAME'))

if os.getenv('HOSTNAME') == 'aiatlasbm001.cern.ch':
    tmpdir_base = '/data/%s/custom_nightly_tests/rundirs' % username
    # todo: also check quota of target archive dir on afs
    free_space_checks_gb = [('/data', 30.0)]
else:
    print("job must run on aiatlasbm001.cern.ch")
    sys.exit(0)

##############################################################################
############################# DEFINE JOBS ####################################
##############################################################################

#
# A job function should return None if it doesn't want to run.
#
# Otherwise it should return (infiles,cmd) where infiles is a list of
#  tuple pairs. The former being the input file location on afs or
#  castor and the latter being the local expected location of the input file (should start with tmp_infiles_base).
#  (todo: CASTOR not supported yet)
#
# It is assumed that all output files starts with "my*"
#


def __nevts(nevts, builddate):
    if nevts == 'all':
        return -1
    return nevts

##########################################################################


def Reco_tf_cmd():
    return 'Reco_tf.py'


def __Reco_tf_hitstoaod(infile, nevts, short, amp):
    if short:
        nevts = 1
    f = infile
    preExec = ' --conditionsTag \'default:OFLCOND-RUN12-SDR-31\' --conditionsTag \'default:OFLCOND-RUN12-SDR-31\' --digiSeedOffset1 170 --digiSeedOffset2 170 --geometryVersion \'default:ATLAS-R2-2015-03-01-00\' --inputHighPtMinbiasHitsFile /build1/atlaspmb/mc/mc15_13TeV.361035.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_high.merge.HITS.e3581_s2578_s2195/HITS.05608152.*.pool.root.? --inputLowPtMinbiasHitsFile /build2/atlaspmb/mc/mc15_13TeV.361034.Pythia8EvtGen_A2MSTW2008LO_minbias_inelastic_low.merge.HITS.e3581_s2578_s2195/HITS.05608147.*.pool.root.?  --jobNumber 1  --numberOfCavernBkg="0" --numberOfHighPtMinBias \'0.122680569785\' --numberOfLowPtMinBias \'39.8773194302\'  --outputAODFile="myAOD.pool.root" --outputESDFile="myESD.pool.root" --outputRDOFile="myRDO.pool.root"  --pileupFinalBunch \'6\' --postExec \'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];\' \'HITtoRDO:job.StandardPileUpToolsAlg.PileUpTools["MergeMcEventCollTool"].OnlySaveSignalTruth=True;job.StandardPileUpToolsAlg.PileUpTools["MdtDigitizationTool"].LastXing=150\' \'RDOtoRDOTrigger:from AthenaCommon.AlgSequence import AlgSequence;AlgSequence().LVL1TGCTrigger.TILEMU=True;from AthenaCommon.AppMgr import ServiceMgr;import MuonRPC_Cabling.MuonRPC_CablingConfig;ServiceMgr.MuonRPC_CablingSvc.RPCTriggerRoadsfromCool=False\' \'ESDtoAOD:CILMergeAOD.removeItem("xAOD::CaloClusterAuxContainer#CaloCalTopoClustersAux.LATERAL.LONGITUDINAL.SECOND_R.SECOND_LAMBDA.CENTER_MAG.CENTER_LAMBDA.FIRST_ENG_DENS.ENG_FRAC_MAX.ISOLATION.ENG_BAD_CELLS.N_BAD_CELLS.BADLARQ_FRAC.ENG_BAD_HV_CELLS.N_BAD_HV_CELLS.ENG_POS.SIGNIFICANCE.CELL_SIGNIFICANCE.CELL_SIG_SAMPLING.AVG_LAR_Q.AVG_TILE_Q.EM_PROBABILITY.PTD.BadChannelList");CILMergeAOD.add("xAOD::CaloClusterAuxContainer#CaloCalTopoClustersAux.N_BAD_CELLS.ENG_BAD_CELLS.BADLARQ_FRAC.AVG_TILE_Q.AVG_LAR_Q.CENTER_MAG.ENG_POS.CENTER_LAMBDA.SECOND_LAMBDA.SECOND_R.ISOLATION.EM_PROBABILITY");StreamAOD.ItemList=CILMergeAOD()\' --postInclude \'default:RecJobTransforms/UseFrontier.py\' --preExec \'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(40.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(40);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True);\' \'HITtoRDO:userRunLumiOverride={"run":222525, "startmu":40.0, "endmu":41.0, "stepmu":1.0, "startlb":1, "timestamp": 1376703331}\' \'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.cutLevel.set_Value_and_Lock(14);\' \'ESDtoAOD:ToolSvc += CfgMgr.xAODMaker__TrackParticleCompressorTool( "xAODTrackParticleCompressorTool", OffDiagCovMatrixBits = 7 ); from JetRec import JetRecUtils;f=lambda s:["xAOD::JetContainer#AntiKt4%sJets"%(s,),"xAOD::JetAuxContainer#AntiKt4%sJetsAux."%(s,),"xAOD::EventShape#Kt4%sEventShape"%(s,),"xAOD::EventShapeAuxInfo#Kt4%sEventShapeAux."%(s,),"xAOD::EventShape#Kt4%sOriginEventShape"%(s,),"xAOD::EventShapeAuxInfo#Kt4%sOriginEventShapeAux."%(s,)]; JetRecUtils.retrieveAODList = lambda : f("EMPFlow")+f("LCTopo")+f("EMTopo")+["xAOD::EventShape#NeutralParticleFlowIsoCentralEventShape","xAOD::EventShapeAuxInfo#NeutralParticleFlowIsoCentralEventShapeAux.", "xAOD::EventShape#NeutralParticleFlowIsoForwardEventShape","xAOD::EventShapeAuxInfo#NeutralParticleFlowIsoForwardEventShapeAux.","xAOD::EventShape#ParticleFlowIsoCentralEventShape","xAOD::EventShapeAuxInfo#ParticleFlowIsoCentralEventShapeAux.", "xAOD::EventShape#ParticleFlowIsoForwardEventShape","xAOD::EventShapeAuxInfo#ParticleFlowIsoForwardEventShapeAux.", "xAOD::EventShape#TopoClusterIsoCentralEventShape","xAOD::EventShapeAuxInfo#TopoClusterIsoCentralEventShapeAux.", "xAOD::EventShape#TopoClusterIsoForwardEventShape","xAOD::EventShapeAuxInfo#TopoClusterIsoForwardEventShapeAux.","xAOD::CaloClusterContainer#EMOriginTopoClusters","xAOD::ShallowAuxContainer#EMOriginTopoClustersAux.","xAOD::CaloClusterContainer#LCOriginTopoClusters","xAOD::ShallowAuxContainer#LCOriginTopoClustersAux."]; from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.btaggingAODList=["xAOD::BTaggingContainer#BTagging_AntiKt4EMTopo","xAOD::BTaggingAuxContainer#BTagging_AntiKt4EMTopoAux.","xAOD::BTagVertexContainer#BTagging_AntiKt4EMTopoJFVtx","xAOD::BTagVertexAuxContainer#BTagging_AntiKt4EMTopoJFVtxAux.","xAOD::VertexContainer#BTagging_AntiKt4EMTopoSecVtx","xAOD::VertexAuxContainer#BTagging_AntiKt4EMTopoSecVtxAux.-vxTrackAtVertex"]; from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.ThinGeantTruth.set_Value_and_Lock(True);  AODFlags.ThinNegativeEnergyCaloClusters.set_Value_and_Lock(True); AODFlags.ThinNegativeEnergyNeutralPFOs.set_Value_and_Lock(True); from eflowRec.eflowRecFlags import jobproperties; jobproperties.eflowRecFlags.useAODReductionClusterMomentList.set_Value_and_Lock(True); from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock("AODSLIM");\'  --numberOfCavernBkg \'0\' --preInclude \'HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_muRange.py\'  --mts ESD:0 --steering "RAWtoESD:in-RDO,in+RDO_TRIG,in-BS" --ignoreErrors \'True\' ' + amp + ' '
    return (infile, f), '%s --inputHITSFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, preExec)


def __Reco_tf_hitstoaod_mc16(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    preExec = '"all:rec.doTrigger.set_Value_and_Lock(False);rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(40.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(40);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)"'
    config = ' --postExec "all:CfgMgr.MessageSvc().setError+=[\\"HepMcParticleLink\\"]" "ESDtoAOD:fixedAttrib=[s if \\"CONTAINER_SPLITLEVEL = \'99\'\\" not in s else \\"\\" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib" --postInclude "default:PyJobTransforms/UseFrontier.py" --preExec {} --preInclude "HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInlcude.PileUpBunchTrainsMC16c_2017_Config1.py,RunDependentSimData/configLumi_run310000.py" "RAWtoESD:LumiBlockComps/LumiBlockMuWriter_jobOptions.py" --skipEvents="0" --autoConfiguration="everything" --valid="True" --conditionsTag "default:OFLCOND-MC16-SDR-RUN2-09" --geometryVersion="default:ATLAS-R2-2016-01-00-01" --runNumber="410000" --digiSeedOffset1="568" --digiSeedOffset2="568" --digiSteeringConf="StandardSignalOnlyTruth" --AMITag="r10724" --inputHighPtMinbiasHitsFile="/build4/atlaspmb/mc16e-full-chain/inputs/HITS.10701335._*.pool.root.?" --inputLowPtMinbiasHitsFile="/build4/atlaspmb/mc16e-full-chain/inputs/HITS.10701323._*.pool.root.?" --numberOfCavernBkg="0" --numberOfHighPtMinBias="0.2595392" --numberOfLowPtMinBias="99.2404608" --pileupFinalBunch="6" --outputAODFile="myAOD.pool.root" --jobNumber="568" '.format(preExec)
    return (infile, f), '%s --inputHITSFile="%s" --maxEvents="%i" %s' % (Reco_tf_cmd(), f, nevts, config)


def __Reco_tf_rawtoall_mc_itk(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    cfg = '--outputRDOFile myRDO.pool.root --outputESDFile myESD.pool.root --outputAODFile myAOD.pool.root --outputDAOD_IDTRKVALIDFile myDAOD_IDTRKVALID.pool.root --digiSteeringConf StandardInTimeOnlyTruth --geometryVersion ATLAS-P2-ITK-17-06-00 --conditionsTag OFLCOND-MC15c-SDR-14-03 --DataRunNumber 242000 --steering doRAWtoALL --postInclude all:\'InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py\' HITtoRDO:\'InDetSLHC_Example/postInclude.SLHC_Digitization_lowthresh.py\' RAWtoALL:\'InDetSLHC_Example/postInclude.DigitalClustering.py\' --preExec all:\'from AthenaCommon.GlobalFlags import globalflags; globalflags.DataSource.set_Value_and_Lock("geant4"); from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags; SLHC_Flags.doGMX.set_Value_and_Lock(True); SLHC_Flags.LayoutOption="InclinedAlternative"\' HITtoRDO:\'from Digitization.DigitizationFlags import digitizationFlags; digitizationFlags.doInDetNoise.set_Value_and_Lock(False); digitizationFlags.overrideMetadata+=["SimLayout","PhysicsList"];\' RAWtoALL:\'from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doStandardPlots.set_Value_and_Lock(True);from PixelConditionsServices.PixelConditionsServicesConf import PixelCalibSvc;ServiceMgr +=PixelCalibSvc();InDetFlags.useDCS.set_Value_and_Lock(True);ServiceMgr.PixelCalibSvc.DisableDB=True;from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags;InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(True);InDetDxAODFlags.ThinHitsOnTrack.set_Value_and_Lock(False)\' ESDtoDPD:\'rec.DPDMakerScripts.set_Value_and_Lock(["PrimaryDPDMaker/PrimaryDPDMaker.py"]);from InDetRecExample.InDetJobProperties import InDetFlags;InDetFlags.useDCS.set_Value_and_Lock(True);from PixelConditionsServices.PixelConditionsServicesConf import PixelCalibSvc;ServiceMgr +=PixelCalibSvc();ServiceMgr.PixelCalibSvc.DisableDB=True\' --preInclude all:\'InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py,InDetSLHC_Example/preInclude.SLHC_Calorimeter_mu0.py\' HITtoRDO:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py\' default:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Reco.py,InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py\' RDOMergeAthenaMP:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py\' POOLMergeAthenaMPAOD0:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Ana.py,InDetSLHC_Example/SLHC_Setup_Reco_Alpine.py\' POOLMergeAthenaMPDAODIDTRKVALID0:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Ana.py,InDetSLHC_Example/SLHC_Setup_Reco_Alpine.py\' --postExec HITtoRDO:\'pixeldigi.EnableSpecialPixels=False; CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];\' RAWtoALL:\'ToolSvc.InDetSCT_ClusteringTool.useRowInformation=True; from AthenaCommon.AppMgr import ToolSvc; ToolSvc.InDetTrackSummaryTool.OutputLevel=INFO;from InDetPhysValMonitoring.InDetPhysValMonitoringConf import InDetPhysValDecoratorAlg;decorators = InDetPhysValDecoratorAlg();topSequence += decorators\''
    return (infile, f), '%s --inputHITSFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)

def __Reco_tf_rawtoall_mc_phase2_upgrade_itk_mu200(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    cfg = '--outputESDFile myESD.pool.root --digiSteeringConf \'StandardInTimeOnlyTruth\' --conditionsTag \'OFLCOND-MC15c-SDR-14-05\' --imf \'False\' --DataRunNumber \'242020\' --geometryVersion \'ATLAS-P2-ITK-22-02-00\' --preExec all:\'from AthenaCommon.GlobalFlags import globalflags; globalflags.DataSource.set_Value_and_Lock("geant4");\' RAWtoESD:\'InDetFlags.doFastTracking.set_Value_and_Lock(True);InDetFlags.useDCS.set_Value_and_Lock(True);from JetRec.JetRecFlags import jetFlags, JetContentDetail; jetFlags.detailLevel.set_Value_and_Lock(JetContentDetail.Full)\' --preInclude all:\'InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py,InDetSLHC_Example/preInclude.SLHC_Setup.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py,InDetSLHC_Example/preInclude.SLHC_Calorimeter_mu200.py\' RAWtoESD:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Reco.py\' --postInclude all:\'InDetSLHC_Example/postInclude.SLHC_Setup_ITK.py,LArROD/LArSuperCellEnable.py\' RAWtoESD:\'InDetSLHC_Example/postInclude.AnalogueClustering.py\' --postExec all:\'ServiceMgr.PixelLorentzAngleSvc.ITkL03D = True\' RAWtoESD:\'ToolSvc.InDetSCT_ClusteringTool.useRowInformation=True;conddb.addMarkup("MDT/Ofl/CABLING/MEZZANINE_SCHEMA","<forceRunNumber>232550</forceRunNumber>");conddb.addMarkup("MDT/Ofl/CABLING/MAP_SCHEMA","<forceRunNumber>232550</forceRunNumber>")\''
    return (infile, f), '%s --inputRDOFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)

def __Reco_tf_mc_phase2_upgrade_mu60(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    cfg = '--inputLowPtMinbiasHitsFile=/build4/atlaspmb/phase2-upgrade-tests/mc15_14TeV.800380.Py8EG_A3NNPDF23LO_minbias_inelastic_low_keepJets.merge.HITS.e8205_s3595_s3594/*HITS*.pool.root* --inputHighPtMinbiasHitsFile=/build4/atlaspmb/phase2-upgrade-tests/mc15_14TeV.800381.Py8EG_A3NNPDF23LO_minbias_inelastic_high_keepJets.merge.HITS.e8205_s3595_s3594/*HITS*.pool.root* --outputESDFile=myESD.pool.root --jobNumber=1 --digiSteeringConf \'StandardSignalOnlyTruth\' --conditionsTag all:\'OFLCOND-MC15c-SDR-14-05\' --geometryVersion all:\'ATLAS-P2-ITK-22-02-00\' --DataRunNumber 242006 --numberOfHighPtMinBias 0.241724 --numberOfLowPtMinBias 69.7564 --preInclude all:\'InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py,InDetSLHC_Example/preInclude.SLHC_Setup.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py,InDetSLHC_Example/preInclude.SLHC_Calorimeter_mu60.py\' default:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Reco.py,InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py\' HITtoRDO:\'InDetSLHC_Example/preInclude.SLHC.py,Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrains2012Config1_DigitConfig.py,RunDependentSimData/configLumi_muRange.py\' POOLMergeAthenaMPAOD0:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Ana.py\' --postInclude all:\'InDetSLHC_Example/postInclude.SLHC_Setup_ITK.py,LArROD/LArSuperCellEnable.py\' HITtoRDO:\'InDetSLHC_Example/postInclude.SLHC_Digitization_lowthresh.py\' RAWtoESD:\'InDetSLHC_Example/postInclude.AnalogueClustering.py\' --preExec all:\'from AthenaCommon.GlobalFlags import globalflags; globalflags.DataSource.set_Value_and_Lock("geant4"); from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags; SLHC_Flags.doGMX.set_Value_and_Lock(True)\' HITtoRDO:\'from Digitization.DigitizationFlags import digitizationFlags; digitizationFlags.doInDetNoise.set_Value_and_Lock(False); digitizationFlags.overrideMetadata+=["SimLayout","PhysicsList"]; userRunLumiOverride={"run":242006, "startmu":50.0, "endmu":70.0, "stepmu":1.0, "startlb":1, "timestamp":1412020000};digitizationFlags.experimentalDigi+=["SimpleMerge"]\' RAWtoESD:\'from JetRec.JetRecFlags import jetFlags, JetContentDetail; jetFlags.detailLevel.set_Value_and_Lock(JetContentDetail.Full)\' ESDtoAOD:\'from JetRec.JetRecFlags import jetFlags, JetContentDetail; jetFlags.detailLevel.set_Value_and_Lock(JetContentDetail.Full)\' --postExec all:\'ServiceMgr.PixelLorentzAngleSvc.ITkL03D=True\' HITtoRDO:\'CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];\' RAWtoESD:\'ToolSvc.InDetSCT_ClusteringTool.useRowInformation=True;\' ESDtoAOD:\'StreamAOD.ItemList+=["CaloCellContainer#AllCalo"]\' --pileupFinalBunch 6'
    return (infile, f), '%s --inputHITSFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)


def __Reco_tf_mc_phase2_upgrade_mu200(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    cfg = '--inputLowPtMinbiasHitsFile=/build4/atlaspmb/phase2-upgrade-tests/mc15_14TeV.800380.Py8EG_A3NNPDF23LO_minbias_inelastic_low_keepJets.merge.HITS.e8205_s3595_s3594/*HITS*.pool.root* --inputHighPtMinbiasHitsFile=/build4/atlaspmb/phase2-upgrade-tests/mc15_14TeV.800381.Py8EG_A3NNPDF23LO_minbias_inelastic_high_keepJets.merge.HITS.e8205_s3595_s3594/*HITS*.pool.root* --outputESDFile=myESD.pool.root --jobNumber=1 --digiSteeringConf \'StandardSignalOnlyTruth\' --conditionsTag all:\'OFLCOND-MC15c-SDR-14-05\' --geometryVersion all:\'ATLAS-P2-ITK-22-02-00\' --DataRunNumber 242020 --numberOfHighPtMinBias 0.725172 --numberOfLowPtMinBias 209.2692 --preInclude all:\'InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py,InDetSLHC_Example/preInclude.SLHC_Setup.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py,InDetSLHC_Example/preInclude.SLHC_Calorimeter_mu200.py\' default:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Reco.py,InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py\' HITtoRDO:\'InDetSLHC_Example/preInclude.SLHC.py,Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrains2012Config1_DigitConfig.py,RunDependentSimData/configLumi_muRange.py\' POOLMergeAthenaMPAOD0:\'InDetSLHC_Example/preInclude.SLHC.NoTRT_NoBCM_NoDBM.Ana.py\' --postInclude all:\'InDetSLHC_Example/postInclude.SLHC_Setup_ITK.py,LArROD/LArSuperCellEnable.py\' HITtoRDO:\'InDetSLHC_Example/postInclude.SLHC_Digitization_lowthresh.py\' RAWtoESD:\'InDetSLHC_Example/postInclude.AnalogueClustering.py\' --preExec all:\'from AthenaCommon.GlobalFlags import globalflags; globalflags.DataSource.set_Value_and_Lock("geant4"); from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags; SLHC_Flags.doGMX.set_Value_and_Lock(True)\' HITtoRDO:\'from Digitization.DigitizationFlags import digitizationFlags; digitizationFlags.doInDetNoise.set_Value_and_Lock(False); digitizationFlags.overrideMetadata+=["SimLayout","PhysicsList"]; userRunLumiOverride={"run":242020, "startmu":190.0, "endmu":210.0, "stepmu":1.0, "startlb":1, "timestamp":1412020000};digitizationFlags.experimentalDigi+=["SimpleMerge"]\' RAWtoESD:\'from JetRec.JetRecFlags import jetFlags, JetContentDetail; jetFlags.detailLevel.set_Value_and_Lock(JetContentDetail.Full)\' ESDtoAOD:\'from JetRec.JetRecFlags import jetFlags, JetContentDetail; jetFlags.detailLevel.set_Value_and_Lock(JetContentDetail.Full)\' --postExec all:\'ServiceMgr.PixelLorentzAngleSvc.ITkL03D=True\' HITtoRDO:\'CfgMgr.MessageSvc().setError+=["HepMcParticleLink"];\' RAWtoESD:\'ToolSvc.InDetSCT_ClusteringTool.useRowInformation=True;\' ESDtoAOD:\'StreamAOD.ItemList+=["CaloCellContainer#AllCalo"]\' --pileupFinalBunch 6'
    return (infile, f), '%s --inputHITSFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)

def __Reco_tf_mc_muons_run3layout(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    cfg = '--preExec \'from MuonRecExample.MuonRecFlags import muonRecFlags;muonRecFlags.setDefaults();muonRecFlags.doFastDigitization=False;muonRecFlags.useLooseErrorTuning.set_Value_and_Lock(True);from RecExConfig.RecFlags import rec;rec.doTrigger=False;rec.doEgamma=True;rec.doLucid=True;rec.doZdc=True;rec.doJetMissingETTag=True\' --autoConfiguration everything --conditionsTag \'default:OFLCOND-MC16-SDR-RUN2-08\' --outputESDFile myESD.pool.root'
    return (infile, f), '%s --inputHITSFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)

def __hitstoaod_mc_ttbar(branch, cmtcfg, builddate, short, nevts):
    infile = '/build1/atlaspmb/mc/mc15_valid.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.simul.HITS.e3698_s2726/HITS.06950958._003212.pool.root.1'
    return __Reco_tf_hitstoaod(infile, __nevts(nevts, builddate), short, '')


def __hitstoaod_mc16_ttbar(branch, cmtcfg, builddate, short, nevts):
    infile = '/build4/atlaspmb/mc16e-full-chain/inputs/HITS.10504490._000425.pool.root.1'
    return __Reco_tf_hitstoaod_mc16(infile, __nevts(nevts, builddate), short)


def __rawtoall_mc_itk(branch, cmtcfg, builddate, short, nevts):
    infile = '/build2/atlaspmb/SLHC_jobs/SIM_100/myHITS.pool.root'
    return __Reco_tf_rawtoall_mc_itk(infile, __nevts(nevts, builddate), short)


def __rawtoall_mc_phase2_upgrade_itk_mu200(branch, cmtcfg, builddate, short, nevts):
    infile = '/build4/atlaspmb/phase2-upgrade-tests/create-rdos/20210111/mu200/myRDO.pool.root'
    return __Reco_tf_rawtoall_mc_phase2_upgrade_itk_mu200(infile, __nevts(nevts, builddate), short)


def __hitstoesd_mc_phase2_upgrade_mu60(branch, cmtcfg, builddate, short, nevts):
    infile = '/build4/atlaspmb/phase2-upgrade-tests/mc15_14TeV.600012.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.HITS.e8185_s3595_s3600/*HITS*.pool.root*'
    return __Reco_tf_mc_phase2_upgrade_mu60(infile, __nevts(nevts, builddate), short)


def __hitstoesd_mc_phase2_upgrade_mu200(branch, cmtcfg, builddate, short, nevts):
    infile = '/build4/atlaspmb/phase2-upgrade-tests/mc15_14TeV.600012.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.HITS.e8185_s3595_s3600/*HITS*.pool.root*'
    return __Reco_tf_mc_phase2_upgrade_mu200(infile, __nevts(nevts, builddate), short)


def __hitstoesd_mc_muons_run3layout(branch, cmtcfg, builddate, short, nevts):
    infile = '/build4/atlaspmb/muons-run3/data/user.amete.22635289.EXT0._000001.myHITS.pool.root'
    return __Reco_tf_mc_muons_run3layout(infile, __nevts(nevts, builddate), short)


# default

def fullchain_mc15_ttbar_valid_13tev_25ns_mu40(**kw):
    return __hitstoaod_mc_ttbar(nevts=100, **kw)

def fullchain_mc16_ttbar_valid_13tev_25ns_mu40(**kw):
    return __hitstoaod_mc16_ttbar(nevts=100, **kw)

def rawtoall_mc_itk(**kw):
    return __rawtoall_mc_itk(nevts=100, **kw)

def rawtoall_mc_phase2_upgrade_itk_mu200(**kw):
    return __rawtoall_mc_phase2_upgrade_itk_mu200(nevts=250, **kw)

def digireco_mc_phase2_upgrade_mu60(**kw):
    return __hitstoesd_mc_phase2_upgrade_mu60(nevts=100, **kw)

def digireco_mc_phase2_upgrade_mu200(**kw):
    return __hitstoesd_mc_phase2_upgrade_mu200(nevts=50, **kw)

def digireco_mc_muons_run3layout(**kw):
    return __hitstoesd_mc_muons_run3layout(nevts=1000, **kw)

####


def __Reco_tf_rawtoall(infile, nevts, short, branch):
    if short:
        nevts = 1
    f = infile
    ## As of 2020/11/19 we're running tests w/ different preExecs
    ## 21.X needs a preExec to use dynamic alignment that doesn't work in master
    ## We have to disable HLTMon in master to work around segfaults i.e. see ATR-22947
    preExec = '{}'.format('r2a:\'from AthenaMonitoring.DQMonFlags import DQMonFlags; DQMonFlags.doHLTMon=False;\'' if not '21.' in branch else 'r2a:\'from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True);\'')
    if preExec:
        preExec = '--preExec {}'.format(preExec)
    ## See ATLASRECTS-6590
    condTags = { '21.0':'CONDBR2-BLKPA-2016-16', 'master':'CONDBR2-BLKPA-RUN2-09' }
    cfg = ' --autoConfiguration \'everything\' --conditionsTag  \'{}\' --geometryVersion  \'ATLAS-R2-2015-04-00-00\'  --outputDESDM_SLTTMUFile  \'myDESDM_SLTTMU.pool.root\' --outputDRAW_RPVLLFile  \'myDRAW_RPVLL.pool.root\' --outputDESDM_MCPFile  \'myDESDM_MCP.pool.root\' --outputDRAW_ZMUMUFile  \'myDRAW_ZMUMU.pool.root\' --outputDESDM_EGAMMAFile  \'myDESDM_EGAMMA.pool.root\' --outputDESDM_CALJETFile  \'myDESDM_CALJET.pool.root\' --outputAODFile  \'myAOD.pool.root\' --outputDAOD_IDTIDEFile \'myDAOD_IDTIDE.pool.root\' --outputDESDM_PHOJETFile  \'myDESDM_PHOJET.pool.root\' --outputDESDM_TILEMUFile  \'myDESDM_TILEMU.pool.root\' --outputDRAW_EGZFile  \'myDRAW_EGZ.pool.root\' --outputDESDM_EXOTHIPFile  \'myDESDM_EXOTHIP.pool.root\' --outputDESDM_SGLELFile  \'myDESDM_SGLEL.pool.root\' --ignoreErrors \'True\' --steering doRAWtoALL {}'.format(condTags[branch], preExec)
    return (infile, f), '%s --inputBSFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)


def __rawtoall_data16(branch, cmtcfg, builddate, short, nevts):
    infile = '/build1/atlaspmb/data16_13TeV/data16_13TeV.00305777.physics_Main.daq.RAW._lb0290._SFO-6._0002.data'
    return __Reco_tf_rawtoall(infile, __nevts(nevts, builddate), short, branch)


# default


def rawtoall_tier0_reco_data16(**kw): return __rawtoall_data16(nevts=500, **kw)


# data 17


def __Reco_tf_rawtoall_data17(infile, nevts, short, branch):
    if short:
        nevts = 1
    f = infile
    ## As of 2020/11/19 we're running tests w/ different preExecs
    ## 21.X needs a preExec to use dynamic alignment that doesn't work in master
    preExec = '{}'.format('' if not '21.' in branch else 'r2a:\'from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True);\'')
    ## See ATLASRECTS-6590
    condTags = { '21.0':'CONDBR2-BLKPA-2017-10', 'master':'CONDBR2-BLKPA-RUN2-09' }
    if preExec:
        preExec = '--preExec {}'.format(preExec)
    cfg = ' --autoConfiguration \'everything\' --conditionsTag  \'{}\' --geometryVersion  \'ATLAS-R2-2016-01-00-01\'  --outputDESDM_SLTTMUFile  \'myDESDM_SLTTMU.pool.root\' --outputDRAW_RPVLLFile  \'myDRAW_RPVLL.pool.root\' --outputDESDM_MCPFile  \'myDESDM_MCP.pool.root\' --outputDRAW_ZMUMUFile  \'myDRAW_ZMUMU.pool.root\' --outputDESDM_EGAMMAFile  \'myDESDM_EGAMMA.pool.root\' --outputDESDM_CALJETFile  \'myDESDM_CALJET.pool.root\' --outputAODFile  \'myAOD.pool.root\' --outputDAOD_IDTIDEFile \'myDAOD_IDTIDE.pool.root\' --outputDESDM_PHOJETFile  \'myDESDM_PHOJET.pool.root\' --outputDESDM_TILEMUFile  \'myDESDM_TILEMU.pool.root\' --outputDRAW_EGZFile  \'myDRAW_EGZ.pool.root\' --outputDESDM_EXOTHIPFile  \'myDESDM_EXOTHIP.pool.root\' --outputDESDM_SGLELFile  \'myDESDM_SGLEL.pool.root\' --ignoreErrors \'True\' --steering doRAWtoALL {}'.format(condTags[branch], preExec)
    return (infile, f), '%s --inputBSFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)


def __rawtoall_data17(branch, cmtcfg, builddate, short, nevts):
    infile = '/build1/atlaspmb/data17_13TeV/data17_13TeV.00326446.physics_Main.daq.RAW._lb0154._SFO-1._0001.data'
    return __Reco_tf_rawtoall_data17(infile, __nevts(nevts, builddate), short, branch)


def rawtoall_tier0_reco_data17(**kw): return __rawtoall_data17(nevts=300, **kw)

# data18


def __Reco_tf_rawtoall_data18(infile, nevts, short, branch):
    if short:
        nevts = 1
    f = infile
    ## As of 2020/11/19 we're running tests w/ different preExecs
    ## 21.X needs a preExec to use dynamic alignment that doesn't work in master
    ## Remove DRAW_TAUMUH, see ATLASG-1595
    preExec = 'r2a:\'if "TileJetMonTool/TileJetMonTool" in ToolSvc.getSequence(): ToolSvc.TileJetMonTool.do_1dim_histos=True;\'{}'.format(
        '' if not '21.' in branch else ' r2a:\'from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True);\'')
    ## See ATLASRECTS-6590
    condTags = { '21.0':'CONDBR2-BLKPA-2018-12', 'master':'CONDBR2-BLKPA-RUN2-09' }
    cfg = ' --autoConfiguration \'everything\' --conditionsTag \'{}\' --geometryVersion \'ATLAS-R2-2016-01-00-01\' --outputAODFile \'myAOD.pool.root\' --outputDRAW_EGZFile \'myDRAW_EGZ.pool.root\' --outputDRAW_RPVLLFile \'myDRAW_RPVLL.pool.root\' --outputDRAW_ZMUMUFile \'myDRAW_ZMUMU.pool.root\' --outputDESDM_CALJETFile \'myDESDM_CALJET.pool.root\' --outputDESDM_EXOTHIPFile \'myDESDM_EXOTHIP.pool.root\' --outputDESDM_MCPFile \'myDESDM_MCP.pool.root\' --outputDESDM_PHOJETFile \'myDESDM_PHOJET.pool.root\' --outputDESDM_TILEMUFile \'myDESDM_TILEMU.pool.root\' --outputDAOD_IDTIDEFile \'myDAOD_IDTIDE.pool.root\' --AMITag \'f1002\' --ignoreErrors \'False\' --steering \'doRAWtoALL\' --preExec {} --postExec r2a:\'TriggerFlags.AODEDMSet="AODFULL"; rec.doAFP.set_Value_and_Lock(True);\''.format(condTags[branch], preExec)
    return (infile, f), '%s --inputBSFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)


def __rawtoall_data18(branch, cmtcfg, builddate, short, nevts):
    infile = '/build1/atlaspmb/data18_13TeV/data18_13TeV.00364485.physics_Main.daq.RAW._lb0729._SFO-7._0001.data'
    return __Reco_tf_rawtoall_data18(infile, __nevts(nevts, builddate), short, branch)


def rawtoall_tier0_reco_data18(**kw):
    return __rawtoall_data18(nevts=100, **kw)


# data18 w/ AthenaMT --threads=1


def __Reco_tf_rawtoall_data18_mt(infile, nevts, short, branch, nthreads, perfmonmt):
    if short:
        nevts = 1
    f = infile
    cfg = ' --autoConfiguration \'everything\' --conditionsTag \'all:CONDBR2-BLKPA-RUN2-09\' --geometryVersion \'default:ATLAS-R2-2016-01-00-01\' --multithreaded \'True\' --outputAODFile \'myAOD.pool.root\' --outputHISTFile \'myHIST.pool.root\' --preExec \'all:from AthenaMonitoring.DQMonFlags import DQMonFlags; DQMonFlags.doMonitoring=True; DQMonFlags.doNewMonitoring=True; {} \' --runNumber \'357750\' --ignoreErrors \'True\' --steering \'doRAWtoALL\''.format('' if not perfmonmt else 'from PerfMonComps.PerfMonFlags import jobproperties; jobproperties.PerfMonFlags.doMonitoring = False; jobproperties.PerfMonFlags.doFastMonMT = True; jobproperties.PerfMonFlags.doFullMonMT = True; jobproperties.PerfMonFlags.OutputJSON  = "perfmonmt_RAWtoALL.json"; include( "PerfMonComps/PerfMonMTSvc_jobOptions.py" );')
    return (infile, f), 'ATHENA_CORE_NUMBER=%i %s --inputBSFile %s --maxEvents %i %s' % (nthreads, Reco_tf_cmd(), f, nevts, cfg)


def __rawtoall_data18_mt(branch, cmtcfg, builddate, short, nevts, nthreads, perfmonmt):
    infile = '/build1/atlaspmb/data18_13TeV/data18_13TeV.00357750.physics_Main.daq.RAW._lb0105._SFO-4._0002.data'
    return __Reco_tf_rawtoall_data18_mt(infile, __nevts(nevts, builddate), short, branch, nthreads, perfmonmt)


def rawtoall_data18_mt1(**kw): return __rawtoall_data18_mt(nevts=300, nthreads=1, perfmonmt=False, **kw)


# Trigger q445 w/ threads=1


def __Reco_tf_rdotordotrigger_q445_threads(infile, nevts, nthreads):
    f = infile
    return (infile, f), '%s --inputRDOFile %s --AMI q445 --maxEvents %i --athenaopts=\'--threads=%i --pmon=sdmon\' --outputRDO_TRIGFile=myRDOTrigger.pool.root --steering=doRDO_TRIG ' % (Reco_tf_cmd(), f, nevts, nthreads)


def __rdotordotrigger_q445_threads(branch, cmtcfg, builddate, short, nevts, nthreads):
    infile = '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/TriggerTest/valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.digit.RDO.e4993_s3214_d1697_tid26494053_00/RDO.26494053._000001.pool.root.1'
    return __Reco_tf_rdotordotrigger_q445_threads(infile, nevts, nthreads)


def rdotordotrigger_q445_mt1(**kw): return __rdotordotrigger_q445_threads(nevts=100, nthreads=1, **kw)


# Sim_tf


def Sim_tf_cmd():
    return 'Sim_tf.py'


def __Sim_tf_evnttohits_ttbar(infile, nevts, short, branch, isFast=False, isValidation=False, validationType=None, isRun3Geometry=False):
    if short:
        nevts = 1
    f = infile
    if isFast:
        cfg = ' --AMIConfig a899 --outputHITSFile myHITS.pool.root '
        if '21.' not in branch:
            cfg += ' --preInclude \'EVNTtoHITS:Campaigns/MC16Simulation.py\''
    elif not isValidation:
        cfg = ' --AMIConfig s3505 --outputHITSFile myHITS.pool.root --preInclude \'SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py\' '
        if 'master' in branch:
            cfg += ' --postInclude \'SimulationJobOptions/postInclude.MC16_EMEC_Setup.py\''
    else:
        cfg = ' --AMIConfig s3505 --outputHITSFile myHITS.pool.root '
    if isValidation:
        if validationType == 'PRR':
            cfg += ' --preInclude \'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py,SimulationJobOptions/preInclude.PhotonRussianRoulette.py,SimulationJobOptions/preInclude.NeutronRussianRoulette.py\' --preExec \'EVNTtoHITS:simFlags.TightMuonStepping=True;simFlags.PRRThreshold.set_Value_and_Lock(0.5)\' --postExec \'EVNTtoHITS:from G4AtlasApps.SimFlags import simFlags; simFlags.G4Commands += ["/process/em/applyCuts true"]\''
        elif validationType == 'Run3':
            cfg += ' --preInclude \'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py,SimulationJobOptions/preInclude.G4Optimizations.py\' --postInclude \'SimulationJobOptions/postInclude.G4Optimizations.py\''
    if isRun3Geometry:
        cfg += ' --geometryVersion \'default:ATLAS-R3S-2021-02-00-00_VALIDATION\''
    return (infile, f), '%s --inputEVNTFile %s --maxEvents %i %s' % (Sim_tf_cmd(), f, nevts, cfg)


def __Sim_tf_evnttohits_itk(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    cfg = '--outputHITSFile myHITS.pool.root --skipEvents 0 --randomSeed 873254 --geometryVersion ATLAS-P2-ITK-17-06-00_VALIDATION --conditionsTag OFLCOND-MC15c-SDR-14-03 --truthStrategy MC15aPlus --DataRunNumber 242000 --preInclude all:\'InDetSLHC_Example/preInclude.SLHC.py,InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py,InDetSLHC_Example/preInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py\' --preExec all:\'from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags; SLHC_Flags.doGMX.set_Value_and_Lock(True); SLHC_Flags.LayoutOption="InclinedAlternative"\' --postInclude all:\'PyJobTransforms/UseFrontier.py,InDetSLHC_Example/postInclude.SLHC_Setup_InclBrl_4.py,InDetSLHC_Example/postInclude.SLHC_Setup.py,InDetSLHC_Example/postInclude.SiHitAnalysis.py\' --postExec EVNTtoHITS:\'ServiceMgr.DetDescrCnvSvc.DoInitNeighbours=False; from AthenaCommon import CfgGetter; CfgGetter.getService("ISF_MC15aPlusTruthService").BeamPipeTruthStrategies+=["ISF_MCTruthStrategyGroupIDHadInt_MC15"];\''
    return (infile, f), '%s --inputEVNTFile %s --maxEvents %i %s' % (Sim_tf_cmd(), f, nevts, cfg)


def __Sim_tf_evnttohits_fastcalo(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    cfg = '--conditionsTag \'default:OFLCOND-MC16-SDR-14\' --physicsList \'FTFP_BERT_ATL\' --truthStrategy \'MC15aPlus\' --simulator \'G4FastCalo\' --postInclude \'default:PyJobTransforms/UseFrontier.py\' --preInclude \'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py\' --preExec \'EVNTtoHITS:simFlags.TightMuonStepping=True\' --DataRunNumber \'284500\' --geometryVersion \'default:ATLAS-R2-2016-01-00-01\' --outputHITSFile \'myHITS.pool.root\''
    return (infile, f), '%s --inputEVNTFile %s --maxEvents %i %s' % (Sim_tf_cmd(), f, nevts, cfg)


def __evnttohits_ttbar(branch, cmtcfg, builddate, short, nevts, isFast, isValidation, validationType, isRun3Geometry):
    infile = '/build4/atlaspmb/simulation-inputs/EVNT.12458444._000035.pool.root.1'
    return __Sim_tf_evnttohits_ttbar(infile, __nevts(nevts, builddate), short, branch, isFast, isValidation, validationType, isRun3Geometry)


def __evnttohits_itk(branch, cmtcfg, builddate, short, nevts):
    infile = '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/inputs/EVNT.01485091._001049.pool.root.1'
    return __Sim_tf_evnttohits_itk(infile, __nevts(nevts, builddate), short)


def __evnttohits_ttbar_fastcalo(branch, cmtcfg, builddate, short, nevts):
    infile = '/build4/atlaspmb/simulation-inputs/EVNT.12458444._000035.pool.root.1'
    return __Sim_tf_evnttohits_fastcalo(infile, __nevts(nevts, builddate), short)


def simulation_ttbar(**kw):
    return __evnttohits_ttbar(nevts=100, isFast=False, isValidation=False, validationType=None, isRun3Geometry=False, **kw)


def fast_simulation_ttbar(**kw):
    return __evnttohits_ttbar(nevts=100, isFast=True, isValidation=False, validationType=None, isRun3Geometry=False, **kw)


def simulation_run3optimizations_ttbar(**kw):
    return __evnttohits_ttbar(nevts=100, isFast=False, isValidation=True, validationType='Run3', isRun3Geometry=False, **kw)


def simulation_run3optandgeom_ttbar(**kw):
    return __evnttohits_ttbar(nevts=10, isFast=False, isValidation=True, validationType='Run3', isRun3Geometry=True, **kw)


def simulation_itk(**kw):
    return __evnttohits_itk(nevts=25, **kw)


# Derivations


# MC


def __Reco_tf_aodtodaod_mc(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    cfg = ' --outputDAODFile my.pool.root --reductionConf PHYSVAL --preExec \'rec.doApplyAODFix.set_Value_and_Lock(True);from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = "BTagCalibRUN12-08-49"\''
    return (infile, f), '%s --inputAODFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)


def __aodtodaod_mc(branch, cmtcfg, builddate, short, nevts):
    infile = '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/DerivationFrameworkART/AOD.14795494._005958.pool.root.1'
    return __Reco_tf_aodtodaod_mc(infile, __nevts(nevts, builddate), short)


def derivation_physval_mc16(**kw): return __aodtodaod_mc(nevts=1000, **kw)


# DATA


def __Reco_tf_aodtodaod_data(infile, nevts, short):
    if short:
        nevts = 1
    f = infile
    cfg = ' --outputDAODFile my.pool.root --reductionConf PHYSVAL --preExec \'rec.doApplyAODFix.set_Value_and_Lock(True);from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = "BTagCalibRUN12Onl-08-49"\''
    return (infile, f), '%s --inputAODFile %s --maxEvents %i %s' % (Reco_tf_cmd(), f, nevts, cfg)


def __aodtodaod_data(branch, cmtcfg, builddate, short, nevts):
    infile = '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/DerivationFrameworkART/data18_13TeV.00348403.physics_Main.merge.AOD.f920_m1947._lb0829._0001.1'
    return __Reco_tf_aodtodaod_data(infile, __nevts(nevts, builddate), short)


def derivation_physval_data18(**kw): return __aodtodaod_data(nevts=500, **kw)


# Overlay jobs


def overlay_mc(**kw):
    inhits = '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/OverlayMonitoringRTT/mc16_13TeV.424000.ParticleGun_single_mu_Pt100.simul.HITS.e3580_s3126/HITS.11330296._000376.pool.root.1'
    inbkg = '/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/OverlayMonitoringRTT/PileupPremixing/22.0/v4/RDO.merged-pileup.100events.pool.root'  # master v4
    amitag = 'd1498'
    maxevt = '100'
    return (inhits, inhits), '{} --AMIConfig {} --inputHITSFile {} --inputRDO_BKGFile {}  --maxEvents {} --outputRDOFile myRDO.pool.root --imf False '.format(Reco_tf_cmd(), amitag, inhits, inbkg, maxevt)


# Digitization jobs


def digitization_validation_mc16(**kw):
    infile = '/build4/atlaspmb/mc16e-full-chain/inputs/HITS.10504490._000425.pool.root.1'
    nevts  = '50'
    config = ' --postExec "all:CfgMgr.MessageSvc().setError+=[\\"HepMcParticleLink\\"]" "ESDtoAOD:fixedAttrib=[s if \\"CONTAINER_SPLITLEVEL = \'99\'\\" not in s else \\"\\" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib" --postInclude "default:PyJobTransforms/UseFrontier.py" --preExec "all:rec.doTrigger.set_Value_and_Lock(False);rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(40.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(40);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)" "HITtoRDO:from Digitization.DigitizationFlags import digitizationFlags;digitizationFlags.doPixelPlanarRadiationDamage.set_Value_and_Lock(True); digitizationFlags.doPixel3DRadiationDamage.set_Value_and_Lock(True);" --preInclude "HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInlcude.PileUpBunchTrainsMC16c_2017_Config1.py,RunDependentSimData/configLumi_run310000.py" "RAWtoESD:LumiBlockComps/LumiBlockMuWriter_jobOptions.py" --skipEvents="0" --autoConfiguration="everything" --valid="True" --conditionsTag "default:OFLCOND-MC16-SDR-25" --geometryVersion="default:ATLAS-R2-2016-01-00-01" --runNumber="410000" --digiSeedOffset1="568" --digiSeedOffset2="568" --digiSteeringConf="StandardSignalOnlyTruth" --AMITag="r10724" --inputHighPtMinbiasHitsFile="/build4/atlaspmb/mc16e-full-chain/inputs/HITS.10701335._*.pool.root.?" --inputLowPtMinbiasHitsFile="/build4/atlaspmb/mc16e-full-chain/inputs/HITS.10701323._*.pool.root.?" --numberOfCavernBkg="0" --numberOfHighPtMinBias="0.2595392" --numberOfLowPtMinBias="99.2404608" --pileupFinalBunch="6" --outputRDOFile="myRDO.pool.root" --jobNumber="568" '
    return (infile, infile), '{} --inputHITSFile="{}" --maxEvents="{}" {}'.format(Reco_tf_cmd(), infile, nevts, config)


##########################################################################

joblist_default = [
    rawtoall_tier0_reco_data16,
    rawtoall_tier0_reco_data17,
    rawtoall_tier0_reco_data18,
    fullchain_mc16_ttbar_valid_13tev_25ns_mu40,
]

joblist_derivations = [
    derivation_physval_mc16,
    derivation_physval_data18,
]

joblist_multithread = [
    #rdotordotrigger_q445_mt1,
    rawtoall_data18_mt1,
]

joblist_itk = [
    simulation_itk,
    rawtoall_mc_itk,
    digireco_mc_phase2_upgrade_mu60,
    digireco_mc_phase2_upgrade_mu200,
    rawtoall_mc_phase2_upgrade_itk_mu200,
]

joblist_overlay = [
    overlay_mc,
]

joblist_simulation = [
    simulation_ttbar,
    fast_simulation_ttbar,
]

joblist_simulation_validation = [
    simulation_run3optimizations_ttbar,
    simulation_run3optandgeom_ttbar,
]

joblist_digitization_validation = [
    digitization_validation_mc16,
]

joblist_muons = [
    digireco_mc_muons_run3layout,
]

#############################################################################

runlist = []

if os.getenv('HOSTNAME') == 'aiatlasbm001.cern.ch':
    runlist += [('master', 'x86_64-centos7-gcc11-opt'  , joblist_default)]
    runlist += [('21.0'  , 'x86_64-centos7-gcc62-opt'  , joblist_default)]
    runlist += [('master', 'x86_64-centos7-gcc11-opt'  , joblist_multithread)]
    runlist += [('master--archflagtest', 'x86_64-centos7-gcc11-opt'  , joblist_multithread)]
    runlist += [('21.9'  , 'x86_64-centos7-gcc62-opt'  , joblist_itk)]
    runlist += [('master', 'x86_64-centos7-gcc11-opt'  , joblist_simulation)]
    runlist += [('21.0'  , 'x86_64-centos7-gcc62-opt'  , joblist_simulation)]
    runlist += [('master', 'x86_64-centos7-gcc11-opt'  , joblist_simulation)]
    runlist += [('master--HepMC3', 'x86_64-centos7-gcc11-opt', joblist_simulation)]
    runlist += [('master', 'x86_64-centos7-gcc11-opt'  , joblist_simulation_validation)]
    runlist += [('master', 'x86_64-centos7-gcc11-opt'  , joblist_digitization_validation)]
    #runlist += [('master', 'x86_64-centos7-gcc11-opt'  , joblist_muons)]
    runlist += [('21.2'  , 'x86_64-centos7-gcc62-opt'  , joblist_derivations)]

#############################################################################


def get_load():
    #duplicated in Misc.py
    p = subprocess.Popen(
        '/bin/ps -eo pcpu --no-headers'.split(),
        stdout=subprocess.PIPE).communicate()[0]
    p = p.decode('utf-8')
    return 0.01 * sum(map(lambda x: float(x), p.split()))


def get_n_cores():
    #duplicated in Misc.py
    n = 0
    for l in open('/proc/cpuinfo'):
        if l.startswith('processor') and l.split()[0:2] == ['processor', ':']:
            n += 1
    return n


def ok_to_run():
    l = get_load()
    n = get_n_cores()
    if n == 1:
        return l < 0.05
    return l < n * 0.33


def memory_status(username):
    used_memory = subprocess.Popen(
        "ps -u %s -o rss | awk '{sum+=$1} END {print sum}'" %
        username, shell=True, stdout=subprocess.PIPE).communicate()[0]
    used_memory = used_memory.decode('utf-8')
    return int(used_memory)


if not ok_to_run():
    sys.exit(0)  # not an error, will relaunch later


if memory_status('atlaspmb') > 30000000:
    print('not enough memory available. exit!')
    sys.exit(0)


##############################################################################
######################### ABORT IF LIMITED SPACE #############################
##############################################################################


def get_free_space(mountpoint):
    if not os.path.exists(mountpoint):
        return None
    for l in subprocess.Popen(
            ['df', '-k', mountpoint], stdout=subprocess.PIPE).communicate()[0].decode('utf-8').split('\n'):
        l = l.split()
        if l and l[-1] == mountpoint:
            return float(l[-3]) / (1024.0 * 1024.0)


space_ok = True
for mountpoint, limit in free_space_checks_gb:
    fs = get_free_space(mountpoint)
    if fs is None:
        print("Could not determine space left on %s" % mountpoint)
        space_ok = False
    elif fs < limit:
        print("Too little space left on %s : %.2f gb (required %.2f gb)" % (
            mountpoint, fs, limit))
        space_ok = False
if not space_ok:
    print("Aborting.")
    sys.exit(1)


##############################################################################
#################### FIGURE OUT WHICH JOBS TO RUN ############################
##############################################################################


def touch(filename):
    # only creates if doesn't exist. Won't update timestamp of existing file.
    if not os.path.exists(filename):
        open(filename, 'a').close()


def decode_build_date(relNstr, modtime):
    assert relNstr.startswith('rel_') and len(
        relNstr) == 5 and relNstr[-1] in '0123456'
    weekday = int(relNstr[4:])
    weekday = (weekday - 1) % 7  # to get monday=0, sunday=6
    assert weekday >= 0 and weekday <= 6
    builddate = datetime.date.fromtimestamp(modtime)
    # Adjust builddate so it matches the rel_X number exactly (rel_1=monday,
    # etc.):
    if builddate.weekday() != weekday:
        #print "WARNING: Correcting date!!!"
        #print weekday,'  ',builddate.weekday()
        deltadays = weekday - builddate.weekday()
        if deltadays >= 5:
            deltadays -= 7
        if deltadays <= -5:
            deltadays += 7
        #print deltadays
        #assert deltadays>=-2 and deltadays<=2
        builddate = builddate + datetime.timedelta(deltadays)
    return builddate


for b, cmtcfg, joblist in runlist:

    # Define the ATLAS project according to the release
    atlas_project = 'Athena'
    if b == '21.2':
        atlas_project = 'AthDerivation'

    pattern = '%s/%s_%s_%s/*/%s/*/InstallArea/%s/bin' % (
        nightlies_dir_cvmfs, b, atlas_project, cmtcfg, atlas_project, cmtcfg)

    print(b, cmtcfg, joblist)
    print(pattern)
    dirs = sorted(glob.glob(pattern))

    for d in dirs:
        print(d)
        if not os.path.isdir(d):
            continue  # protect against afs issues
        dtime = os.path.getmtime(d)

        hours_ago = (time.time() - dtime) / (3600.0)
        # To stay away from builds in progress the timestamp should have an age
        # of at least 1 hour
        if hours_ago < 1:
            print('hours_ago<1')
            continue
        # To stay away from builds about to be restarted, the timestamp should
        # have an age of at most 3 days.
        if hours_ago > 24 * 3:
            print('hours_ago>24*3')
            continue
        # Get build date:
        relN = d.split('/')[-6]

        relsplit = relN.split('-')
        if (len(relsplit) != 3):
            continue
        year = relsplit[0]
        month = relsplit[1]
        day = relsplit[2].split('T')[0]
        builddate = datetime.date(int(year), int(month), int(day))

        build_archive_dir = os.path.join(
            archive_dir, builddate.strftime('%d/%m/%Y'), b, cmtcfg)
        # Figure out atlas setup command:
        asetup_args = cmtcfg.split('-') + [b, relN]
        if asetup_args[0] == 'x86_64':
            asetup_args[0] = '64'
        else:
            asetup_args[0] = '32'
        asetup_args = asetup_args[1:] # Drop the 32, 64 from the asetup command...
        for ival, val in enumerate(asetup_args):
            if 'T' in val and '-' in val and 'r' not in val:
                # only rYYYY-MM-DDTHHMM works as of 30/01/2019
                asetup_args[ival] = 'r' + asetup_args[ival]
        asetup_cmd = 'lsetup "asetup ' + ','.join(asetup_args) + ',' + atlas_project
        asetup_cmd += '"'

        # Figure out which jobs to run:
        for job in joblist:

            print(job.__name__)

            # Simulation Run3 optimizations also runs on a weekly basis 
            if 'simulation_run3optimizations_ttbar' in job.__name__:
                if int(day)%7 != 1:
                    continue

            # Get command + short command:
            infiles, cmd = job(branch=b, cmtcfg=cmtcfg, builddate=builddate, short=False)  # old
            # infiles,cmd=job
            if not cmd:
                continue  # not on this build apparently

            infiles_short, cmd_short = job(branch=b, cmtcfg=cmtcfg, builddate=builddate, short=True)
            assert infiles == infiles_short
            assert cmd_short

            # Check if done already done, and if not acquire "lock" by making
            # target output dir:
            target_dir = os.path.join(build_archive_dir, job.__name__)
            file_done = os.path.join(target_dir, '__done')
            file_start = os.path.join(target_dir, '__start')

            if os.path.isdir(target_dir):
                if os.path.exists(file_done):
                    continue
                # already running or done (todo: check here that the job
                # actually finished in 24 hours... otherwise delete and
                # relaunch)
                if not os.path.exists(file_start):
                    time.sleep(60)
                if not os.path.exists(file_start) or (
                        time.time() - os.path.getmtime(file_start)) > 24 * 3600:
                    print("Warning: Having to remove %s" % target_dir)
                    shutil.rmtree(target_dir)
                else:
                    print("elseing target dir")
                    continue

            try:
                os.makedirs(target_dir)  # Now we are in charge...
                assert os.path.isdir(target_dir)
                touch(file_start)
            except Exception as err:
                print("ERROR: Problems encountered while making %s" % target_dir)
                print("Aborting.")
                if os.path.isdir(target_dir):
                    shutil.rmtree(target_dir)
                sys.exit(1)
                pass

            # Temporary run-dir:
            tmpdir = os.path.join(tmpdir_base, '__'.join([b, cmtcfg, relN, job.__name__]))
            tmpdir_start = os.path.join(tmpdir, '__start')
            if os.path.exists(tmpdir):
                canremove = False
                if not os.path.exists(tmpdir_start):
                    time.sleep(20)
                if not os.path.exists(tmpdir_start):
                    canremove = True
                elif (time.time() - os.path.getmtime(tmpdir_start)) > 24 * 3600:
                    canremove = True
                if not canremove:
                    print("WARNING: Found unexpected recent tmp rundir %s" % tmpdir)
                    continue
                shutil.rmtree(tmpdir)
            os.makedirs(tmpdir)
            touch(tmpdir_start)
            # NB: Stop after one job has run! Cron will relaunch us each hour
            # and will then run the next job!

            def cmd_gen(_cmd, _asetup_cmd, rundir, infiles, relN, builddate):
                file_command = os.path.join(rundir, '__command.sh')
                cmds = ['#!/bin/bash',
                        '',
                        '#Input files are copied locally before running. Sources are:']
                cmds += ['source ~/.bashrc;']
                cmds += ['source ~/.bash_profile;']
                cmds += ['export TRF_ECHO=1;']
                cmds += ['', 'touch __start_asetup', _asetup_cmd]
                cmds += ['touch __start', _cmd +
                         ' >__log.txt 2>&1', 'echo $? > __exitcode']

                dayofweek = builddate.weekday() - 1
                cmds += [ r'perl -pi -e "s/private\/private/${AtlasBuildBranch}\/rel' + str(dayofweek) + '/g;" __log.txt']
                if atlas_project == 'AthDerivation':
                    cmds += ['mv DAOD_PHYSVAL.my.pool.root myDAOD_PHYSVAL.pool.root']
                cmds += ['for f in my*.pool.root; do',
                         '    if [ -f "$f" ]; then checkxAOD.py $f > $f.checkfile.txt 2>/dev/null; fi',
                         'done']
                cmds += ['BAD=0',
                         'for f in ntuple*.pmon.gz; do',
                         '    if [ -f "$f" ]; then tar xf $f "*.pmonsd.*" || BAD=1; fi',
                         'done',
                         'if [ $BAD != 0 ]; then echo "ERROR: tar problems in $PWD"; fi']
                cmds += ["ls -oqAQ1S --block-size=1 --ignore='__*' > __dirlist.txt"]
                cmds += ['rm -f *.pool.root']
                cmds += ['#Python code to create sqlite file']
                cmds += ['export PYTHONPATH=${BASEDIR}/PerformanceMonitoring/python:$PYTHONPATH']
                cmds += ['python << END']
                cmds += ['']
                cmds += ['import os']
                cmds += ['import pmbDB as mydb']
                cmds += ['x=mydb.pmbDB(os.path.dirname(os.path.realpath(\'__exitcode\')).replace(\'/\',\'__\')+\'.db\')']
                cmds += ['jobname=\'' + job.__name__ + '\'']
                cmds += ['release=os.environ[\'AtlasBuildBranch\']']
                cmds += ['platform=os.environ[\'%s_PLATFORM\']'%(atlas_project)]
                cmds += ['nightly=\'' + relN + '\'']
                cmds += ['x.addEntry(release,platform,nightly,jobname,\'./\')']
                cmds += ['']
                cmds += ['END']
                cmds += ['rsync *.db /build1/atlaspmb/custom_nightly_tests/database_staging_area/.']
                cmds += ['gzip *.txt', 'touch __done', '']

                fh = open(file_command, 'w')
                fh.write('\n'.join(cmds))
                fh.close()
                return file_command

            tmpdir_shortrun = os.path.join(tmpdir, 'prerun')
            os.makedirs(tmpdir_shortrun)
            tmpdir_run = os.path.join(tmpdir, 'run')
            os.makedirs(tmpdir_run)
            cmdfile = cmd_gen(
                cmd,
                asetup_cmd,
                tmpdir_run,
                infiles,
                relN,
                builddate)
            ec2 = os.system('cd %s && source %s' % (tmpdir_run, cmdfile))
            if ec2:
                print("NB: Problems in job", target_dir)
            collect = []
            for f in os.listdir(tmpdir_run):
                if os.path.isdir(f):
                    continue
                bn = os.path.basename(f)
                if bn.startswith('__') or '.db' in bn or '.checkfile.txt.gz' in bn or 'pmonsd' in bn or 'ntuple' in bn or bn.startswith(
                        'log') or bn.startswith('mem') or bn.startswith('prmon') or '.json' in bn:
                    collect += [bn]
            for c in collect:  # Could be susceptible to EOS failures
                shutil.copy2(os.path.join(tmpdir_run, c), target_dir)
            shutil.rmtree(tmpdir)

            touch(file_done)
            sys.exit(0)  # On purpose we stop after one job has run...
