#!/usr/bin/env python3

from utils import db_area, web_area, n_days, jobs, n_workers
import multiprocessing
import os
import pmbWebPage as myweb
import subprocess

# Functions
def copy_outputs(job_name):
    cmd = 'rsync -avzu  '+web_area+'/arch-mon-'+job_name+' /eos/project/a/atlasweb/www/atlas-pmb/. '
    output,error = subprocess.Popen(['/bin/bash', '-c', cmd], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    output,error = output.decode('utf-8'),error.decode('utf-8')
    if not error:
        print("\nCopy job for {} is ALL OK!\n".format(job_name))
    else:
        print("\nCopy job for {} returned an ERROR!\n{}".format(job_name,error))

def my_worker_function(args):

    print(args)
    # Check the arguments and unpack
    if args is None or len(args) != 3:
        return
    (job_name, releases, platforms) = args

    # Get the database file
    db_file_name = '{}/{}.db'.format(db_area,job_name)
    if not os.path.exists(db_file_name):
        print('Input database {} does not exists...'.format(db_file_name))
        return

    # Create the webpage class instance
    current_job=myweb.PMBWebPage(db_file_name,job_name,releases,platforms,n_days,web_area)

    # Check the steps
    if current_job.steps == [] :
       print("The job {} has no steps to process, quitting...".format(job_name))
       return
    else:
       print("Processing the job {}".format(job_name))

    # Individual pages
    current_job.Build_All_WebPages()

    # Comparison pages
    if len(releases)>1 or len(platforms)>1:
        for r in releases:
            for p in platforms:
                if '21.0' in r and 'x86_64-centos7-gcc62-opt' not in p:
                    continue
                if 'master' in r and 'x86_64-centos7-gcc62-opt' in p:
                    continue
                print('Processing release {} on platform {}'.format(r,p))
                comparison_job=myweb.PMBWebPage(db_file_name,job_name,[r],[p],n_days,web_area)
                comparison_job.Build_All_WebPages()

    # Copy the outputs
    copy_outputs(job_name)

# Run the jobs in a pool of 10 workers
if '__main__' in __name__:
    with multiprocessing.Pool(n_workers) as p:
        p.map(my_worker_function, jobs)
