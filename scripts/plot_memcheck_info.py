import os
import json
import glob
import collections
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

release = 'master'
job = 'fullchain_mc15_ttbar_valid_13tev_25ns_mu40'
step = 'RDOtoRDOTrigger'
out_folder = '~/MemCheckPlots/'

#
# Code below doesn't contain stuff that needs to be configured by the user
#

in_folder = '~/eos_atlaspmb/archive/custom/'
in_folder = os.path.expanduser(in_folder)
out_folder = os.path.expanduser(out_folder)

file_pattern = '*/*/*/' + release + '/x86_64-slc6-gcc62-opt/' + \
    job + '/mem.summary.' + step + '.json'
file_names = glob.glob(in_folder + file_pattern)  # find all files

dates = {}

# Loop
for name in file_names:
    print(name)
    date = name.split('custom/')[1].split('/' + release)[0]
    date = datetime.datetime.strptime(date, "%d/%m/%Y")
    isSuccess = False
    with open(name.replace('mem.summary.' + step + '.json', '__exitcode')) as f:
        for line in f:
            if '0' in line:
                isSuccess = True
                break
    if not isSuccess:
        continue
    with open(name) as f:
        try:
            data = json.load(f)
            dates[date] = [data['Avg']['avgPSS'], data['Avg']['avgRSS']]
        except BaseException:
            pass

# Order
dates = collections.OrderedDict(sorted(dates.items()))

# Can be more efficient
xvalues = []
pssvalues = []
rssvalues = []

for i, v in dates.iteritems():
    xvalues.append(i)
    pssvalues.append(v[0] / 1024. / 1024.)
    rssvalues.append(v[1] / 1024. / 1024.)

xvalues = np.array(xvalues)
pssvalues = np.array(pssvalues)
rssvalues = np.array(rssvalues)

# Plot
with PdfPages(out_folder + release + '_' + job + '_' + step + '.pdf') as pdf:
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    fig.suptitle(
        'Release : %s - Job : %s - Step : %s' %
        (release, job, step), fontsize=10)
    ax.plot_date(
        xvalues,
        pssvalues,
        markerfacecolor='blue',
        markeredgecolor='white',
        label='Avg. PSS')
    ax.plot_date(
        xvalues,
        rssvalues,
        markerfacecolor='green',
        markeredgecolor='white',
        label='Avg. RSS')
    fig.autofmt_xdate()
    ax.set_xlabel('Nightly Date')
    ax.set_ylabel('Memory [GB]')
    ax.set_ylim([0., 3.5])
    ax.legend(loc='upper left')
    plt.grid(True)
    pdf.savefig()
    plt.close()
