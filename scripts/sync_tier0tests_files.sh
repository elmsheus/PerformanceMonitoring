#!/bin/bash

release="${1}"
test_type="${2}"
project_type="${3}"
reference_version="${4}"
if [[ -z "${project_type}" ]]; then
  project_type="Athena" # Default to Athena if empty
fi
eos_dir="/eos/atlas/atlascerngroupdisk/data-art/grid-input/Tier0ChainTests"
work_dir="/build/atlaspmb/RunTier0TestsReferenceFiles/rundir_${release}_${test_type}"
sync_files=true

###########################
# Setup 
###########################
mkdir -p ${work_dir};
cd ${work_dir};
source ~/.bashrc;
source ~/.bash_profile;
lsetup "asetup ${project_type},${release},latest,gcc62,slc6";

###########################
# See if nightly is new
###########################
nightly_dir="${project_type}_DIR"
nightly=`echo "${!nightly_dir##*/${release}/}"`
nightly=`echo "${nightly%%/${project_type}/*}"`
nightly=`echo "${nightly%%T*}"`
current=`date --date=${nightly} +%s`
if [[ -f ../newest_nightly_${release}_${test_type} ]]; then
  reference=`date --date=$(cat ../newest_nightly_${release}_${test_type}) +%s`
else
  reference=-1
fi

if [[ $current -gt $reference ]]; then
  echo "We have a new nightly..."
  echo "${nightly}" > ../newest_nightly_${release}_${test_type}
else
  echo "We DO NOT have a new nightly..."
  exit 0
fi

if [[ "${release}" == "master" ]]; then
  release="22.0"
fi

###########################
# qTest
###########################
if [[ "${test_type}" == "qTest" ]]; then

  ###########################
  # Clean-up 
  ###########################
  if [[ -d ${work_dir}/run_q221 ]]; then
    rm -r ${work_dir}/run_q221
  fi
  if [[ -d ${work_dir}/run_q431 ]]; then
    rm -r ${work_dir}/run_q431
  fi
  if [[ -f ${work_dir}/RunTier0Tests.log ]]; then
    mv ${work_dir}/RunTier0Tests.log ${work_dir}/RunTier0Tests_LAST.log
  fi
  
  ###########################
  # RunTier0Tests.py 
  ###########################
  RunTier0Tests.py -n -e " --maxEvents=20 ";
  
  if [[ $sync_files == false ]]; then
      exit $?
  fi
  
  ###########################
  # q221
  ###########################
  tail -n 20 run_q221/log.HITtoRDO        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null && \
  tail -n 20 run_q221/log.RDOtoRDOTrigger | grep "INFO leaving with code 0: \"successful run\"" > /dev/null && \
  tail -n 20 run_q221/log.RAWtoESD        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null && \
  tail -n 20 run_q221/log.ESDtoAOD        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null && \
  tail -n 20 run_q221/log.AODtoTAG        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null;
  
  # DELETE EXISTING FILES AND THEN DELETE THE RUN_DIR
  if [[ $? -eq 0 ]]; then
      rsync -auvz run_q221/myRDO.pool.root ${eos_dir}/q221/${release}/${reference_version}/.
      rsync -auvz run_q221/myESD.pool.root ${eos_dir}/q221/${release}/${reference_version}/.
      rsync -auvz run_q221/myAOD.pool.root ${eos_dir}/q221/${release}/${reference_version}/.
      rsync -auvz run_q221/myTAG.pool.root ${eos_dir}/q221/${release}/${reference_version}/.
  else
      echo "ERROR :: At least one step of q221 failed, not syncing..."
      echo "ERROR :: At least one step of q221 failed, not syncing..."
      echo "ERROR :: At least one step of q221 failed, not syncing..."
  fi
  
  ###########################
  # q431
  ###########################
  tail -n 20 run_q431/log.RAWtoESD        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null && \
  tail -n 20 run_q431/log.ESDtoAOD        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null && \
  tail -n 20 run_q221/log.AODtoTAG        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null;
  
  if [[ $? -eq 0 ]]; then
      rsync -auvz run_q431/myESD.pool.root ${eos_dir}/q431/${release}/${reference_version}/.
      rsync -auvz run_q431/myAOD.pool.root ${eos_dir}/q431/${release}/${reference_version}/.
      rsync -auvz run_q431/myTAG.pool.root ${eos_dir}/q431/${release}/${reference_version}/.
  else
      echo "ERROR :: At least one step of q431 failed, not syncing..."
      echo "ERROR :: At least one step of q431 failed, not syncing..."
      echo "ERROR :: At least one step of q431 failed, not syncing..."
  fi

fi

###########################
# sTest
###########################
if [[ "${test_type}" == "sTest" ]]; then
  
  current_tag="s3126"

  ###########################
  # Clean-up 
  ###########################
  if [[ -d ${work_dir}/run_${current_tag} ]]; then
    rm -r ${work_dir}/run_${current_tag}
  fi
  if [[ -f ${work_dir}/RunTier0Tests.log ]]; then
    mv ${work_dir}/RunTier0Tests.log ${work_dir}/RunTier0Tests_LAST.log
  fi

  ###########################
  # RunTier0Tests.py 
  ###########################
  RunTier0Tests.py -n -s -e " --maxEvents=10 ";
  
  if [[ $sync_files == false ]]; then
      exit $?
  fi
  
  ###########################
  # Check success  
  ###########################
  tail -n 20 run_${current_tag}/log.EVNTtoHITS        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null
  
  # DELETE EXISTING FILES AND THEN DELETE THE RUN_DIR
  if [[ $? -eq 0 ]]; then
      rsync -auvz run_${current_tag}/myHITS.pool.root ${eos_dir}/${current_tag}/${release}/${reference_version}/.
  else
      echo "ERROR :: At least one step of ${current_tag} failed, not syncing..."
      echo "ERROR :: At least one step of ${current_tag} failed, not syncing..."
      echo "ERROR :: At least one step of ${current_tag} failed, not syncing..."
  fi

fi

###########################
# oTest
###########################
if [[ "${test_type}" == "oTest" ]]; then

  current_tag="overlay-d1498"

  ###########################
  # Clean-up 
  ###########################
  if [[ -d ${work_dir}/run_${current_tag} ]]; then
    rm -r ${work_dir}/run_${current_tag}
  fi
  if [[ -f ${work_dir}/RunTier0Tests.log ]]; then
    mv ${work_dir}/RunTier0Tests.log ${work_dir}/RunTier0Tests_LAST.log
  fi

  ###########################
  # RunTier0Tests.py 
  ###########################
  RunTier0Tests.py -n -o;
  
  if [[ $sync_files == false ]]; then
      exit $?
  fi
  
  ###########################
  # Check success  
  ###########################
  tail -n 20 run_${current_tag}/log.OverlayPool        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null
  
  # DELETE EXISTING FILES AND THEN DELETE THE RUN_DIR
  if [[ $? -eq 0 ]]; then
      rsync -auvz run_${current_tag}/myRDO.pool.root ${eos_dir}/${current_tag}/${release}/${reference_version}/.
  else
      echo "ERROR :: At least one step of ${current_tag} failed, not syncing..."
      echo "ERROR :: At least one step of ${current_tag} failed, not syncing..."
      echo "ERROR :: At least one step of ${current_tag} failed, not syncing..."
  fi

fi

###########################
# dTest
###########################
if [[ "${test_type}" == "dTest" ]]; then

  current_tag="patched_derivation_test"
  eos_dir="/eos/atlas/atlascerngroupdisk/data-art/grid-input/DerivationFrameworkART/ci-reference-files/mc/"

  ###########################
  # Clean-up
  ###########################
  if [[ -d ${work_dir}/${current_tag} ]]; then
    rm -r ${work_dir}/${current_tag}
  fi
  if [[ -f ${work_dir}/derivation_test.log ]]; then
    mv ${work_dir}/derivation_test.log ${work_dir}/derivation_test_LAST.log
  fi

  ###########################
  # frozen_derivation_test.py
  ###########################
  frozen_derivation_test.py --mode=mc --patched --clean-dir="${eos_dir}";

  if [[ $sync_files == false ]]; then
      exit $?
  fi

  ###########################
  # Check success
  ###########################
  tail -n 20 ${current_tag}/log.AODtoDAOD        | grep "INFO leaving with code 0: \"successful run\"" > /dev/null

  # DELETE EXISTING FILES AND THEN DELETE THE RUN_DIR
  if [[ $? -eq 0 ]]; then
      rsync -auvz ${current_tag}/DAOD_PHYSVAL.pool.root ${eos_dir}/.
  else
      echo "ERROR :: At least one step of ${current_tag} failed, not syncing..."
      echo "ERROR :: At least one step of ${current_tag} failed, not syncing..."
      echo "ERROR :: At least one step of ${current_tag} failed, not syncing..."
  fi

fi
