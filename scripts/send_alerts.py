#!/usr/bin/env python3

import sys
import datetime
import numpy as np
import sqlite3
from sqlite3 import Error
import pandas as pd
import subprocess
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from subprocess import Popen, PIPE
from utils import in_area, db_area, web_area, jobs
from os import system

import logging
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

# Currently testing only a single job..
jobname = "rawtoall_data18_mt1"
branch = "master"
platform = "x86_64-centos7-gcc11-opt"

dbname = f"{db_area}/{jobname}.db"
html_folder = f"{web_area}/arch-mon-{jobname}/pages_custom/zzz/{branch}__{platform}"

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn

def load_sizeperevt(conn, days):
    cur = conn.cursor()
    sql = "select j.jid, j.date, c.domain, c.name, c.sizeperevt from job j inner join container c on j.jid=c.jid where c.domain != 'all' and j.date > date('now','-%s days') and c.sizeperevt !=0 group by j.jid, j.date,c.domain, c.name;" % str(days)
    cur.execute(sql)
    rows = cur.fetchall()

    return rows

def get_date_list(days):
    dates = []
    for day in range(days):
        date = (datetime.datetime.now() - datetime.timedelta(days=day)).date().strftime("%Y-%m-%d")
        dates.append(date)
    return dates

def get_same_entries(A, B, column):
    if A.shape[0] > B.shape[0]:
        indices = np.nonzero(np.isin(A[:, column], B[:, column]))
        return A[indices], B
    else:
        indices = np.nonzero(np.isin(B[:, column], A[:, column]))   
        return A, B[indices]

def get_diff_entries(A, B, column):

    if A.shape[0] > B.shape[0]:
        diff = np.isin(A[:, column], B[:, column])
        indices = np.argwhere(diff==False).flatten()
        return A[indices]
    else:
        diff = np.isin(B[:, column], A[:, column])
        indices = np.argwhere(diff==False).flatten()
        return B[indices]

if __name__ == '__main__':
    
    days = 7
    send_email = True 
    dates = get_date_list(days)
    logging.info('loading records for %s' % dates)
    
    ####loading records from db####
    conn = create_connection(dbname)
    with conn:
        res = load_sizeperevt(conn, days)
    res = np.asarray(res)
 
    if len(res) == 0:
       logging.info('no data loaded over the last %s days' % days)
       sys.exit(0)

    logging.info(res)

    day1 = dates[1] # change this for the date
    day2 = dates[2] # change this for the date

    record1= res[res[:, 1] == day1]
    record2 = res[res[:, 1] == day2]

    logging.info('comparing the difference between %s and %s' % (day1, day2))

    mail_body = ''

    ####different entries####
    diff_entries = get_diff_entries(record1, record2, 3)      
    if diff_entries.shape[0]>0:
        mail_body += '<p> <b>Different Entries (%s and %s) </b> </p>' % (day1, day2)
        mail_body += pd.DataFrame(diff_entries, columns=['JobId', 'Date', 'Domain', 'Name', 'SizePerEvent']).to_html()
    else:
        logging.info('no different entries observed for %s and %s' % (day1, day2))

    ####difference among the same entries####
    same_entries_day1, same_entries_day2 = get_same_entries(record1, record2, 3)      
    diff = np.absolute((same_entries_day1[:, 4].astype(float) - same_entries_day2[:, 4].astype(float)) / same_entries_day2[:, 4].astype(float))
    threashold = 0.1/100
    diff_values = np.concatenate((same_entries_day1[diff > threashold], same_entries_day2[diff > threashold]), axis=1)
 
    if diff_values.shape[0]>0:
        mail_body += '<p> <b> Differences Among The Same  Entries (%s and %s) </b> </p>' % (day1, day2)
        mail_body += pd.DataFrame(diff_values, columns=['JobId', 'Date', 'Domain', 'Name', 'SizePerEvent', 'JobId', 'Date', 'Domain', 'Name', 'SizePerEvent']).to_html()
    else:
        logging.info('no difference among the same entries observed for %s and %s' % (day1, day2))   

    ####build a simple page if no differences are found####
    if not mail_body:
        mail_body += '<p> <b>No observed differences (%s and %s) </b> </p>' % (day1, day2)
        logging.info('no overall difference is observed for %s and %s' % (day1, day2))

    if mail_body:
        ####saving html file####
        if not system(f"mkdir -p {html_folder}"):
            with open(f"{html_folder}/sizeperevent_monitor.html","w") as f1, open(f"{html_folder}/sizeperevent_monitor_{day1}-{day2}.html","w") as f2:
                f1.write(mail_body)
                f2.write(mail_body)
                f1.close()
                f2.close()
        else:
            print("ERROR WITH HTML FILE CREATION")

        if send_email and 'No observed differences' not in mail_body:
            ####sending out the notifications####
            html = MIMEText(mail_body, "html")
            msg = MIMEMultipart("alternative")
            msg["To"] = 'shaojun.sun@cern.ch, alaettin.serhan.mete@cern.ch'
            msg["Subject"] = "Container size changes report"
            msg.attach(html)

            p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
            p.communicate(msg.as_string().encode())
