#!/bin/bash

# Define and execute the main job
execute() {

    # Define test parameters
    JOBNAME="${1}";
    JOBRELEASE="${2}";
    JOBPLATFORM="${3}";

    # Figure out the relevant steps, can be coma separated e.g. StepA,StepB etc.
    declare -A JOBSTEPS=( ["rawtoall_data18_mt16"]="RAWtoALL"
                          ["rawtoall_data18_mt8"]="RAWtoALL"
                          ["rawtoall_data18_mt1"]="RAWtoALL"
                          ["rawtoall_data22_mt8"]="RAWtoALL"
                          ["rawtoall_data22_mt1"]="RAWtoALL"
                          ["evnttohits_itk_ca_mt16"]="EVNTtoHITS"
                          ["rdotordotrigger_q221_mt8"]="RDOtoRDOTrigger"
                          ["fullchain_q445_mt8"]="HITtoRDO,RDOtoRDOTrigger,RAWtoALL"
                          ["fullchain_mc21a_mt8"]="Overlay,RDOtoRDOTrigger,RAWtoALL" )

    # Define the top-level workdir
    WORKDIR="/data/atlaspmb/athenamt-perfmonmt-jobs";

    # Create the rundir
    RUNDIR="${WORKDIR}/${JOBNAME}/${JOBRELEASE}/${JOBPLATFORM}";
    if [[ ! -d ${RUNDIR} ]]; then
        echo "Directory ${RUNDIR} doesn't exist, nothing to do...";
        return 0;
    fi

    # Setup the environment
    BASEDIR="/afs/cern.ch/atlas/project/pmb/spot";
    source $BASEDIR/spot-pyenv/bin/activate;
    export PYTHONPATH=$BASEDIR/../new_pmb/PerformanceMonitoring/python:$PYTHONPATH;

    # Go to the workdir
    cd ${WORKDIR};

    # Create the database files
    python3 $BASEDIR/spotscripts/create_spot_db.py -i ${RUNDIR} -o ${WORKDIR}/dbfiles \
        -j ${JOBNAME} -r ${JOBRELEASE} -p ${JOBPLATFORM};

    # Merge the database files
    if [[ -f dbfiles/${JOBNAME}.db ]]; then
      echo "Removing existing merged database file...";
      rm -f dbfiles/${JOBNAME}.db;
    fi
    python3 $BASEDIR/spotscripts/merge_spot_db.py -i "${WORKDIR}/dbfiles/*__${JOBNAME}__*.db" -o ${WORKDIR}/dbfiles/${JOBNAME}.db;

    # Create the webpage plots and html files
    python3 $BASEDIR/spotscripts/create_spot_plots.py -i ${WORKDIR}/dbfiles/${JOBNAME}.db -o ${HOME}/www_atlaspmb/spot-mon-${JOBNAME}/pages \
        -j ${JOBNAME} -r ${JOBRELEASE} -p ${JOBPLATFORM} -s ${JOBSTEPS[${JOBNAME}]} -w;

}

# Define the main function
main() {

    # Define the jobs this setup will attempt to run
    JOBS=( "22.0 x86_64-centos7-gcc11-opt rawtoall_data22_mt8" \
           "22.0 x86_64-centos7-gcc11-opt rawtoall_data18_mt8" \
           "22.0 x86_64-centos7-gcc11-opt fullchain_mc21a_mt8" \
           "22.0 x86_64-centos7-gcc11-opt fullchain_q445_mt8" \
           "22.0 x86_64-centos7-gcc11-opt rawtoall_data22_mt1" \
           "22.0 x86_64-centos7-gcc11-opt rawtoall_data18_mt1" \
           "master x86_64-centos7-gcc11-opt rawtoall_data22_mt8" \
           "master x86_64-centos7-gcc11-opt rawtoall_data18_mt8" \
           "master x86_64-centos7-gcc11-opt fullchain_mc21a_mt8" \
           "master x86_64-centos7-gcc11-opt fullchain_q445_mt8" \
           "master x86_64-centos7-gcc11-opt rawtoall_data22_mt1" \
           "master x86_64-centos7-gcc11-opt rawtoall_data18_mt1" \
           "master x86_64-centos7-clang14-opt rawtoall_data22_mt8" \
           "master x86_64-centos7-clang14-opt rawtoall_data18_mt8" \
           "master x86_64-centos7-clang14-opt fullchain_mc21a_mt8" \
           "master x86_64-centos7-clang14-opt fullchain_q445_mt8" \
           "master x86_64-centos7-clang14-opt rawtoall_data22_mt1" \
           "master x86_64-centos7-clang14-opt rawtoall_data18_mt1" \
           "master--archflagtest x86_64-centos7-gcc11-opt rawtoall_data22_mt8" \
           "master--archflagtest x86_64-centos7-gcc11-opt fullchain_mc21a_mt8" \
           "master--ltoflagtest x86_64-centos7-gcc11-opt rawtoall_data22_mt8" \
           "master--ltoflagtest x86_64-centos7-gcc11-opt fullchain_mc21a_mt8" )

    # Loop over all defined jobs
    for job in "${JOBS[@]}"
    do
        # Parse the inputs: release platform name
        vals=(${job})
        jobrelease="${vals[0]}"
        jobplatform="${vals[1]}"
        jobname="${vals[2]}"
        # Process the job
        execute "${jobname}" "${jobrelease}" "${jobplatform}"
    done

}

# Execute main
main
