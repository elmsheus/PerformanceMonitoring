#!/bin/bash

source ~/.bashrc
source ~/.bash_profile
lsetup "asetup master,Athena,latest"
eosfusebind -g # ATLINFR-3256

export TRF_ECHO=True
export HOSTNAME=`hostname`
SERVICE=pmb_cron.py
python=python
counts=`ps ax | grep -v grep | grep $python | grep $SERVICE | wc -l`
max_counts=16
if [ "$counts" -ge "$max_counts" ]
then
    "Max counts reached"
    exit 0
else
    python /afs/cern.ch/atlas/project/pmb/new_pmb/PerformanceMonitoring/scripts/pmb_cron.py
fi
wait
