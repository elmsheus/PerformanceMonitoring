# Gitlab workflow
This page describes how to make changes to this project from a remote session. 

You must have the atlaspmb SSH keys. In the atlaspmb account they are located at
```
/afs/cern.ch/user/a/atlaspmb/.ssh/atlaspmb.id_rsa
/afs/cern.ch/user/a/atlaspmb/.ssh/atlaspmb.id_rsa.pub
```

Otherwise you can use your own key, but you would need to add your ssh key

```
https://gitlab.cern.ch/profile/keys
```

Within your session:

Add your ssh key to your session and retrieve your Kerberos ticket

```
eval `ssh-agent`
ssh-add ~/.ssh/atlaspmb.id_rsa
kinit atlaspmb@CERN.CH
```

If not already done
```
git clone https://:@gitlab.cern.ch:8443/atlaspmb/PerformanceMonitoring.git
```

```
git checkout -b <topic> origin/master --no-track
```

Develop your code!

```
git add <file>
git commit
git push --set-upstream origin <topic>

Example 

$ git push --set-upstream origin rebase
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 394 bytes | 0 bytes/s, done.
Total 4 (delta 2), reused 0 (delta 0)
remote: 
remote: To create a merge request for rebase, visit:
remote:   https://gitlab.cern.ch/atlaspmb/PerformanceMonitoring/merge_requests/new?merge_request%5Bsource_branch%5D=rebase
remote: 
To https://gitlab.cern.ch:8443/atlaspmb/PerformanceMonitoring.git
 * [new branch]      rebase -> rebase
Branch rebase set up to track remote branch rebase from origin.
```

Put merge request URL displayed as a result of the "git push" command into a web browser where you have logged in atlaspmb user. 
Write a meaningful and relevent comment and press merge request button. Then accept merge request by pressing "Merge" button.

# Updating a local repository with changes from a Github repository

```
$ git pull origin master
```


